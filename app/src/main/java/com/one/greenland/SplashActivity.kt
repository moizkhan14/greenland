package com.one.greenland

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.google.gson.Gson
import com.one.greenland.common.NEW_INSTALLED
import com.one.greenland.common.USER_MODEL
import com.one.greenland.models.User
import com.one.greenland.utils.AppUtils.callIntentClearTaskFlag
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.activities.UserSessionActivity


class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDelay()
    }

    private fun loadingDelay() {
        Handler(Looper.getMainLooper()).postDelayed({
            if (!BuildConfig.DEBUG && PreferenceUtils.getValueForKey(NEW_INSTALLED, true)) {
                PreferenceUtils.removeStoredValue(USER_MODEL)
                PreferenceUtils.setValueForKey(NEW_INSTALLED, false)
            }
            val user = Gson().fromJson(PreferenceUtils.getModelForKey(USER_MODEL), User::class.java)
            if (user != null && user.isLoggedIn) {
                callIntentClearTaskFlag(this, HomeActivity::class.java)
            } else
                callIntentClearTaskFlag(this, UserSessionActivity::class.java)
        }, 3000)
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_splash
    }

    override fun getActivityName(): String? {
        return "Splash Activity"
    }

    override fun receiveExtras(arguments: Bundle?) {

    }
}