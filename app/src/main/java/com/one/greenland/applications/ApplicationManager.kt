package com.one.greenland.applications

import android.app.Application
import android.content.Context

/**
 * Created by umair on 1/1/18
 */
class ApplicationManager : Application() {
    var context: Context? = null
        private set

    override fun onCreate() {
        super.onCreate()
        context = this
        instance = this

    }

    companion object {
        @get:Synchronized
        var instance: ApplicationManager? = null
            private set
    }
}