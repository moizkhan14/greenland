package com.one.greenland.common

//Fragment Tag
const val HOME_FRAGMENT_TAG = "Home Fragment"
const val BOOKING_FRAGMENT_TAG = "Booking Fragment"
const val CART_FRAGMENT_TAG = "Cart Fragment"
const val MENU_FRAGMENT_TAG = "Menu Fragment"

const val SIGN_IN_UI_VISIBLE = "sign_in_ui_visible"
const val SIGN_UP_UI_VISIBLE = "sign_up_ui_visible"
const val FORGOT_PASSWORD_UI_VISIBLE = "forgot_password_ui_visible"

const val ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

// Dialog Literal Names

const val POPULAR_SERVICE_DIALOG = "popular_service_dialog"
const val POPULAR_SERVICE_ADD_ON_DIALOG = "popular_service_add_on_dialog"
const val POPULAR_SERVICE_ADD_ON_DIALOG_CANCEL = "popular_service_add_on_dialog_cancel"

const val MAIN_POPULAR_SERVICE_DIALOG_TYPE = "main_popular_service_dialog_type"
const val MAIN_POPULAR_SERVICE_ADD_ON_DIALOG_TYPE = "main_popular_service_add_on_dialog_type"

//Model's Keys
const val NEW_INSTALLED = "new_installed"
const val USER_MODEL = "user_model"
const val ORDER_LISTING_MODEL = "order_listing_model"
const val ORDER_CART = "order_cart"
const val REGISTERED_USER_LIST = "registered_user_list"
const val PROFILE_PICTURE = "user_profile_pic"
const val ORDER_ID = "order_id"

//Extras
const val USER_SESSION_INTENT_EXTRAS = "fragment_name"
const val PHONE_NUMBER = "phone_number"
const val BUNDLE_KEY = "bundle"
const val CALLING_ACTIVITY_KEY = "calling_activity"

//Validations
const val PASSWORD_LENGTH = 8
const val CNIC_LENGTH = 13

//Parameters
const val CROSS_TO_TICK = "cross_to_tick"
const val TICK_TO_CROSS = "tick_to_cross"
const val CURRENT_USER_MESSAGE = 0
const val OTHER_USER_MESSAGE = 1

//Click IDs
const val CLICK_ID_POPULAR_SERVICES = 1
const val CLICK_ID_CATEGORIES = 2

// Sample Service Items
const val POPULAR_SERVICES = "[{\n" +
        "\t\"id\": 156,\n" +
        "\t\"service_title\": \"Gardners\",\n" +
        "\t\"service_duration\": -10,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"service_category_id\": 27,\n" +
        "\t\"description\": \"- The Gardber provide you services at home.\n" +
        "- If you want to hire  a gardner at home you can contact to gardner by contacting us.\n" +
        "- If gardner wants a job, please contact us freely.\",\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_category_title\": \"Gardners\",\n" +
        "\t\"cover_image\": \"https://firebasestorage.googleapis.com/v0/b/instam-afd92.appspot.com/o/gardner.png?alt=media&token=fcd62ea4-4fdc-4484-8e00-5cb7b3d4bbd6\",\n" +
        "\t\"service_addons\": []\n" +
        "}, {\n" +
        "\t\"id\": 158,\n" +
        "\t\"service_title\": \"Botanist\",\n" +
        "\t\"service_duration\": 40,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"service_category_id\": 27,\n" +
        "\t\"description\": \"- Our Expert botaninst will provide guidelines to you.\n" +
        "- Sort your quries and issues.\n" +
        "- You can get tips and that how you should take care of plants.\n" +
        "- Medicinal herbs for discard plants. \",\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_category_title\": \"Botanical Services\",\n" +
        "\t\"cover_image\": \"https://firebasestorage.googleapis.com/v0/b/instam-afd92.appspot.com/o/botanist.png?alt=media&token=bc7d10b8-c0b4-474e-8a77-0ef15496c5a5\",\n" +
        "\t\"service_addons\": []\n" +
        "}, {\n" +
        "\t\"id\": 161,\n" +
        "\t\"service_title\": \"Nurseries\",\n" +
        "\t\"service_duration\": 30,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"service_category_id\": 27,\n" +
        "\t\"description\": \"- Nurseries will provide your desired plants at your door step.\n" +
        "- Nurseries can maintain your plants.\",\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_category_title\": \"Nursery Services\",\n" +
        "\t\"cover_image\": \"https://firebasestorage.googleapis.com/v0/b/instam-afd92.appspot.com/o/nursesry.png?alt=media&token=9b3c5302-c441-4040-a557-f6cf50d5ad87\",\n" +
        "\t\"service_addons\": []\n" +
        "}]"

const val FLOWERING_PLANTS = "[{\n" +
        "\t\"id\": 19,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 45,\n" +
        "\t\"description\": \" A red rose is an unmistakable expression of love. Red roses convey deep emotions - be it love, longing or desire.\n" +
        "Rose care is easier than you think—anyone can grow them successfully. Plant your roses in a sunny location with good drainage. Fertilize them regularly for impressive flowers. Water them evenly to keep the soil moist. Prune established rose bushes in early spring. And watch for diseases like powdery mildew or black spot.\n" +
        "\",\n" +
        "\t\"image\": \"redrose\",\n" +
        "\t\"service_price\": 1200.0,\n" +
        "\t\"service_title\": \"Red Rose Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 20,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 45,\n" +
        "\t\"description\": \"White roses often represent purity, innocence and youthfulness. White roses are sometimes referred to as bridal roses because of their association with young love and eternal loyalty.\n" +
        "The best time to plant roses is in the spring, after the last frost, or in the fall at least six weeks before the average first frost in your area. This gives the roots enough time to burrow into the soil before the plants go dormant over the winter\n" +
        "\",\n" +
        "\t\"image\": \"whiterose\",\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_title\": \"White Rose Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 21,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 60,\n" +
        "\t\"description\": \"The sweet peach rose conveys a meaning of modesty, genuineness, sincerity and gratitude. It can be great for saying “thank you” and would be an appropriate gift when closing a business deal,\n" +
        "They live for six months. These flowers bloom in the early fall, in cold weather.\n" +
        "\",\n" +
        "\t\"image\": \"rosepeach\",\n" +
        "\t\"service_price\": 1100.0,\n" +
        "\t\"service_title\": \"Peach Rose Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 22,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [{\n" +
        "\t\t\"id\": 114,\n" +
        "\t\t\"is_active\": true,\n" +
        "\t\t\"is_addon\": true,\n" +
        "\t\t\"is_featured\": false,\n" +
        "\t\t\"service_addon_price\": 500.0,\n" +
        "\t\t\"service_category_id\": 0,\n" +
        "\t\t\"service_duration\": 10,\n" +
        "\t\t\"service_id\": 22,\n" +
        "\t\t\"service_title\": \"Money Plant\"\n" +
        "\t}, {\n" +
        "\t\t\"id\": 115,\n" +
        "\t\t\"is_active\": true,\n" +
        "\t\t\"is_addon\": true,\n" +
        "\t\t\"is_featured\": false,\n" +
        "\t\t\"service_addon_price\": 500.0,\n" +
        "\t\t\"service_category_id\": 0,\n" +
        "\t\t\"service_duration\": 20,\n" +
        "\t\t\"service_id\": 22,\n" +
        "\t\t\"service_title\": \"Money Plant\"\n" +
        "\t}],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\": \"Many indoor plants are believed to bring prosperity and good luck including money   plant.\n" +
        "It is indeed an Air Purifying Plant!\n" +
        "It reduces Stress!\n" +
        "Money Plant is also believed to bring long-lasting friendship, due its heart shaped leaves.\n" +
        "\",\n" +
        "\t\"image\": \"moneyplant\",\n" +
        "\t\"service_price\": 1700.0,\n" +
        "\t\"service_title\": \"Money Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 23,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 115,\n" +
        "\t\"description\": \"The classic Zinnia is easy to grow and provides vibrant red, orange, yellow, violet white or pink blooms. Lasting from the summer into mid-fall, Zinnias attract butterflies and hummingbirds.\n" +
        "It blooms in Mid-summer and mid-fall. It needs full sun and well drained soil.\n" +
        "\",\n" +
        "\t\"image\": \"zinnia\",\n" +
        "\t\"service_price\": 3000.0,\n" +
        "\t\"service_title\": \"Zinnia Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 24,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering plants\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\": \"One of the most recognizable perennials, you know its spring time once a Daffodil is in bloom. Their iconic trumpet flowers typically come in yellow, but are also grown with red, orange, yellow, white and even pink blossoms.\n" +
        "These flowers bloom in spring .it needs full sun and drained soil.\n" +
        "\",\n" +
        "\t\"image\": \"daffodils\",\n" +
        "\t\"service_price\": 1700.0,\n" +
        "\t\"service_title\": \"Daffodils\"\n" +
        "}, {\n" +
        "\t\"id\": 25,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\": \"The lavender rose is often a sign of enchantment and love at first sight.\",\n" +
        "\t\"image\": \"purplerose\",\n" +
        "\t\"service_price\": 1200.0,\n" +
        "\t\"service_title\": \"Purple Rose Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 76,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 70,\n" +
        "\t\"description\": \"Known for their tall stalks and bright yellow petals, the Sunflower is an all time classic wildflower. They most commonly come in yellow, but Sunflowers can also be found in rich orange and red hues.\n" +
        "It blooms in early summer and early fall.it needs full sun and well drained soil.\n" +
        "\",\n" +
        "\t\"image\": \"sunflower\",\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_title\": \"Sun Flowers\"\n" +
        "}, {\n" +
        "\t\"id\": 27,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\": \"The meaning of pink roses can stand for femininity, elegance, refinement and sweetness\n" +
        "It blooms in spring. It need well drained soil.\n" +
        "\",\n" +
        "\t\"image\": \"pinkrose\",\n" +
        "\t\"service_price\": 1300.0,\n" +
        "\t\"service_title\": \"Pink Rose Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 28,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Flowering Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\": \"As one of the top two largest flower plant families, the Orchid consists of about 27,800 accepted species. The most common genus, Phalaenopsis, is what is typically seen in home gardens and containers. They come in a vareity of tropical colors, including violet, pink, orange, red and white.\n" +
        "It blooms in mid-summer – mid fall. It needs full sun and well drained soil.\n" +
        "\",\n" +
        "\t\"image\": \"orchids\",\n" +
        "\t\"service_price\": 1600.0,\n" +
        "\t\"service_title\": \"Orchids\"\n" +
        "}]"

const val PLANTS_VEGETABLE = "[{\n" +
        "\t\"id\": 29,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 45,\n" +
        "\t\"description\": \"Bean, seed or pod of certain leguminous plants of the family Fabaceae. Size will vary with variety. Bush beans generally get about 2 feet tall and 1 foot. wide. The beans will grow from 3 to 4 inches long. You can sow green bean seeds directly into the garden once the soil has warmed in the spring.\",\n" +
        "\t\"image\": \"beans\",\n" +
        "\t\"service_price\": 1100.0,\n" +
        "\t\"service_title\": \"Beans Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 30,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 45,\n" +
        "\t\"description\": \"The chili pepper, from Nahuatl chīlli, is the fruit. Water your chilli plants regularly throughout the growing season. Chillies also don't mind humidity as much as sweet peppers or tomatoes do. Most people will need to grow chillis in full sun. In the hottest, sunniest regions chillies still grow well with a bit of shade.\",\n" +
        "\t\"image\": \"chilly\",\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_title\": \"Chilly Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 31,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 60,\n" +
        "\t\"description\": \"Cucumber is a widely-cultivated creeping vine plant . So find a place with maximum sunshine and fertile soil, and get ready to grow the best cucumber plants. Depending on the variety, cucumbers are ready for harvest 50 to 70 days after planting.\",\n" +
        "\t\"image\": \"cucumber\",\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_title\": \"Cucumber Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 32,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [{\n" +
        "\t\t\"id\": 116,\n" +
        "\t\t\"is_active\": true,\n" +
        "\t\t\"is_addon\": true,\n" +
        "\t\t\"is_featured\": false,\n" +
        "\t\t\"service_addon_price\": 300.0,\n" +
        "\t\t\"service_category_id\": 0,\n" +
        "\t\t\"service_duration\": 10,\n" +
        "\t\t\"service_id\": 22,\n" +
        "\t\t\"service_title\": \"Salaud Leaves and Peas\"\n" +
        "\t}, {\n" +
        "\t\t\"id\": 117,\n" +
        "\t\t\"is_active\": true,\n" +
        "\t\t\"is_addon\": true,\n" +
        "\t\t\"is_featured\": false,\n" +
        "\t\t\"service_addon_price\": 1500.0,\n" +
        "\t\t\"service_category_id\": 0,\n" +
        "\t\t\"service_duration\": 20,\n" +
        "\t\t\"service_id\": 22,\n" +
        "\t\t\"service_title\": \"Salaud Leaves and Peas\"\n" +
        "\t}],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\": \"The chopped green garden salad is filled with crunchy, antioxidant and nutrient rich green vegetables, green apple, and pepita seeds, and smothered with a creamy avocado.\n" +
        "Canned peas are sweet peas - frozen peas are English peas and what is found on salad bars if peas are served.\n" +
        "\",\n" +
        "\t\"image\": \"salad\",\n" +
        "\t\"service_price\": 1500.0,\n" +
        "\t\"service_title\": \"Salaud Leaves and Peas\"\n" +
        "},{\n" +
        "\t\"id\": 33,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\": \"It is tempting to harvest potatoes as soon as possible to enjoy them in meals but different varieties can take anything from 70 to 120 days to grow. To begin with, dig a trench that is 6-8 inches deep. Plant each piece of potato (cut side down, with the eyes pointing up) every 12-15 inches, with the rows spaced 3 feet apart.\",\n" +
        "\t\"image\": \"potato\",\n" +
        "\t\"service_price\": 1700.0,\n" +
        "\t\"service_title\": \"Potato Plant\"\n" +
        "},  {\n" +
        "\t\"id\": 34,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\": \"The onion, also known as the bulb onion or common onion, is a vegetable that is the most widely cultivated species. In the garden, onion plants should be spaced 4 to 6 inches apart in the row, with rows 1 to 2 feet apart. In general, onions prefer full sun, sample soil moisture, and fertile, well-drained garden soil. \",\n" +
        "\t\"image\": \"onion\",\n" +
        "\t\"service_price\": 1700.0,\n" +
        "\t\"service_title\": \"Onion Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 35,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\": \"The tomato is the edible, often red, berry of the plant, commonly known as a tomato plant. Start tomatoes indoors in seed-starting tray. six to eight weeks before the last frost date in your area. Tomatoes are vigorous growers that require maximum sun. They will need 6 to 8 hours of sun a day, so plant in the sunniest parts of your garden.\",\n" +
        "\t\"image\": \"tomato\",\n" +
        "\t\"service_price\": 1200.0,\n" +
        "\t\"service_title\": \"Tomato Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 36,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 70,\n" +
        "\t\"description\": \"Cabbage is a leafy green, red, or white .In cool-summer regions, plant cabbage in late spring for a fall harvest. In mild-winter regions, start seed in late summer—about 6 to 8 weeks before the first frost–for a winter or spring harvest.\",\n" +
        "\t\"image\": \"cabbage\",\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_title\": \"Cabbage Plants\"\n" +
        "}, {\n" +
        "\t\"id\": 37,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\": \"A low growing, attractive, perennial, clumping plant which gets its common name from the slight mushroom flavour of the leaves. Most mushrooms grow best in temperatures between 55 and 60°F, away from direct heat and drafts\",\n" +
        "\t\"image\": \"mushroom\",\n" +
        "\t\"service_price\": 1300.0,\n" +
        "\t\"service_title\": \"Mushroom Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 38,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Vegetables Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\": \"Mentha is a genus of plants in the family Lamiaceae.. You can find mint growing indoors in a pot of soil or even in a bottle of water. Where: Mint performs its best in full sun, as long as the soil is kept moist, but it also thrives in partial shade. Mint is considered an invasive plant, since it sends out “runners” and spreads vigorously\",\n" +
        "\t\"image\": \"mint\",\n" +
        "\t\"service_price\": 1600.0,\n" +
        "\t\"service_title\": \"Mint Plant\"\n" +
        "}]"

const val PLANTS_FRUIT = "[{\n" +
        "\t\"id\": 39,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 45,\n" +
        "\t\"description\" : \"An apple is an edible fruit produced by an apple tree. Apple trees are cultivated worldwide and are the most widely grown species in the genus Malus. After filling the pots with loam soil, poke two one-inch (2.54 cm) holes in the soil of each pot about three inches (7.6 cm) apart, then place a seed in each hole. A seedling of any apple variety will grow into a tree from 12 to 20 feet high and take six to 10 years to bear apples.\",\n" +
        "\t\"image\" : \"apple\",\n" +
        "\t\"service_price\": 1500.0,\n" +
        "\t\"service_title\": \"Apple Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 40,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 45,\n" +
        "\t\"description\" : \"Citrus × sinensis, also known as the Citrus sinensis, includes the commonly cultivated sweet oranges. Oranges are self-pollinating and don't need bees to produce fruit. To grow oranges we need sunlight, water, and good cultural practices such as fertilizers and pruning. Our trees also like about 30 days of 32 degree temperature to help maintain the firmness and freshness of the fruit.\",\n" +
        "\t\"image\" : \"orange\",\n" +
        "\t\"service_price\": 1200.0,\n" +
        "\t\"service_title\": \"Orange Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 41,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 60,\n" +
        "\t\"description\" : \"Mangifera indica, commonly known as mango. The seed grown trees will take a lot longer to bear fruit. Mango trees that were grown in a nursery are usually grafted and should fruit within three to four years. Seedling trees may take five to eight years.\",\n" +
        "\t\"image\" : \"mango\",\n" +
        "\t\"service_price\": 1800.0,\n" +
        "\t\"service_title\": \"Mango Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 42,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [{\n" +
        "\t\t\"id\": 129,\n" +
        "\t\t\"is_active\": true,\n" +
        "\t\t\"is_addon\": true,\n" +
        "\t\t\"is_featured\": false,\n" +
        "\t\t\"service_addon_price\": 0.0,\n" +
        "\t\t\"service_category_id\": 0,\n" +
        "\t\t\"service_duration\": 10,\n" +
        "\t\t\"service_id\": 22,\n" +
        "\t\t\"service_title\": \"Cherry Plant\"\n" +
        "\t}, {\n" +
        "\t\t\"id\": 105,\n" +
        "\t\t\"is_active\": true,\n" +
        "\t\t\"is_addon\": true,\n" +
        "\t\t\"is_featured\": false,\n" +
        "\t\t\"service_addon_price\": 2500.0,\n" +
        "\t\t\"service_category_id\": 0,\n" +
        "\t\t\"service_duration\": 20,\n" +
        "\t\t\"service_id\": 22,\n" +
        "\t\t\"service_title\": \"Cherry Plant\"\n" +
        "\t}],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\" : \"Cherry trees are a sight to behold in the spring, when they're covered in white or pink blossoms. ... Sweet cherries grow in hardiness zones 5 to 7. You'll need at least two or three trees, as they'll need to pollinate each other.\",\n" +
        "\t\"image\" : \"cherry\",\n" +
        "\t\"service_price\": 2500.0,\n" +
        "\t\"service_title\": \"Cherry Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 43,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 115,\n" +
        "\t\"description\" : \"The garden strawberry is a widely grown hybrid species of the genus Fragaria, collectively known as the strawberries. For spring planting, as soon as the soil is dry and able to be worked (usually March or April), you should plant your strawberries. The plants need to be well-established before the temperatures rise in the summer months.\",\n" +
        "\t\"image\" : \"stawaberry\",\n" +
        "\t\"service_price\": 1800.0,\n" +
        "\t\"service_title\": \"Stawberry Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 44,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\" : \"Guava plants thrive in any soil with good drainage, and full sun for best flowering and fruit production. While a guava can be grown from seed, it will not be true to the parent and may take up to 8 years to produce fruit. The white-fleshed guava is high in antioxidants, and the red-fleshed variety is even higher.\",\n" +
        "\t\"image\" : \"gauva\",\n" +
        "\t\"service_price\": 1700.0,\n" +
        "\t\"service_title\": \"Guava Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 45,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\" : \"The pomegranate is a fruit-bearing deciduous shrub. Pomegranates prefer a semi-arid mild-temperate to subtropical climate and are naturally adapted to regions with cool winters and hot summers. Some patience is required when growing a pomegranate tree, as it takes five to seven months for fruit\",\n" +
        "\t\"image\" : \"pomgernate\",\n" +
        "\t\"service_price\": 1800.0,\n" +
        "\t\"service_title\": \"Pomegrante Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 46,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 70,\n" +
        "\t\"description\" : \"The banana plant is the largest herbaceous flowering plant. If you have a banana plant, you must have 10 to 15 months of growing for the plant to produce fruit. The exact time the plant takes to fruit will depend on various environmental conditions.\",\n" +
        "\t\"image\" : \"banana\",\n" +
        "\t\"service_price\": 1000.0,\n" +
        "\t\"service_title\": \"Banana Plants\"\n" +
        "}, {\n" +
        "\t\"id\": 47,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\" : \"A plum is a fruit of the subgenus Prunus of the genus Prunus. The trees generally begin bearing fruit four to six years after planting. Plums also need winter chill, pruning and the right climate to produce a good yield.\",\n" +
        "\t\"image\" : \"plum\",\n" +
        "\t\"service_price\": 1300.0,\n" +
        "\t\"service_title\": \"Plum Plant\"\n" +
        "}, {\n" +
        "\t\"id\": 48,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Fruit Plants\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\" : \"To grow good papayas you need a frost free climate, lots of sunlight, lots of water and very good soil. If you can supply all of the above you can pretty much stick some papaya seeds in the ground at any time of the year, and six to ten months later they will start fruiting.\",\n" +
        "\t\"image\" : \"papaya\",\n" +
        "\t\"service_price\": 1600.0,\n" +
        "\t\"service_title\": \"Papaya Plant\"\n" +
        "}]"

const val HERBS_FERTILIZERS = "[{\n" +
        "\t\"id\": 49,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Herbs and Fertlizers\",\n" +
        "\t\"service_duration\": 45,\n" +
        "\t\"description\": \"The olive, known by the botanical name Olea europaea, meaning European olive, is a species of small tree in the family Oleaceae, Olives are very high in vitamin E and other powerful antioxidants. Studies show that they are good for the heart and may protect against osteoporosis and cancer. you need to eat around 30 olives in a day to spend a healthy life.\",\n" +
        "\t\"image\": \"olives\",\n" +
        "\t\"service_price\": 2500.0,\n" +
        "\t\"service_title\": \"Olives\"\n" +
        "}, {\n" +
        "\t\"id\": 50,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Herbs and Fertlizers\",\n" +
        "\t\"service_duration\": 45,\n" +
        "\t\"description\": \"Turmeric is a flowering plant, Curcuma longa of the ginger family. Turmeric is the spice that gives curry its yellow color. It has been used in India for thousands of years as a spice and medicinal herb. Turmeric usually does not cause significant side effects; however, some people can experience stomach upset, nausea or diarrhea. In one report, a person who took very high amounts of turmeric, over 1500 mg twice daily, experienced a dangerous abnormal heart rhythm.\",\n" +
        "\t\"image\": \"turmeric\",\n" +
        "\t\"service_price\": 1200.0,\n" +
        "\t\"service_title\": \"Turmeric\"\n" +
        "}, {\n" +
        "\t\"id\": 51,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Herbs and Fertlizers\",\n" +
        "\t\"service_duration\": 60,\n" +
        "\t\"description\": \"Cymbopogon, also known as lemongrass.  It is Good for Digestion, Full of antioxidants, Regulates High Blood Pressure, Boosts Metabolism and Burns Fat, For Naturally Great Skin and Hair\",\n" +
        "\t\"image\": \"lemongrass\",\n" +
        "\t\"service_price\": 1800.0,\n" +
        "\t\"service_title\": \"Lemongrass\"\n" +
        "}, {\n" +
        "\t\"id\": 52,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [{\n" +
        "\t\t\"id\": 144,\n" +
        "\t\t\"is_active\": true,\n" +
        "\t\t\"is_addon\": true,\n" +
        "\t\t\"is_featured\": false,\n" +
        "\t\t\"service_addon_price\": 0.0,\n" +
        "\t\t\"service_category_id\": 0,\n" +
        "\t\t\"service_duration\": 10,\n" +
        "\t\t\"service_id\": 22,\n" +
        "\t\t\"service_title\": \"Garden Plants Fertilizers\"\n" +
        "\t}, {\n" +
        "\t\t\"id\": 145,\n" +
        "\t\t\"is_active\": true,\n" +
        "\t\t\"is_addon\": true,\n" +
        "\t\t\"is_featured\": false,\n" +
        "\t\t\"service_addon_price\": 2500.0,\n" +
        "\t\t\"service_category_id\": 0,\n" +
        "\t\t\"service_duration\": 20,\n" +
        "\t\t\"service_id\": 22,\n" +
        "\t\t\"service_title\": \"Garden Plants Fertilizers\"\n" +
        "\t}],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Herbs and Fertlizers\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\": \"Most gardeners should use a complete fertilizer with twice as much phosphorus as nitrogen or potassium. An example would be 10-20-10 or 12-24-12. These fertilizers usually are easy to find. Some soils contain enough potassium for good plant growth and don't need more.\",\n" +
        "\t\"image\": \"garden\",\n" +
        "\t\"service_price\": 2500.0,\n" +
        "\t\"service_title\": \"Garden Plants Fertilizers\"\n" +
        "}, {\n" +
        "\t\"id\": 53,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Herbs and Fertlizers\",\n" +
        "\t\"service_duration\": 115,\n" +
        "\t\"description\": \"Organic fertilizers are fertilizers derived from animal matter, animal excreta, human excreta, and vegetable matter.  The types of fertilizers are Cottonseed meal. Molasses. Legume cover crops. Green manure cover crops.\",\n" +
        "\t\"image\": \"organic\",\n" +
        "\t\"service_price\": 1800.0,\n" +
        "\t\"service_title\": \"Organic Fertilizers\"\n" +
        "}, {\n" +
        "\t\"id\": 54,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Herbs and Fertlizers\",\n" +
        "\t\"service_duration\": 75,\n" +
        "\t\"description\": \"Inorganic fertilizer, also referred to as synthetic fertilizer, is manufactured artificially and contains minerals or synthetic chemicals. For example, synthetic nitrogen fertilizers are typically made from petroleum or natural gas.\",\n" +
        "\t\"image\": \"inorgani\",\n" +
        "\t\"service_price\": 1700.0,\n" +
        "\t\"service_title\": \"Inorganic Fertlizers\"\n" +
        "}, {\n" +
        "\t\"id\": 55,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Herbs and Fertlizers\",\n" +
        "\t\"service_duration\": 1,\n" +
        "\t\"description\": \"Biofertilizers are the substance that contains microbes, which helps in promoting the growth of plants, trees by increasing the supply of essential nutrients to the plants. It comprises living organisms which include mycorrhizal fungi, blue-green algae, and bacteria.\",\n" +
        "\t\"image\": \"bio\",\n" +
        "\t\"service_price\": 1800.0,\n" +
        "\t\"service_title\": \"Bio Fertilizers\"\n" +
        "}, {\n" +
        "\t\"id\": 56,\n" +
        "\t\"is_active\": true,\n" +
        "\t\"is_addon\": false,\n" +
        "\t\"is_featured\": false,\n" +
        "\t\"service_addons\": [],\n" +
        "\t\"service_category_id\": 3,\n" +
        "\t\"service_category_title\": \"Herbs and Fertlizers\",\n" +
        "\t\"service_duration\": 70,\n" +
        "\t\"description\": \"Liquid fertilizers are introduced into the soil to a specific depth by trailer or tractor-mounted machines with plows or cultivators.  The best organic liquid fertilizers are Humboldts Secret Golden Tree and SEA-90 Organic Fertilizer.\",\n" +
        "\t\"image\": \"organicliquid\",\n" +
        "\t\"service_price\": 1500.0,\n" +
        "\t\"service_title\": \"Organic Liquid Fertilizers\"\n" +
        "}]"
