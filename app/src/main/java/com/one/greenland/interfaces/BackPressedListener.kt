package com.one.greenland.interfaces

interface BackPressedListener {
    fun onFragmentBackPressed()
}