package com.one.greenland.interfaces

import com.one.greenland.models.DateModel

interface CalendarItemClickListener {
    fun dateClicked(date: DateModel)
}