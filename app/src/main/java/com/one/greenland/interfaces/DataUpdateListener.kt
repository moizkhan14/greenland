package com.one.greenland.interfaces

interface DataUpdateListener {
    fun onUpdateData()
}