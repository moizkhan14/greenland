package com.one.greenland.interfaces

import com.one.greenland.models.DialogFragmentButtonProperties

interface DialogFragmentClicks {
    fun onDialogFragmentViewClicked(dialogFragmentButtonProperties: DialogFragmentButtonProperties)
}