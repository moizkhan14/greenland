package com.one.greenland.interfaces

interface ItemClickListener {
    fun itemClicked(model: Any)
    fun itemClicked(clickId:Int, model: Any)
}