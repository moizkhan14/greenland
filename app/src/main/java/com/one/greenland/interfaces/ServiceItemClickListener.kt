package com.one.greenland.interfaces

interface ServiceItemClickListener {
    fun itemSelected(service: Any)
    fun itemDeselected(service: Any)
}