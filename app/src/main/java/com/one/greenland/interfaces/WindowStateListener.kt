package com.one.greenland.interfaces

interface WindowStateListener {
    fun isWindowOpen(isOpen: Boolean)
}