package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressModel(
    var address_1: String?,
    var address_2: String?,
    var address_title: String?,
    var country_name: String?,
    var id: Int?,
    var is_default: Boolean,
    var landmark: String?,
    var latitude: String?,
    var longitude: String?,
    var zip_code: String?,
    var city_id: String?,
    var area_id: String?,
    var city: CityModel?,
    var area: AreaModel?,
    @Transient
    var isSelected: Boolean = false
) : Parcelable

{
    constructor() : this(null, null, null, null, null, false, null, null, null, null, null
    , null, null, null, false)
}


