package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AreaModel(
    val area: String,
    val city_id: Int,
    val id: Int
) : Parcelable