package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CityModel(
    val city_name: String,
    val id: Int,
    var is_active: Boolean,

    @Transient
    var isSelected: Boolean
) : Parcelable