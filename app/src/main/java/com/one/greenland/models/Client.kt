package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Client(
    var approved_status: String?,
    var country_code: String?,
    var first_name: String?,
    var gender: String?,
    var id: Int?,
    var is_cnic_verified: Boolean?,
    var last_name: String?,
    var phone: String?,
    var profile_photo_url: String?,
    var profile_picture: String?,
    var user_details: UserDetail?,
    var user_name: String?
) : Parcelable