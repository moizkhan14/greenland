package com.one.greenland.models


class DateModel {
    var date: String? = null
    var day: String? = null
    var month: String? = null
    var fullDate: String? = null
    var isSelected: Boolean = false

}
