package com.one.greenland.models

data class DealCity(
    val city_id: Int,
    val deal_id: Int,
    val id: Int
)