package com.one.greenland.models

data class DealService(
    val deal_id: Int,
    val discounted_price: String,
    val id: Int,
    val service_id: Int
)