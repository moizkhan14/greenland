package com.one.greenland.models

data class DealsModel(
        var deal_banner: String?,
        val deal_cities: List<DealCity>?,
        val deal_end_date: String?,
        val deal_services: List<DealService>?,
        val deal_start_date: String?,
        val deal_status: String?,
        val deal_title: String?,
        val id: Int?,
        val is_active: Boolean?,
        val is_female_allowed: Boolean?,
        val is_male_allowed: Boolean?
) {
    constructor(deal_banner: String?) : this(null, null, null, null, null, null, null, null, null, null, null) {
        this.deal_banner = deal_banner
    }
}