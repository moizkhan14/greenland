package com.one.greenland.models

import android.view.View

data class DialogFragmentButtonProperties(
        val view: View,
        val firstButtonTag: String?
)