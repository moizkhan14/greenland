package com.one.greenland.models

import android.graphics.drawable.Drawable

data class MenuModel(val imageSrc: Drawable?, val menuItemName: String)