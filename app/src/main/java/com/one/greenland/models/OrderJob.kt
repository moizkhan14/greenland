package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderJob(
    val id: Int,
    val job_code: String,
    val start_time: String,
    val end_time: String,
    val waiting_charges: Int,
    val job_status: String,
    val job_amount: String,
    val job_travel_charges: String,
    val job_discount: String,
    val job_duration: String,
    val order_job_services: List<OrderJobService>,
    val technician: User,
    val technician_id: Int
) : Parcelable
