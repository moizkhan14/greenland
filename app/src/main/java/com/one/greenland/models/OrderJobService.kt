package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderJobService(
    val id: Int,
    val service_id: Int,
    val service_title: String
) : Parcelable