package com.one.greenland.models

data class OrderJobsAttribute(
    var comments: String,
    val id: Int,
    val job_rating: Double
)