package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderListingModel(
        var orders: MutableList<OrderModel>
):Parcelable