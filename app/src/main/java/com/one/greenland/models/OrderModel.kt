package com.one.greenland.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderModel(
        var actual_price: Double?,
        var address: AddressModel?,
        var address_id: Int?,
        var client: Client?,
        var discount: Double?,
        var id: Int?,
        var is_feedback_given: Boolean?,
        var order_date: String?,
        var order_jobs: List<OrderJob>?,
        @SerializedName("order_services_attributes", alternate = ["order_services"])var order_services: MutableList<OrderService>,
        var order_time: String?,
        var phone: String?,
        var special_notes: String?,
        var status: String?,
        var total_price: Double?,
        var travel_charges: Double?
) : Parcelable

{
    constructor() : this(null, null, null,null, null, null, null, null, null, mutableListOf(), null, null, null, null, null, null)
}