package com.one.greenland.models

import com.one.greenland.models.OrderModel

data class OrderRequest(
        var order_id: Int?,
        var order: OrderModel
)
{
    constructor(): this (null,OrderModel())
}