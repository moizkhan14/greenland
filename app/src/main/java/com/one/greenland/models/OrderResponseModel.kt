package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderResponseModel(
        var order: OrderModel?,
        var order_summary: OrderSummaryModel?
) : Parcelable

{
    constructor() : this(null, null)
}