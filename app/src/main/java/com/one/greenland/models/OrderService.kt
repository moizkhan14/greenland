package com.one.greenland.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderService(
        var client_id: Int?,
        var end_time: String?,
        var service_title: String?,
        var id: Int?,
        var job_code: String?,
        var order_id: Int?,
        @SerializedName("order_service_addons_attributes", alternate = ["order_service_addons"]) var order_service_addons: MutableList<OrderServiceAddon>,
        var service_date: String?,
        var service_id: Int?,
        var is_same_time: Boolean?,
        var start_time: String?,
        var technician_id: Int?,
        var total_price: Double,
        var unit_count: Int,
        var unit_price: Double,
        var service: Service?,
        var service_category_title: String?,
        var technician: User?
) : Parcelable
{
    constructor() : this( null, null , null, null, null,
            null, mutableListOf(), null, null, true,
            null, null, 0.0, 0, 0.0, null,null, null)
}
