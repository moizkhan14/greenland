package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderServiceAddon(
        var id: Int?,
        var service_id: Int?,
        var total_price: Double,
        var unit_count: Int,
        var unit_price: Double,
        var service_addon_title: String?
) : Parcelable

{
    constructor() : this(null,null, 0.0, 0, 0.0, null)
}