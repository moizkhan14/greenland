package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderSummaryModel(
    val discount: Double,
    val net_total: Double,
    val order_id: Int,
    val outstanding_balance: Double,
    val total_price: Double,
    val travel_charges: Double
) : Parcelable