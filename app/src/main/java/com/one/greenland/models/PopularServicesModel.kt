package com.one.greenland.models

data class PopularServicesModel(
        val service_category_title: String,
        val cover_image: String,
        val id: Int,
        val is_addon: Boolean,
        val description: String,
        val service_price: String,
        val service_title: String,
        val service_addons: MutableList<ServiceAddon>
)