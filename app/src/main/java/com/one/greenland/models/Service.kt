package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Service(
    val id: Int,
    val is_active: Boolean,
    val is_addon: Boolean,
    val is_featured: Boolean,
    var service_addons: List<ServiceAddon>?,
    val service_category_id: Int,
    val service_category_title: String,
    val service_duration: Int,
    val service_price: Double,
    val service_title: String,
    @Transient
    var isSelected: Boolean,
    var description: String,
    var image: String
) : Parcelable