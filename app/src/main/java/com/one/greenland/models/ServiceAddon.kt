package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ServiceAddon(
        val id: Int,
        val is_active: Boolean,
        val is_addon: Boolean,
        val is_featured: Boolean,
        val service_addon_price: Double,
        val service_category_id: Int,
        val service_duration: Int,
        val service_id: Int,
        val service_title: String
) : Parcelable