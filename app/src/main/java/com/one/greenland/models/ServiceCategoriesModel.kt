package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize

data class ServiceCategoriesModel(
        val id: Int,
        val category_svg_icon: Int,
        val service_category_title: String
) : Parcelable