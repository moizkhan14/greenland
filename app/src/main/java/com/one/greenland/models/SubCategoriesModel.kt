package com.one.greenland.models

data class SubCategoriesModel(
        var subcategories: MutableList<SubCategory>
)
{
    constructor() : this(mutableListOf()
    )
}