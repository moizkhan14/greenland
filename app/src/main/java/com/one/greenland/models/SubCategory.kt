package com.one.greenland.models

data class SubCategory(
    val description: String?,
    val id: Int?,
    val service_category_title: String?,
    var services: List<Service>
)

{
    constructor() : this(null,null,null, mutableListOf())
}