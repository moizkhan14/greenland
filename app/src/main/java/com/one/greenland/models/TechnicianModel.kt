package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TechnicianModel(
    val country_code: String,
    val first_name: String,
    val gender: String,
    val id: Int,
    val last_name: String,
    val phone: String,
    val profile_photo_url: String,
    val user_details: UserDetail,
    val user_name: String
) : Parcelable