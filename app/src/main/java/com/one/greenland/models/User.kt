package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
        var id: Int?,
        var email: String?,
        var user_name: String?,
        var first_name: String?,
        var last_name: String?,
        var password: String?,
        var password_confirmation: String?,
        var country_code: String?,
        var phone: String?,
        var phone_pin: String?,
        var forgot_password_code: String?,
        var auth_token: String?,
        var cnic: String?,
        var age_range: String?,
        var user_status: String?,
        var profile_photo_url: String?,
        var user_details: UserDetail?,
        var default_role: String?,
        var address: AddressModel?,
        var addresses: MutableList<AddressModel>,
        var orders: MutableList<OrderModel>,

        // @Transient
        var isLoggedIn: Boolean = false,
        // @Transient
        var latitude: String?,
        // @Transient
        var longitude: String?,
        // @Transient
        var cityId: String?,
        // @Transient
        var pushNotification: Boolean,
        // @Transient
        var gender: String?,
        // @Transient
        var cityName: String?,
        var chatMessages: MutableList<ChatMessage>?
) : Parcelable {
    constructor() : this(null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, mutableListOf(), mutableListOf(), false, null, null, null, false, null, null
    , mutableListOf())
}