package com.one.greenland.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserDetail(
        val hair_length: String,
        val hair_texture: String,
        val hair_type: String,
        val membership_code: String,
        val skin_type: String,
        val preferred_gender: String,
        val commission: Int,
        val contract_to: String,
        val contract_from: String,
        val beautician_code: String,
        val notes: String
) : Parcelable