package com.one.greenland.utils


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.CountDownTimer
import android.telephony.TelephonyManager
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.Fade
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.one.greenland.BuildConfig.DEBUG
import com.one.greenland.R
import com.one.greenland.common.*
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.interfaces.ServiceItemClickListener
import com.one.greenland.interfaces.WindowStateListener
import com.one.greenland.models.ServiceAddon
import com.one.greenland.models.User
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.activities.UserSessionActivity
import com.one.greenland.views.adapters.ServiceAddsOnAdapter

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

object AppUtils {

    var isPopularServiceDialogOpen = false


    fun viewFadeOutHiddenAnimation(targetView: View, parent: ViewGroup) {
        val transition: Transition = Fade()
        transition.duration = 300
        transition.addTarget(targetView)

        TransitionManager.beginDelayedTransition(parent, transition)
        targetView.visibility = View.GONE
    }

    fun viewFadeInVisibleAnimation(targetView: View, parent: ViewGroup) {
        val transition: Transition = Fade()
        transition.duration = 300
        transition.addTarget(targetView)

        TransitionManager.beginDelayedTransition(parent, transition)
        targetView.visibility = View.VISIBLE
    }

    fun showBottomNavigationView(mContext: Context?, bottomNavigationView: CardView) {
        bottomNavigationView.visibility = View.VISIBLE
        bottomNavigationView.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.slide_up))
    }

    fun hideBottomNavigationView(mContext: Context?, bottomNavigationView: CardView) {
        bottomNavigationView.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.slide_down))
        bottomNavigationView.visibility = View.GONE
    }

    fun DebugToast(context: Context, message: String) {
        if (DEBUG)
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun callIntent(context: Context, className: Class<*>) {
        val intent = Intent(context, className)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        ContextCompat.startActivity(context, intent, null)
    }

    fun callIntentClearTaskFlag(context: Context, className: Class<*>) {
        val intent = Intent(context, className)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        ContextCompat.startActivity(context, intent, null)
    }

    fun callIntentClearTaskFlagStringExtras(context: Context, className: Class<*>, key: String, string: String) {
        val intent = Intent(context, className)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(key, string)
        ContextCompat.startActivity(context, intent, null)
    }

    fun callIntentStringExtras(context: Context, className: Class<*>, key: String, string: String) {
        val intent = Intent(context, className)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(key, string)
        ContextCompat.startActivity(context, intent, null)
    }

    fun callIntentBundleExtras(context: Context, className: Class<*>, bundle: Bundle) {
        val intent = Intent(context, className)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(BUNDLE_KEY, bundle)
        ContextCompat.startActivity(context, intent, null)
    }

    fun getCountryCode(context: Context): String {
        val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val countryCodeValue: Int = PhoneNumberUtil.getInstance().getCountryCodeForRegion(tm.networkCountryIso.toUpperCase(Locale.US))
        var countryCode: String = "+" + PhoneNumberUtil.getInstance().getCountryCodeForRegion(tm.networkCountryIso.toUpperCase(Locale.US))
        if (countryCodeValue == 0) {
            countryCode = "+92"
        }
        return countryCode
    }

    fun getFormatedDate(strDate: String, format: String): String {
        var dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val date = dateFormat.parse(strDate)
        dateFormat = SimpleDateFormat(format, Locale.US)
        return dateFormat.format(date!!)
    }

    fun isValidEditText(editText: EditText, error: String): Boolean {
        if (editText.text.isNullOrEmpty()) {
            editText.error = error
            return false
        }
        return true
    }

    fun isValidFullName(context: Context, etFullName: EditText): Boolean {
        if (!isValidEditText(etFullName, context.getString(R.string.name_required)))
            return false

        if (!etFullName.text.toString().trim().contains(" ")) {
            etFullName.error = context.getString(R.string.name_required)
            return false
        }

        return true
    }

    fun isValidCNIC(context: Context, cnic: EditText): Boolean {
        if (!isValidEditText(cnic, context.getString(R.string.cnic_required_text)))
            return false
        try {
            val cnicString =
                    cnic.text.toString().split("-")[0] + cnic.text.toString().split("-")[1] + cnic.text.toString().split("-")[2]


            if (cnicString.length < CNIC_LENGTH) {
                cnic.error = context.getString(R.string.cnic_length_text)
                return false
            }
        } catch (ex: Exception) {
            cnic.error = context.getString(R.string.cnic_sample_number_text)
            return false
        }
        return true
    }

    fun isValidAddress(address: EditText): Boolean {
        try {
            val pattren = Pattern.compile("^(?!\\d+\$)(?:[a-zA-Z0-9][a-zA-Z0-9 ,.#@&\$]*)?\$")
            val matcher: Matcher = pattren.matcher(address.text.toString())
            if (!matcher.matches()) {
                address.error = "Invalid address. The address must contain alphabets and numbers"
                return false
            }
        } catch (e: java.lang.Exception) {
            address.error = "Invalid address. The address must contain alphabets and numbers"
            return false
        }
        return true
    }

    fun isValidPhone(context: Context, etPhoneNumber: EditText): Boolean {
        if (!isValidEditText(etPhoneNumber, context.getString(R.string.phone_required_text))) {
            return false
        }

        try {
            val phone = if (etPhoneNumber.text.toString().contains("+92")) {
                etPhoneNumber.text.toString().split(" ")[1] + etPhoneNumber.text.toString().split(" ")[2] + etPhoneNumber.text.toString().split(" ")[3]
            } else
                etPhoneNumber.text.toString()
            val p: Pattern = Pattern.compile("(0/+92)?[3][0-9]{9}")
            val m: Matcher = p.matcher(phone)

            if (!m.matches()) {
                etPhoneNumber.error = if (etPhoneNumber.text.toString().contains("+92")) context.getString(R.string.invalid_phone_with_country_code) else context.getString(R.string.invalid_phone)
                return false
            }
        } catch (ex: Exception) {
            etPhoneNumber.error = if (etPhoneNumber.text.toString().contains("+92")) context.getString(R.string.invalid_phone_with_country_code) else context.getString(R.string.invalid_phone)
            return false
        }
        return true
    }

    fun isValidPassword(context: Context, password: EditText): Boolean {
        if (!isValidEditText(password, context.getString(R.string.password_required_text)))
            return false

        if (password.text.contains(" ")) {
            password.error = context.getString(R.string.password_white_space_text)
            return false
        } else if (password.text.toString().length < PASSWORD_LENGTH) {
            password.error = context.getString(R.string.password_length_text)
            return false
        }


        return true
    }

    private fun saveUserListInPreferences(user: MutableList<User>) {
        PreferenceUtils.putModelList(REGISTERED_USER_LIST, user)
    }

    fun logOut(context: Context, user: User) {
        val list = PreferenceUtils.getModelForKey(REGISTERED_USER_LIST)
        val listType =
                object : TypeToken<MutableList<User>>() {

                }.type

        val users = Gson().fromJson<MutableList<User>>(
                list,
                listType
        )
        if (!users.isNullOrEmpty()) {

            for (index in 0 until users.size) {
                if (users[index].phone == user.phone) {
                    users[index] = user
                    saveUserListInPreferences(users)
                    break
                }
            }
        }
        PreferenceUtils.removeStoredValue(USER_MODEL)
        (context as HomeActivity).clearCart()

        AppUtils.callIntentClearTaskFlag(context, UserSessionActivity::class.java)
    }

    fun isPasswordMatched(context: Context, password: EditText, confirmPassword: EditText): Boolean {
        if (confirmPassword.text.toString() != password.text.toString()) {
            confirmPassword.error = context.getString(R.string.password_does_not_match)
            return false
        }

        return true
    }


    fun startTimer(context: Context, tvResendTime: TextView, ivResendCode: ImageView, tvResendCode: TextView) {
        tvResendTime.visibility = View.VISIBLE
        ivResendCode.visibility = View.GONE
        tvResendCode.text = context.getString(R.string.resend_code)
        object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                tvResendTime.text = " " + (millisUntilFinished / 1000) + "s"
            }

            override fun onFinish() {
                ivResendCode.visibility = View.VISIBLE
                tvResendTime.visibility = View.GONE
                tvResendCode.text = context.getString(R.string.resend_code_text)
            }

        }.start()
    }

    fun showAlertDialog(
            activity: Activity,
            imageSrc: Drawable,
            description: String
    ) {
        val mBuilder = AlertDialog.Builder(activity)

        val view = activity.layoutInflater.inflate(R.layout.dialog_info, null)

        mBuilder.setView(view)
        val dialog = mBuilder.create()
        dialog.setCancelable(false)
        val btnOk = view.findViewById<Button>(R.id.okButton)
        val descriptionTextView = view.findViewById<TextView>(R.id.descriptionTextView)
        val dialogImageView = view.findViewById<ImageView>(R.id.dialogImageView)
        val cvCancel = view.findViewById<CardView>(R.id.cancelCardView)
        cvCancel.visibility = View.VISIBLE

        descriptionTextView!!.text = description

        Glide.with(activity).load(imageSrc).into(dialogImageView)

        cvCancel.setOnClickListener {
            dialog.dismiss()
        }
        btnOk.setOnClickListener {
            dialog.dismiss()
        }

        dialog.window!!.setGravity(Gravity.BOTTOM)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        if (description.trim() == "null") {
            dialog.dismiss()
        }
    }

    fun showErrorDialog(activity: Activity, message: String) {
        showAlertDialog(activity, ContextCompat.getDrawable(activity, R.drawable.ic_error)!!, message)
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun hideKeyboardFromFragment(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showAddOnDialog(context: Context, serviceAddOn: List<ServiceAddon>, serviceId: Int,
                        dataUpdateListener: DataUpdateListener, serviceItemClickListener: ServiceItemClickListener, windowListener: WindowStateListener) {
        val mBuilder = AlertDialog.Builder(context)
        val view = (context as Activity).layoutInflater.inflate(R.layout.dialog_add_ons, null)

        mBuilder.setView(view)
        val dialog = mBuilder.create()
        val ivCancel = view.findViewById<ImageView>(R.id.ivCancel)
        val rvAddOn = view.findViewById<RecyclerView>(R.id.rvAddOn)
        windowListener.isWindowOpen(true)
        dialog.setCancelable(false)


        ivCancel.setOnClickListener {
            windowListener.isWindowOpen(false)
            dialog.dismiss()
        }

        rvAddOn.layoutManager = LinearLayoutManager(context)

        rvAddOn.adapter = ServiceAddsOnAdapter(context, serviceAddOn, view, serviceId, dataUpdateListener, serviceItemClickListener)

        dialog.window!!.setGravity(Gravity.BOTTOM)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    fun showUserAlreadyExitsDialog(
            context: Context,
            phoneNumber: String
    ) {
        val mBuilder = AlertDialog.Builder(context)

        val view = (context as Activity).layoutInflater.inflate(R.layout.dialog_user_already_exits, null)

        mBuilder.setView(view)
        val dialog = mBuilder.create()
        dialog.setCancelable(false)
        val btnSignIn = view.findViewById<Button>(R.id.btnSignIn)
        val tvPhoneNumber = view.findViewById<TextView>(R.id.tvPhoneNumber)

        tvPhoneNumber.text = "+92$phoneNumber"

        btnSignIn.setOnClickListener {
            callIntentClearTaskFlagStringExtras(context, UserSessionActivity::class.java, USER_SESSION_INTENT_EXTRAS, "SignIn")
            dialog.dismiss()
        }

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    fun getRandomString(): String {
        var count = 15
        val builder = StringBuilder()

        while (count-- != 0) {
            val character = (Math.random() * ALPHA_NUMERIC_STRING.length).toInt()
            builder.append(ALPHA_NUMERIC_STRING[character])
        }

        return builder.toString()
    }

}