package com.one.greenland.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.TextUtils
import android.util.Base64
import androidx.preference.PreferenceManager
import com.one.greenland.applications.ApplicationManager
import com.google.gson.Gson
import com.one.greenland.models.AddressModel
import com.one.greenland.models.User
import java.io.ByteArrayOutputStream
import java.util.*

/**
 * Created by umair on 1/1/18.
 */
class PreferenceUtils {
    fun checkForNullValue(value: String?) {
        if (value == null) {
            throw NullPointerException()
        }
    }

    companion object {
        fun setValueForKey(key: String, value: String?) {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.instance!!.context)
            val editor = sharedPref.edit()
            editor.putString(key, value)
            editor.apply()
        }

        fun setValueForKey(key: String, value: Boolean?) {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.instance!!.context)
            val editor = sharedPref.edit()
            editor.putBoolean(key, value!!)
            editor.apply()
        }

        fun setValueForKey(key: String, value: Int) {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.instance!!.context)
            val editor = sharedPref.edit()
            editor.putInt(key, value)
            editor.apply()
        }

        fun removeStoredValue(key: String){
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.instance!!.context)
            val editor = sharedPref.edit()
            editor.remove(key)
            editor.apply()
        }

        fun getStringValueForKey(key: String): String? {
            return getValueForKey(key, null)
        }

        fun getIntValueForKey(key: String): Int {
            return getValueForKey(key, 0)
        }

        fun getValueForKey(key: String, defaultValue: String?): String? {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.instance!!.context)
            return sharedPref.getString(key, defaultValue)
        }

        fun getValueForKey(key: String, defaultValue: Boolean): Boolean {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.instance!!.context)
            return sharedPref.getBoolean(key, defaultValue)
        }

        fun getValueForKey(key: String, defaultValue: Int): Int {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.instance!!.context)
            return sharedPref.getInt(key, defaultValue)
        }

        fun saveBitmapImage(key: String, bitmap: Bitmap) {
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val b = baos.toByteArray()
            val encodedImage = Base64.encodeToString(b, Base64.DEFAULT)
            setValueForKey(key, encodedImage)
        }

        fun getBitmapImage(key: String): Bitmap? {
            val previouslyEncodedImage = getValueForKey(key, "")
            if (previouslyEncodedImage != null && previouslyEncodedImage != "") {
                val b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT)
                return BitmapFactory.decodeByteArray(b, 0, b.size)
            }
            return null
        }

        fun getModelForKey(key: String): String? {
            return getValueForKey(key, null)
        }

        fun putModel(key: String, model: Any?) {
            val gson = Gson()
            setValueForKey(key, gson.toJson(model))
        }
        fun putModelList(key: String, model: MutableList<User>) {
            val gson = Gson()
            setValueForKey(key, gson.toJson(model))
        }
        fun putModelListAddress(key: String, model: MutableList<AddressModel>) {
            val gson = Gson()
            setValueForKey(key, gson.toJson(model))
        }

        fun getListString(key: String): ArrayList<String> {
            return ArrayList(Arrays.asList(*TextUtils.split(getValueForKey(key, ""), "‚‗‚")))
        }

        fun putListString(key: String, stringList: ArrayList<String>) {
            checkForNullKey(key)
            val myStringList = stringList.toTypedArray()
            setValueForKey(key, TextUtils.join("‚‗‚", myStringList))
        }

        fun checkForNullKey(key: String?) {
            if (key == null) {
                throw NullPointerException()
            }
        }
    }
}