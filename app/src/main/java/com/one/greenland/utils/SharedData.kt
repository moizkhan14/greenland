package com.one.greenland.utils

import java.util.*

class SharedData {

    companion object {
        var ourInstance = SharedData()

        fun getInstance(): SharedData? {
            return ourInstance
        }
    }


    public var bottomNavEntries: ArrayList<String>? = null

    fun getMyBottomNavEntries(): ArrayList<String>? {
        if (bottomNavEntries == null) bottomNavEntries = ArrayList()
        return bottomNavEntries
    }

    fun setMyBottomNavEntries(bottomNavEntries: ArrayList<String>?) {
        this.bottomNavEntries = bottomNavEntries
    }
}