/*
package com.onebyte.gharpar.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.onebyte.gharpar.R
import com.onebyte.gharpar.SplashActivity
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import com.onesignal.OneSignal


class SilentNotificationReceiver : NotificationExtenderService() {
    override fun onNotificationProcessing(notification: OSNotificationReceivedResult?): Boolean {

        OneSignal.clearOneSignalNotifications()
        if (notification != null) {
            var parent_type = ""
            var parent_id = ""
            val title: String = notification.payload.title
            val body: String = notification.payload.body
            val additionalData = notification.payload.additionalData
            if (additionalData.has("parent_type")) {
                parent_type = additionalData.get("parent_type").toString()
            }
            if (additionalData.has("parent_id")) {
                parent_id = additionalData.get("parent_id").toString()
            }
            println("notification data = $parent_type and $parent_id")
            Log.d("Notification Data: ", notification.payload.additionalData.toString())
            sendNotification(body, title, parent_type, parent_id)
        }

        return true
    }

    private fun sendNotification(messageBody: String, title: String, parentType: String, parentId: String) {
        val mTitle = title ?: "GharPar"

        val intent = Intent(applicationContext, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        if (parentType.isNotEmpty()) {
            intent.putExtra("parent_type", parentType)
            intent.putExtra("pushnotification", "yes")
        }
        if (parentId.isNotEmpty()) {
            intent.putExtra("parentId", parentId)
        }
        val pendingIntent = PendingIntent.getActivity(this, 0 */
/* Request code *//*
, intent,
                PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_UPDATE_CURRENT)

        val channelId = "GharPar"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(mTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setStyle(NotificationCompat.BigTextStyle().bigText(messageBody))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)


        val notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0 */
/* ID of notification *//*
, notificationBuilder.build())
    }
}*/
