package com.one.greenland.views.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.one.greenland.R
import com.one.greenland.views.fragments.BaseFragment
import com.one.greenland.views.fragments.HomeFragment
import java.lang.Exception

abstract class BaseActivity : AppCompatActivity() {
    var currentFragment: BaseFragment? = null
    var callingFragment: String? = null
    private var arguments: Bundle? = null
    override fun onResume() {
        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getRootLayoutId() == 0) throw NullPointerException("No root layout id returned in getRootLayoutId. Please return a layout to set it in the onCreate of the BaseActivityClass") else setContentView(getRootLayoutId())
        changeStatusBarIconColors()
        arguments = intent.extras
        if (arguments != null) {
            receiveExtras(arguments)
        }
    }

    private fun changeStatusBarIconColors() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = resources.getColor(R.color.colorGreen);
        }
    }

    abstract fun getRootLayoutId(): Int

    abstract fun getActivityName(): String?

    abstract fun receiveExtras(arguments: Bundle?)

    fun changeFragment(baseFragment: BaseFragment, callingFragment: String, layoutId: Int, isAnimate: Boolean) {
        currentFragment = baseFragment
        this.callingFragment = callingFragment
        val transaction = supportFragmentManager.beginTransaction()
        if (isAnimate)
            transaction.setCustomAnimations(
                    R.anim.slide_in_right,
                    R.anim.slide_out_left,
                    R.anim.slide_in_left,
                    R.anim.slide_out_right
            )
        if (baseFragment.addToBackStack()) {
            if (supportFragmentManager.backStackEntryCount == 0)
                transaction.addToBackStack(HomeFragment().getFragmentName())
            transaction.add(layoutId, baseFragment, baseFragment.getFragmentName())
            transaction.addToBackStack(baseFragment.getFragmentName())
            transaction.commit()
        } else supportFragmentManager.beginTransaction().replace(layoutId, baseFragment, baseFragment.getFragmentName()).commit()
    }

    fun changeBackFragment(baseFragment: BaseFragment, callingFragment: String, layoutId: Int) {
        try {
            currentFragment = baseFragment
            this.callingFragment = callingFragment
//        if (baseFragment.addToBackStack()) supportFragmentManager.beginTransaction().replace(layoutId, baseFragment, baseFragment.getFragmentName()).addToBackStack(baseFragment.getFragmentName()).commit()
//        else
            if (baseFragment.getFragmentName() != HomeFragment().getFragmentName() || supportFragmentManager.backStackEntryCount > 1)
                supportFragmentManager.popBackStack()
            supportFragmentManager.beginTransaction().replace(layoutId, baseFragment, baseFragment.getFragmentName()).commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    val latestFragmentFromBackStack: BaseFragment?
        get() {
            val index = supportFragmentManager.backStackEntryCount - 2
            return if (index < 0 || supportFragmentManager.backStackEntryCount == 0) null else {
                val backEntry = supportFragmentManager.getBackStackEntryAt(index)
                val tag = backEntry.name
                supportFragmentManager.findFragmentByTag(tag) as BaseFragment?
            }
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (currentFragment != null) {
            currentFragment!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (currentFragment != null) currentFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onBackPressed() { //super.onBackPressed();
        try {
            if (currentFragment != null) currentFragment!!.onBackPressed() else onSuperBackPressed()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun onSuperBackPressed() {
        try {
            currentFragment = latestFragmentFromBackStack
            if (currentFragment != null && callingFragment != null) {
                changeBackFragment(currentFragment!!, callingFragment!!, R.id.containerMainFragment)
            } else {
                super.onBackPressed()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}