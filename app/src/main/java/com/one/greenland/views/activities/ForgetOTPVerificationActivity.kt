package com.one.greenland.views.activities

import android.animation.LayoutTransition
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.one.greenland.R

import com.one.greenland.common.PHONE_NUMBER
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.getCountryCode
import com.one.greenland.utils.AppUtils.hideKeyboard
import kotlinx.android.synthetic.main.activity_forget_otp_verification.*
import kotlinx.android.synthetic.main.activity_otp_verification.ivResendCode
import kotlinx.android.synthetic.main.activity_otp_verification.textPinEntry
import kotlinx.android.synthetic.main.activity_otp_verification.tvResendCode
import kotlinx.android.synthetic.main.activity_otp_verification.tvResendTime

class ForgetOTPVerificationActivity : BaseActivity(), View.OnClickListener {
    private var phoneNumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializer()
        listenersInitializer()
        AppUtils.startTimer(this, tvResendTime, ivResendCode, tvResendCode)
    }

    private fun initializer() {
        (container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
        tvPhoneNumber.text = getCountryCode(this) + phoneNumber
    }

    private fun listenersInitializer() {
        forwardConstraintLayout.setOnClickListener(this)
        textPinEntry.setOnPinEnteredListener(PinEntryEditText.OnPinEnteredListener {
            verifyPin()
        })
        ivResendCode.setOnClickListener(this)
    }

    private fun pinVerification(pin: String) {
        showProgressBar()
        if (pin == "1122") {
            AppUtils.callIntentStringExtras(this, UpdatePasswordActivity::class.java, PHONE_NUMBER, phoneNumber!!)
            finish()
        }
    }

    private fun resendCode() {
        Handler(Looper.getMainLooper()).postDelayed({
            hideProgressBar()
            Toast.makeText(this, "Code has been sent", Toast.LENGTH_SHORT).show()
        }, 2000)
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_forget_otp_verification
    }

    override fun getActivityName(): String? {
        return "Pin Verification"
    }

    override fun receiveExtras(arguments: Bundle?) {
        if (!arguments!!.getString(PHONE_NUMBER).isNullOrEmpty()) phoneNumber = arguments.getString(PHONE_NUMBER)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.forwardConstraintLayout -> {
                        verifyPin()
                }

                R.id.ivResendCode -> {
                        showProgressBar()
                        resendCode()

                }
            }
        }
    }

    private fun verifyPin() {
        hideKeyboard(this)
        if (textPinEntry.text.toString().isEmpty() || textPinEntry.text.toString().length < 4) {
            hideProgressBar()
            Toast.makeText(
                    this,
                    "Please enter 4 digit verification code!!!",
                    Toast.LENGTH_SHORT
            ).show()
        } else {
            pinVerification(textPinEntry.text.toString())
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun showProgressBar() {
        forwardConstraintLayout.background = ContextCompat.getDrawable(this, R.drawable.background_circle)
        forwardProgressBar.visibility = View.VISIBLE
        forwardImageView.visibility = View.GONE
    }

    private fun hideProgressBar() {
        forwardConstraintLayout.background = ContextCompat.getDrawable(this, R.drawable.round_edge_rectangle)
        forwardProgressBar.visibility = View.GONE
        forwardImageView.visibility = View.VISIBLE
    }
}
