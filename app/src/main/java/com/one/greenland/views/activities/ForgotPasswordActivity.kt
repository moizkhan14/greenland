package com.one.greenland.views.activities

import android.animation.LayoutTransition
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import com.kaopiz.kprogresshud.KProgressHUD
import com.one.greenland.R

import com.one.greenland.common.PHONE_NUMBER
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.hideKeyboard
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : BaseActivity(), View.OnClickListener {

    private var kProgressHud: KProgressHUD? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializer()
        listenersInitializer()
        imeOption()
    }

    private fun initializer() {
        (container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
        kProgressHud = KProgressHUD(this)
        kProgressHud!!.setLabel("Please wait!!!")
    }

    private fun imeOption() {
        etPhoneNumber.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard(this)
                if (phoneValidation())
                    forgetPassword(etPhoneNumber.text.toString().trim())
            }
            false
        }
    }

    private fun listenersInitializer() {
        forwardImageButton.setOnClickListener(this)
        ivBack.setOnClickListener(this)
    }

    private fun forgetPassword(phone: String) {
        kProgressHud!!.show()
        // forgotPasswordAPIRequest.forgetPassword(this, this, phone)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.forwardImageButton -> {
                    hideKeyboard(this)
                    if (phoneValidation())
                        forgetPassword(etPhoneNumber.text.toString().trim())
                }

                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_forgot_password
    }

    override fun getActivityName(): String? {
        return "Forgot Password Activity"
    }

    override fun receiveExtras(arguments: Bundle?) {
        if (!arguments!!.getString(PHONE_NUMBER).isNullOrEmpty()) etPhoneNumber.setText(arguments.getString(PHONE_NUMBER))
    }

    private fun phoneValidation(): Boolean {
        val animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        if (!AppUtils.isValidPhone(this, etPhoneNumber)) {
            etPhoneNumber.requestFocus()
            etPhoneNumber.startAnimation(animShake)
            return false
        }
        return true
    }

}
