package com.one.greenland.views.activities

//import com.onesignal.OneSignal
import android.animation.LayoutTransition
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.amazonaws.mobile.client.AWSMobileClient
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.Gson
import com.one.greenland.R
import com.one.greenland.common.BUNDLE_KEY
import com.one.greenland.common.ORDER_CART
import com.one.greenland.common.USER_MODEL
import com.one.greenland.models.*
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.getRandomString
import com.one.greenland.utils.AppUtils.hideKeyboard
import com.one.greenland.utils.AppUtils.showErrorDialog
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.views.fragments.*
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity(), View.OnClickListener {

    // User objects
    private var user: User? = null

    //var signedInUser: User? = null
    var updateUser: User? = null

    var isUserNeedToUpdateOnServer: Boolean = false

    //Items
    var selectedAgeRange = ""
    var selectedGender = ""

    // Order Response Model
    var orderRequest = OrderRequest()
    var orderModel = OrderModel()
    var orderServiceList: MutableList<OrderService> = mutableListOf()
    var popularServiceOrderServiceAddons: MutableList<OrderServiceAddon> = mutableListOf()

    //Item Ids
    var serviceItemId = 0

    // Update profile instance
    var fragmentUpdateProfileInstance: FragmentUpdateProfile? = null

    private val constraintSet: ConstraintSet = ConstraintSet()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
        initialize()
        registerClicks()
        initializeAWS()
        updateUser = Gson().fromJson(PreferenceUtils.getModelForKey(USER_MODEL), User::class.java)

        getCartOrderRequest()
        orderModel = orderRequest.order
        orderServiceList = orderRequest.order.order_services!!

        updateHomeCartBadge()

        if (intent.hasExtra(BUNDLE_KEY)) {
            val bundle = intent.getBundleExtra(BUNDLE_KEY)
            val parent_type = bundle!!.getString("parent_type")!!.toLowerCase()
            //Toast.makeText(this, "Bundle Recieved:  $parent_type", Toast.LENGTH_SHORT).show()
            when (parent_type) {
                "order" -> {
                    val id: Int = bundle.getString("parentId")!!.toInt()
                    // loadFragment("Order Details", OrderDetailsFragment(), false)
                    selectedBottomNavigation(R.id.bottomNavigationItemBooking)
                }
                else -> {
                    loadFragment("", HomeFragment.homeFragment(), false)
                }
            }
        } else {
            loadFragment("", HomeFragment.homeFragment(), false)
        }

    }

    fun registerClicks() {
        bottomNavigationItemHome.setOnClickListener(this)
        bottomNavigationItemBooking.setOnClickListener(this)
        bottomNavigationItemCart.setOnClickListener(this)
//        bottomNavigationItemCartImage.setOnClickListener(this)
        bottomNavigationItemMore.setOnClickListener(this)
    }

    fun focusListener() {
        bottomNavigationItemHome.isFocusable = true
        bottomNavigationItemBooking.isFocusable = true
        bottomNavigationItemCartImage.isFocusable = true
        bottomNavigationItemCart.isFocusable = true
        bottomNavigationItemMore.isFocusable = true
    }

    private fun initializeAWS() {

//        applicationContext.startService(Intent(applicationContext, TransferService::class.java))

        AWSMobileClient.getInstance().initialize(this).execute()
    }

    fun changeTint(icon: Int, color: Int) {
        val unwrappedDrawable = AppCompatResources.getDrawable(this, icon)
        val wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable!!)
        DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(this, color))
    }

    fun getAppUser(): User {
        getUserModelFromPreferences()
        return if (user == null) User() else user!!
    }

    fun setAppUser(user: User) {
        this.user = user
        saveUserPreferences()
    }

    fun getCartOrderRequest(): OrderRequest {
        getCartOrderFromPreferences()
        return orderRequest
    }

    fun setCartOrderRequest(orderRequest: OrderRequest) {
        this.orderRequest = orderRequest
        saveOrderCartPreferences()
    }

    private fun saveOrderCartPreferences() {
        PreferenceUtils.putModel(ORDER_CART, orderRequest)
    }

    private fun getCartOrderFromPreferences() {
        val savedCart = Gson().fromJson(PreferenceUtils.getModelForKey(ORDER_CART), OrderRequest::class.java)
        if (savedCart != null)
            orderRequest = savedCart
    }

    private fun saveUserPreferences() {
        PreferenceUtils.putModel(USER_MODEL, user!!)

    }

    private fun getUserModelFromPreferences() {
        user = Gson().fromJson(PreferenceUtils.getModelForKey(USER_MODEL), User::class.java)
    }

    fun clearCart() {
        orderRequest = OrderRequest()
        orderModel = OrderModel()
        orderServiceList = mutableListOf()
        orderModel.address = AddressModel()
        PreferenceUtils.removeStoredValue(ORDER_CART)
        updateHomeCartBadge()
    }

    private fun initialize() {
        (bottomNavigationConstraintLayout as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)

        if (bottomNavigationView.visibility != View.VISIBLE)
            AppUtils.showBottomNavigationView(this, bottomNavigationView)

        getUserModelFromPreferences()
    }

    private fun loadFragment(callingFragment: String, fragment: BaseFragment, isAnimate: Boolean) {
        changeFragment(fragment, callingFragment, R.id.containerMainFragment, isAnimate)
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard(this)
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun getActivityName(): String? {
        return "Home Activity"
    }

    override fun receiveExtras(arguments: Bundle?) {
    }

    fun selectedBottomNavigation(viewId: Int) {
        constraintSet.clone(bottomNavigationConstraintLayout)

        constraintSet.connect(
                R.id.bottomNavigationItemSelector,
                ConstraintSet.START,
                viewId,
                ConstraintSet.START
        )

        constraintSet.connect(
                R.id.bottomNavigationItemSelector,
                ConstraintSet.END,
                viewId,
                ConstraintSet.END
        )

        if (viewId != R.id.bottomNavigationItemHome)
            constraintSet.constrainPercentWidth(R.id.bottomNavigationItemHome, 0.21f)
        if (viewId != R.id.bottomNavigationItemBooking)
            constraintSet.constrainPercentWidth(R.id.bottomNavigationItemBooking, 0.21f)
        if (viewId != R.id.bottomNavigationItemCart)
            constraintSet.constrainPercentWidth(R.id.bottomNavigationItemCart, 0.21f)
        if (viewId != R.id.bottomNavigationItemMore)
            constraintSet.constrainPercentWidth(R.id.bottomNavigationItemMore, 0.21f)

        constraintSet.constrainPercentWidth(viewId, 0.35f)

        constraintSet.applyTo(bottomNavigationConstraintLayout)

        bottomNavigationItemHomeImage.setImageResource(R.drawable.ic_menu_home_inactive)
        bottomNavigationItemBookingImage.setImageResource(R.drawable.ic_menu_booking_inactive)
        bottomNavigationItemCartImage.setImageResource(R.drawable.ic_menu_cart_inactive)
        bottomNavigationItemMoreImage.setImageResource(R.drawable.ic_menu_more_inactive)

        bottomNavigationItemHomeText.visibility = View.GONE
        bottomNavigationItemBookingText.visibility = View.GONE
        bottomNavigationItemCartText.visibility = View.GONE
        bottomNavigationItemMoreText.visibility = View.GONE

        when (viewId) {
            R.id.bottomNavigationItemHome -> {
                bottomNavigationItemHomeImage.setImageResource(R.drawable.ic_menu_home_active)
                bottomNavigationItemHomeText.visibility = View.VISIBLE
            }
            R.id.bottomNavigationItemBooking -> {
                bottomNavigationItemBookingImage.setImageResource(R.drawable.ic_menu_booking_active)
                bottomNavigationItemBookingText.visibility = View.VISIBLE
            }
            R.id.bottomNavigationItemCart -> {
                bottomNavigationItemCartImage.setImageResource(R.drawable.ic_menu_cart_active)
                bottomNavigationItemCartText.visibility = View.VISIBLE
            }
            R.id.bottomNavigationItemMore -> {
                bottomNavigationItemMoreImage.setImageResource(R.drawable.ic_menu_more_active)
                bottomNavigationItemMoreText.visibility = View.VISIBLE
            }
        }
    }

    fun updateHomeCartBadge() {
        if (orderServiceList.size > 0) {
            homeCartBadge.visibility = View.VISIBLE
            tvHomeCartBadge.text = orderServiceList.size.toString()
        } else {
            homeCartBadge.visibility = View.GONE
        }
    }

    override fun onClick(view: View?) {
        view?.let {
            selectedBottomNavigation(it.id)
            when (it.id) {
                R.id.bottomNavigationItemHome -> {
                    loadFragment("Home Fragment", HomeFragment.homeFragment(), false)
                }

                R.id.bottomNavigationItemBooking -> {
                    loadFragment(HomeFragment.homeFragment().getFragmentName()!!, BookingFragment.bookingFragment(), false)
                }

                R.id.bottomNavigationItemCart -> {
                    loadFragment(HomeFragment.homeFragment().getFragmentName()!!, CartFragment(), false)
                }

                R.id.bottomNavigationItemCartImage -> {
                    loadFragment(HomeFragment.homeFragment().getFragmentName()!!, CartFragment(), false)
                }

                R.id.bottomNavigationItemMore -> {
                    loadFragment(HomeFragment.homeFragment().getFragmentName()!!, MenuFragment.menuFragment(), false)
                }
            }
        }
    }

    fun openImagePicker(fragmentUpdateProfileInstance: FragmentUpdateProfile) {
        this.fragmentUpdateProfileInstance = fragmentUpdateProfileInstance
        ImagePicker.with(this)
                .cropSquare()
                .compress(1024)
                .maxResultSize(1080, 1080)
                .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {

                //You can also get File Path from intent
                val filePath: String? = ImagePicker.getFilePath(data)

                val serverFilePath = "uploads/user/profile_picture/" +
                        "${this.user?.id}/" +
                        "${getRandomString()}/" +
                        "${filePath?.substring(filePath.lastIndexOf("/") + 1)}"

                fragmentUpdateProfileInstance?.let { fragmentUpdateProfile ->
                    filePath?.let {
                        fragmentUpdateProfile.localPhotoPath = filePath
                        fragmentUpdateProfile.serverPhotoPath = serverFilePath
                        fragmentUpdateProfile.setLocalPathPhotoInImageView(filePath)
                    }
                }
            }
            ImagePicker.RESULT_ERROR -> {
//                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                showErrorDialog(this, "Cannot read photo. Please try different.")
            }
            else -> {
//                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
