package com.one.greenland.views.activities

import android.animation.LayoutTransition
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.one.greenland.R

import com.one.greenland.common.PHONE_NUMBER
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.getCountryCode
import com.one.greenland.utils.AppUtils.hideKeyboard
import com.one.greenland.utils.AppUtils.startTimer
import kotlinx.android.synthetic.main.activity_otp_verification.*

class OTPVerificationActivity : BaseActivity(), View.OnClickListener {

    private var phoneNumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializer()
        listenersInitializer()
        startTimer(this, tvResendTime, ivResendCode, tvResendCode)
    }

    private fun initializer() {
        (container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
        tvPhoneNumber.text = getCountryCode(this) + phoneNumber
    }

    private fun listenersInitializer() {
        forwardConstraintLayout.setOnClickListener(this)
        textPinEntry.setOnPinEnteredListener(PinEntryEditText.OnPinEnteredListener {
            verifyPin()
        })
        ivResendCode.setOnClickListener(this)
    }

    private fun pinVerification(pin: String) {
        showProgressBar()
        if (pin == "1122") {
            AppUtils.callIntentClearTaskFlag(this, HomeActivity::class.java)
            finish()
        }
    }

    private fun resendCode() {
        Handler(Looper.getMainLooper()).postDelayed({
            hideProgressBar()
            Toast.makeText(this, "Code has been sent", Toast.LENGTH_SHORT).show()
        }, 2000)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.forwardConstraintLayout -> {
                    showProgressBar()
                    verifyPin()

                }
                R.id.ivResendCode -> {
                    showProgressBar()
                    resendCode()
                }
            }
        }
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_otp_verification
    }

    override fun getActivityName(): String? {
        return "Pin Verification"
    }

    override fun receiveExtras(arguments: Bundle?) {
        if (!arguments!!.getString(PHONE_NUMBER).isNullOrEmpty()) {
            phoneNumber = arguments.getString(PHONE_NUMBER)
        }
    }

    private fun verifyPin() {
        hideKeyboard(this)
        if (textPinEntry.text.toString().isEmpty() || textPinEntry.text.toString().length < 4) {
            hideProgressBar()
            Toast.makeText(
                    this@OTPVerificationActivity,
                    "Please enter 4 digit verification code!!!",
                    Toast.LENGTH_SHORT
            ).show()
        } else {
            pinVerification(textPinEntry.text.toString())
        }
    }

    private fun showProgressBar() {
        forwardConstraintLayout.background = ContextCompat.getDrawable(this, R.drawable.background_circle)
        forwardProgressBar.visibility = View.VISIBLE
        forwardImageView.visibility = View.GONE
    }

    private fun hideProgressBar() {
        forwardConstraintLayout.background = ContextCompat.getDrawable(this, R.drawable.round_edge_rectangle)
        forwardProgressBar.visibility = View.GONE
        forwardImageView.visibility = View.VISIBLE
    }
}
