package com.one.greenland.views.activities


import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.kaopiz.kprogresshud.KProgressHUD
import com.one.greenland.R
import kotlinx.android.synthetic.main.activity_terms_and_condition.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*

class TermsAndConditionActivity : BaseActivity() {

    var kProgressHud: KProgressHUD? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        kProgressHud = KProgressHUD(this)
        kProgressHud!!.setLabel("Please wait!!!")
        backImageView.setOnClickListener { onBackPressed() }
        tvFragmentName.visibility = View.VISIBLE
        tvFragmentName.text = getString(R.string.terms_conditions_text)
        openUrl("https://gharpar.co/app-terms-and-conditions/")
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_terms_and_condition
    }

    override fun getActivityName(): String? {
        return "Terms And Condition"
    }

    override fun receiveExtras(arguments: Bundle?) {

    }

    override fun onBackPressed() {
        kProgressHud?.let {
            if (it.isShowing) {
                it.dismiss()

            }
        }

        webView.stopLoading()

        super.onBackPressed()
    }

    private fun openUrl(url: String) {
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                url?.let {
                    view?.loadUrl(it)
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                kProgressHud?.show()
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                kProgressHud?.dismiss()

            }
        }
        webView.clearCache(true)
        webView.clearHistory()
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.builtInZoomControls = false
        webView.settings.domStorageEnabled = true;
        //webView.loadData(url, "text/html", "utf-8")
        webView.loadUrl(url)
    }

}