package com.one.greenland.views.activities

import android.animation.LayoutTransition
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.one.greenland.R
import com.one.greenland.common.*
import com.one.greenland.models.User
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.hideKeyboard
import com.one.greenland.utils.AppUtils.isPasswordMatched
import com.one.greenland.utils.AppUtils.isValidPassword
import com.one.greenland.utils.PreferenceUtils
import kotlinx.android.synthetic.main.activity_update_password.*

class UpdatePasswordActivity : BaseActivity(), View.OnClickListener {

    private var phoneNumber: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
        listenersInitializer()
        textViewFocusChangeListener()
        imeOption()
    }

    private fun listenersInitializer() {
        forwardConstraintLayout.setOnClickListener(this)
    }

    private fun updatePassword(password: String) {
        showProgressBar()
        val list = PreferenceUtils.getModelForKey(REGISTERED_USER_LIST)
        val listType =
                object : TypeToken<MutableList<User>>() {

                }.type

        val users = Gson().fromJson<MutableList<User>>(
                list,
                listType
        )
        if (!users.isNullOrEmpty()) {
            val temp = users.find { it.phone == phoneNumber }
            if (temp == null) {
                hideProgressBar()
                AppUtils.showErrorDialog(this, "No user found. Please sign up.")
            } else {
                temp.password = password
                saveUserInPreferences(temp)
                saveUserListInPreferences(users)
                Toast.makeText(this, "Password updated successfully", Toast.LENGTH_SHORT).show()
                AppUtils.callIntentClearTaskFlag(this, HomeActivity::class.java)
            }
        } else {
            hideProgressBar()
            AppUtils.showErrorDialog(this, "No user found. Please sign up.")
        }

    }

    private fun saveUserInPreferences(user: User) {
        PreferenceUtils.putModel(USER_MODEL, user)
    }

    private fun saveUserListInPreferences(user: MutableList<User>) {
        PreferenceUtils.putModelList(REGISTERED_USER_LIST, user)
    }

    private fun imeOption() {
        etVerifyPassword.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard(this)
                if (passwordValidation(true))
                    updatePassword(etNewPassword.text.toString().trim())
            }
            false
        }
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.forwardConstraintLayout -> {
                    hideKeyboard(this)
                    if (passwordValidation(true))
                        updatePassword(etNewPassword.text.toString().trim())
                }
            }
        }
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_update_password
    }

    override fun getActivityName(): String? {
        return "Reset Password Activity"
    }

    override fun receiveExtras(arguments: Bundle?) {
        if (!arguments!!.getString(PHONE_NUMBER).isNullOrEmpty())
            phoneNumber = arguments.getString(PHONE_NUMBER)
    }

    private fun passwordValidation(animate: Boolean): Boolean {
        val animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        when {
            !isValidPassword(this, etNewPassword) -> {
                if (animate)
                    etNewPassword.startAnimation(animShake)
                return false
            }
            !isPasswordMatched(this, etNewPassword, etVerifyPassword) -> {
                if (animate)
                    etVerifyPassword.startAnimation(animShake)
                return false
            }
        }
        return true;
    }

    private fun textViewFocusChangeListener() {
        etNewPassword.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus)
                isValidPassword(this@UpdatePasswordActivity, etNewPassword)
        }
    }

    private fun showProgressBar() {
        forwardConstraintLayout.background = ContextCompat.getDrawable(this, R.drawable.background_circle)
        forwardProgressBar.visibility = View.VISIBLE
        forwardImageView.visibility = View.GONE
    }

    private fun hideProgressBar() {
        forwardConstraintLayout.background = ContextCompat.getDrawable(this, R.drawable.round_edge_rectangle)
        forwardProgressBar.visibility = View.GONE
        forwardImageView.visibility = View.VISIBLE
    }

}
