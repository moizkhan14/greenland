package com.one.greenland.views.activities

import android.animation.LayoutTransition
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.one.greenland.R
import com.one.greenland.common.*
import com.one.greenland.models.User
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.isValidEditText
import com.one.greenland.utils.AppUtils.showErrorDialog
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.utils.PreferenceUtils.Companion.getModelForKey
import kotlinx.android.synthetic.main.activity_user_session.*

class UserSessionActivity : BaseActivity(), View.OnClickListener {

    private var apiToHitName: String = SIGN_IN_UI_VISIBLE
    private var isFromHomeActivity: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTransitions()
        initializer()
        listenersInitializer()
        textViewFocusListener()

    }

    private fun setTransitions() {
        (userSessionMainConstraintLayout as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGE_APPEARING)

        (topBar as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
    }

    private fun initializer() {
        countryCodeTextView.text = AppUtils.getCountryCode(this)
    }

    private fun imeOption() {
        val view = when (apiToHitName) {
            SIGN_IN_UI_VISIBLE -> {
                etPassword
            }
            SIGN_UP_UI_VISIBLE -> {
                etVerifyPassword
            }
            else -> {
                etPhoneNumber
            }
        }
        view.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                AppUtils.hideKeyboard(this)
                hitApi()
            }
            false
        }

    }

    private fun listenersInitializer() {
        tvSignIn.setOnClickListener(this)
        tvSignUp.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        tvContinueAsGuest.setOnClickListener(this)
        tvForgotPassword.setOnClickListener(this)
        forwardConstraintLayout.setOnClickListener(this)
    }

    private fun textViewFocusListener() {

        etFirstName.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus)
                isValidEditText(etFirstName, getString(R.string.first_name_error))
        }

        etLastName.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus)
                isValidEditText(etLastName, getString(R.string.last_name_error))
        }

        etPhoneNumber.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus)
                AppUtils.isValidPhone(this, etPhoneNumber)
        }

        etPassword.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus)
                AppUtils.isValidPassword(this, etPassword)
        }
    }

    private fun selectedTextView(tv: TextView) {
        tv.setTextColor(ContextCompat.getColor(this, R.color.colorBlue))

        val constraintSet = ConstraintSet()
        constraintSet.clone(topBar)

        if (tv.id != R.id.tvSignIn) {
            constraintSet.connect(
                    R.id.tabSelector,
                    ConstraintSet.START,
                    R.id.tvSignUp,
                    ConstraintSet.START
            )

            constraintSet.connect(
                    R.id.tabSelector,
                    ConstraintSet.END,
                    R.id.tvSignUp,
                    ConstraintSet.END
            )

            constraintSet.applyTo(topBar)

            unSelectedTextView(tvSignIn)
        } else {
            constraintSet.connect(
                    R.id.tabSelector,
                    ConstraintSet.START,
                    R.id.tvSignIn,
                    ConstraintSet.START
            )

            constraintSet.connect(
                    R.id.tabSelector,
                    ConstraintSet.END,
                    R.id.tvSignIn,
                    ConstraintSet.END
            )

            constraintSet.applyTo(topBar)

            unSelectedTextView(tvSignUp)
        }
    }

    private fun unSelectedTextView(tv: TextView) {
        tv.setTextColor(ContextCompat.getColor(this, R.color.colorAqua))
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_user_session
    }

    override fun getActivityName(): String? {
        return "User Session Activity"
    }

    override fun receiveExtras(arguments: Bundle?) {
        if (arguments!!.containsKey(BUNDLE_KEY)) {
            if (arguments!!.getBundle(BUNDLE_KEY) != null) {
                val bundle = arguments!!.getBundle(BUNDLE_KEY)
                isFromHomeActivity = bundle!!.getString(CALLING_ACTIVITY_KEY) == "Home Activity"
                if (bundle!!.getString(USER_SESSION_INTENT_EXTRAS, "SignIn").contains("SignIn")) {
                    switchUi(true)
                } else {
                    switchUi(false)
                }
            }
        } else {
            if (arguments!!.getString(USER_SESSION_INTENT_EXTRAS, "SignIn").contains("SignIn")) {
                switchUi(true)
            } else {
                switchUi(false)
            }
        }
    }

    private fun switchUi(signIn: Boolean) {
        if (signIn) {
            switchToSignIn()
        } else {
            switchToSignUp()
        }
        imeOption()
    }

    private fun switchToSignIn() {
        apiToHitName = SIGN_IN_UI_VISIBLE

        selectedTextView(tvSignIn)

        etFirstName.visibility = View.GONE
        etLastName.visibility = View.GONE
        etVerifyPassword.visibility = View.GONE
        etPassword.visibility = View.VISIBLE
        tvForgotPassword.visibility = View.VISIBLE

        ivBack.visibility = View.GONE
        forgotPasswordHeadingTextView.visibility = View.GONE
        forgotPasswordImageView.visibility = View.GONE

        logo.visibility = View.VISIBLE
        tvSignIn.visibility = View.VISIBLE
        tvSignUp.visibility = View.VISIBLE
        tabSelector.visibility = View.VISIBLE

        etPhoneNumber.imeOptions = EditorInfo.IME_ACTION_NEXT
        etPassword.imeOptions = EditorInfo.IME_ACTION_DONE

        setPhoneNumberFieldConstraintForSignInOrUp()
    }

    private fun switchToSignUp() {
        apiToHitName = SIGN_UP_UI_VISIBLE

        selectedTextView(tvSignUp)

        etFirstName.visibility = View.VISIBLE
        etLastName.visibility = View.VISIBLE
        etPassword.visibility = View.VISIBLE
        etVerifyPassword.visibility = View.VISIBLE
        tvForgotPassword.visibility = View.GONE

        ivBack.visibility = View.GONE
        forgotPasswordHeadingTextView.visibility = View.GONE
        forgotPasswordImageView.visibility = View.GONE

        logo.visibility = View.VISIBLE
        tvSignIn.visibility = View.VISIBLE
        tvSignUp.visibility = View.VISIBLE
        tabSelector.visibility = View.VISIBLE

        etPhoneNumber.imeOptions = EditorInfo.IME_ACTION_NEXT
        etPassword.imeOptions = EditorInfo.IME_ACTION_NEXT

        setPhoneNumberFieldConstraintForSignInOrUp()
    }

    private fun switchToForgotPassword() {
        apiToHitName = FORGOT_PASSWORD_UI_VISIBLE

        ivBack.visibility = View.VISIBLE
        forgotPasswordHeadingTextView.visibility = View.VISIBLE
        forgotPasswordImageView.visibility = View.VISIBLE

        logo.visibility = View.INVISIBLE
        tvSignIn.visibility = View.INVISIBLE
        tvSignUp.visibility = View.INVISIBLE
        tabSelector.visibility = View.INVISIBLE

        etFirstName.visibility = View.GONE
        etLastName.visibility = View.GONE
        etPassword.visibility = View.GONE
        etVerifyPassword.visibility = View.GONE


        tvForgotPassword.visibility = View.GONE

        etPhoneNumber.imeOptions = EditorInfo.IME_ACTION_DONE

        setPhoneNumberFieldConstraintForForgotPassword()
    }

    private fun setPhoneNumberFieldConstraintForForgotPassword() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(userSessionMainConstraintLayout)

        constraintSet.connect(
                R.id.etPhoneNumber,
                ConstraintSet.TOP,
                R.id.forgotPasswordImageView,
                ConstraintSet.BOTTOM
        )

        constraintSet.applyTo(userSessionMainConstraintLayout)
    }

    private fun setPhoneNumberFieldConstraintForSignInOrUp() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(userSessionMainConstraintLayout)

        constraintSet.connect(
                R.id.etPhoneNumber,
                ConstraintSet.TOP,
                R.id.etLastName,
                ConstraintSet.BOTTOM
        )

        constraintSet.applyTo(userSessionMainConstraintLayout)
    }

    override fun onClick(v: View?) {
        v?.let {
            when (v.id) {
                R.id.tvSignIn -> {
                    switchUi(true);
                }

                R.id.tvSignUp -> {
                    switchUi(false);
                }

                R.id.ivBack -> {
                    onBackPressed()
                }

                R.id.tvForgotPassword -> {
//                    AppUtils.callIntentStringExtras(this, ForgotPasswordActivity::class.java, PHONE_NUMBER, etPhoneNumber.text.toString())
                    switchToForgotPassword()
                }

                R.id.forwardConstraintLayout -> {
                    AppUtils.hideKeyboard(this)
                    hitApi()

                }
            }
        }
    }

    private fun saveUserInPreferences(user: User) {
        PreferenceUtils.putModel(USER_MODEL, user)
    }

    private fun saveUserListInPreferences(user: MutableList<User>) {
        PreferenceUtils.putModelList(REGISTERED_USER_LIST, user)
    }

    private fun hitApi() {
        when (apiToHitName) {
            SIGN_IN_UI_VISIBLE -> {
                if (signInValidation(true))
                    userSignIn(
                            etPhoneNumber.text.toString().trim(),
                            etPassword.text.toString()
                    )
            }

            SIGN_UP_UI_VISIBLE -> {
                if (signUpValidation(true))
                    userSignUp(
                            etFirstName.text.toString().trim(),
                            etLastName.text.toString().trim(),
                            etPassword.text.toString(),
                            etPhoneNumber.text.toString().trim()
                    )
            }

            FORGOT_PASSWORD_UI_VISIBLE -> {
                if (phoneValidation())
                    forgetPassword(etPhoneNumber.text.toString().trim())
            }
        }
    }

    override fun onBackPressed() {
        if (isFromHomeActivity) {
            AppUtils.callIntentClearTaskFlag(this, HomeActivity::class.java)
        } else {
            when (apiToHitName) {

                FORGOT_PASSWORD_UI_VISIBLE -> {
                    switchToSignIn()
                }

                else -> {
                    super.onBackPressed()
                }
            }
        }
    }

    private fun userSignUp(firstName: String, lastName: String, password: String, phone: String) {
        showProgressBar()
        val list = getModelForKey(REGISTERED_USER_LIST)
        val listType =
                object : TypeToken<MutableList<User>>() {

                }.type

        var users = Gson().fromJson<MutableList<User>>(
                list,
                listType
        )
        if (!list.isNullOrEmpty() && list.contains(phone)) {
            AppUtils.showUserAlreadyExitsDialog(this, etPhoneNumber.text.toString())
        } else {
            if (users.isNullOrEmpty()) {
                users = mutableListOf<User>()
            }
            val user = User()
            user.password = password
            user.first_name = firstName
            user.last_name = lastName
            user.phone = phone
            user.country_code = "+92"
            user.isLoggedIn = true
            user.user_name = "$firstName $lastName"
            users.add(user)
            saveUserInPreferences(user)
            saveUserListInPreferences(users)
        }
        hideProgressBar()
        AppUtils.callIntentStringExtras(this, OTPVerificationActivity::class.java, PHONE_NUMBER, etPhoneNumber.text.toString().trim())

        // signUpAPIRequest.userSignUp(this, this, firstName, lastName, password, phone)
    }

    private fun forgetPassword(phone: String) {
        showProgressBar()
        // forgotPasswordAPIRequest.forgetPassword(this, this, phone)

        val list = getModelForKey(REGISTERED_USER_LIST)
        val listType =
                object : TypeToken<MutableList<User>>() {

                }.type

        val users = Gson().fromJson<MutableList<User>>(
                list,
                listType
        )
        if (!users.isNullOrEmpty()) {
            val temp = users.find { it.phone == phone }
            if (temp == null) {
                hideProgressBar()
                showErrorDialog(this, "No user found. Please sign up.")
            } else {
                AppUtils.callIntentStringExtras(this, ForgetOTPVerificationActivity::class.java, PHONE_NUMBER, phone)
            }
        } else {
            hideProgressBar()
            showErrorDialog(this, "No user found. Please sign up.")
        }

    }

    private fun signUpValidation(animate: Boolean): Boolean {
        val animShake = AnimationUtils.loadAnimation(this, R.anim.shake)

        if (!isValidEditText(etFirstName, getString(R.string.first_name_error))) {
            if (animate)
                etFirstName.startAnimation(animShake)
            return false
        }

        if (!isValidEditText(etLastName, getString(R.string.last_name_error))) {
            if (animate)
                etLastName.startAnimation(animShake)
            return false
        }

        if (!AppUtils.isValidPhone(this, etPhoneNumber)) {
            if (animate)
                etPhoneNumber.startAnimation(animShake)
            return false
        }

        if (!AppUtils.isValidPassword(this, etPassword)) {
            if (animate)
                etPassword.startAnimation(animShake)
            return false
        }

        if (!AppUtils.isPasswordMatched(this, etPassword, etVerifyPassword)) {
            if (animate)
                etVerifyPassword.startAnimation(animShake)
            return false
        }

        return true
    }

    private fun signInValidation(animation: Boolean): Boolean {
        val animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        if (!AppUtils.isValidPhone(this, etPhoneNumber)) {
            if (animation)
                etPhoneNumber.startAnimation(animShake)
            return false
        }

        if (!AppUtils.isValidEditText(etPassword, getString(R.string.password_required_text))) {
            if (animation)
                etPassword.startAnimation(animShake)
            return false
        }

        return true
    }

    private fun phoneValidation(): Boolean {
        val animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        if (!AppUtils.isValidPhone(this, etPhoneNumber)) {
            etPhoneNumber.requestFocus()
            etPhoneNumber.startAnimation(animShake)
            return false
        }
        return true
    }

    private fun userSignIn(phone: String?, password: String?) {
        showProgressBar()

        val list = getModelForKey(REGISTERED_USER_LIST)
        val listType =
                object : TypeToken<MutableList<User>>() {

                }.type

        val users = Gson().fromJson<MutableList<User>>(
                list,
                listType
        )
        if (!users.isNullOrEmpty()) {
            val temp = users.find { it.phone == phone }
            /*for (user in users) {
                if (user.phone == phone) {

                }
            }*/
            if (temp == null) {
                showErrorDialog(this, "No user found. Please sign up.")
            } else {
                if (temp.password == password) {
                    saveUserInPreferences(temp)
                    AppUtils.callIntentClearTaskFlag(this, HomeActivity::class.java)
                    finish()
                } else {
                    showErrorDialog(this, "Phone or password is incorrect. Please try again")
                }
            }
        } else {
            showErrorDialog(this, "No user found. Please sign up.")
        }
        // saveUserInPreferences(user)

        hideProgressBar()
        //  signInAPIRequest.userSignIn(this, this, phone, password)
    }

    private fun showProgressBar() {
        forwardConstraintLayout.background = ContextCompat.getDrawable(this, R.drawable.background_circle)
        forwardProgressBar.visibility = View.VISIBLE
        forwardImageView.visibility = View.GONE
    }

    private fun hideProgressBar() {
        forwardConstraintLayout.background = ContextCompat.getDrawable(this, R.drawable.round_edge_rectangle)
        forwardProgressBar.visibility = View.GONE
        forwardImageView.visibility = View.VISIBLE
    }
}