package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.models.OrderServiceAddon

class AddOnSummaryRecyclerAdapter(val context: Context, var order_service_addons: MutableList<OrderServiceAddon>) : RecyclerView.Adapter<AddOnSummaryRecyclerAdapter.AddOnSummaryViewHolder>() {

    public class AddOnSummaryViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvSubServiceCounter = view.findViewById<TextView>(R.id.tvSubServiceCounter)
        val tvSubServiceName = view.findViewById<TextView>(R.id.tvSubServiceName)
        val tvSubServicePrice = view.findViewById<TextView>(R.id.tvSubServicePrice)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddOnSummaryViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_addon_summary, parent, false)
        return AddOnSummaryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return order_service_addons.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    @ExperimentalStdlibApi
    override fun onBindViewHolder(holder: AddOnSummaryViewHolder, position: Int) {
        holder.tvSubServiceCounter.text = "${"x"}${order_service_addons[position].unit_count}"
        holder.tvSubServiceName.text = order_service_addons[position].service_addon_title!!.capitalize()
        val price = order_service_addons[position].unit_price.toInt() * order_service_addons[position].unit_count.toInt()
        holder.tvSubServicePrice.text = "${"Rs. "}${price}"
    }
}