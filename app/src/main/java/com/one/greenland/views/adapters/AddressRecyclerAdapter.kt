package com.one.greenland.views.adapters

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.common.USER_MODEL
import com.one.greenland.interfaces.ItemClickListener
import com.one.greenland.models.AddressModel
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.fragments.AddressFragment
import com.one.greenland.views.fragments.FragmentUpdateProfile
import com.one.greenland.views.fragments.ScheduleServiceFragment

class AddressRecyclerAdapter(
        val context: Context,
        private val addresses: MutableList<AddressModel>,
        private val listener: ItemClickListener,
        private val canAddNewAddress: Boolean,
        private val canDeleteAddress: Boolean) : RecyclerView.Adapter<AddressRecyclerAdapter.AddressViewHolder>() {

    private var selectedIndex = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_address_user, parent, false)

        return AddressViewHolder(view)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun getItemCount(): Int {
        return if (addresses.size == 0 && canAddNewAddress) {
            1
        } else if (canAddNewAddress)
            addresses.size + 1
        else
            addresses.size
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        if (canDeleteAddress) {
            holder.ivEdit.visibility = View.VISIBLE
        } else {
            holder.ivEdit.visibility = View.GONE
        }
        if (position == addresses.size && canAddNewAddress) {
            holder.clAddress.background = (ContextCompat.getDrawable(context, R.drawable.background_dashed_address))
            holder.ivEdit.visibility = View.GONE
            holder.tvAddress.visibility = View.GONE
            holder.tvAddressValue.visibility = View.GONE
            holder.ivAddAddress.visibility = View.VISIBLE
            holder.ivSelectedTick.visibility = View.GONE
        } else {
            if (addresses.size > 0) {
                holder.tvAddress.text = "${"Address "}${position + 1}"
                holder.tvAddressValue.text = "${addresses[position].address_1}, ${addresses[position].area_id}, ${addresses[position].city_id}."
            }
        }



        if (position < addresses.size) {
            if ((context as HomeActivity).callingFragment == ScheduleServiceFragment.scheduleServiceFragment().getFragmentName()!!) {
                if (((context as HomeActivity).orderModel.address_id != null
                                && (context as HomeActivity).orderModel.address_id == addresses[position].id)) {
                    selectedIndex = position
                    addresses[selectedIndex].isSelected = true
                    listener.itemClicked(addresses[position])
                } else if ((context as HomeActivity).orderModel.address_id == null && ((context as HomeActivity).getAppUser().address != null &&
                                (context as HomeActivity).getAppUser().address!!.id == addresses[position].id
                                && addresses[position].is_default)) {
                    selectedIndex = position
                    addresses[selectedIndex].isSelected = true
                    listener.itemClicked(addresses[position])
                }
            }

            if ((context as HomeActivity).getAppUser().address != null
                    && (context as HomeActivity).getAppUser().address!!.id == addresses[position].id
                    && addresses[position].is_default
                    && (context as HomeActivity).callingFragment == FragmentUpdateProfile().getFragmentName() && canAddNewAddress) {

                selectedIndex = position
                addresses[selectedIndex].isSelected = true
                listener.itemClicked(addresses[position])
            }
        }

        if (canAddNewAddress) {
            holder.ivEdit.setOnClickListener {
                showEditAddressDialog(context, addresses[position], position)
            }
            holder.clAddress.setOnClickListener {
                if (position != addresses.size) {
                    if (selectedIndex > -1) {
                        addresses[selectedIndex].isSelected = false
                        addresses[selectedIndex].is_default = false
                        changeBackgroundOfSelectedItem(
                                selectedIndex,
                                position,
                                holder,
                                addresses[selectedIndex]
                        )
                    }
                    selectedIndex = position
                    addresses[selectedIndex].isSelected = true
                    addresses[selectedIndex].is_default = true
                    listener.itemClicked(addresses[position])
                    notifyDataSetChanged()
                } else {
                    (context as BaseActivity).changeFragment(AddressFragment(), (context as BaseActivity).callingFragment!!, R.id.containerMainFragment, true)
                }
            }
        }
        if (addresses.size > 0 && position != addresses.size) {
            changeBackgroundOfSelectedItem(selectedIndex, position, holder, addresses[position])

            if (addresses[position].isSelected) {
                selectedIndex = position
                listener.itemClicked(addresses[position])
                holder.clAddress.background = (ContextCompat.getDrawable(context, R.drawable.backgtound_address_selected))
                holder.ivSelectedTick.visibility = View.VISIBLE
            }
        }

    }

    public class AddressViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val clAddress = view.findViewById<ConstraintLayout>(R.id.clAddress)!!
        val ivEdit = view.findViewById<ImageView>(R.id.ivEdit)!!
        val ivSelectedTick = view.findViewById<ImageView>(R.id.ivSelectedTick)!!
        val ivAddAddress = view.findViewById<ImageView>(R.id.ivAddAddress)!!
        val tvAddress = view.findViewById<TextView>(R.id.tvAddress)!!
        val tvAddressValue = view.findViewById<TextView>(R.id.tvAddressValue)!!
    }

    private fun changeBackgroundOfSelectedItem(
            selectedIndex: Int,
            position: Int,
            holder: AddressViewHolder,
            addressModel: AddressModel
    ) {
        if (selectedIndex == position && addressModel.isSelected) {
            holder.clAddress.background = (ContextCompat.getDrawable(context, R.drawable.backgtound_address_selected))
            holder.ivSelectedTick.visibility = View.VISIBLE
        } else {
            holder.clAddress.background = (ContextCompat.getDrawable(context, R.drawable.round_edge_fourteen_dp))
            holder.ivSelectedTick.visibility = View.GONE
        }
    }

    private fun showEditAddressDialog(
            context: Context,
            address: AddressModel,
            position: Int
    ) {
        val mBuilder = AlertDialog.Builder(context)

        val view = (context as Activity).layoutInflater.inflate(R.layout.dialog_edit_address, null)

        mBuilder.setView(view)
        val dialog = mBuilder.create()
        dialog.setCancelable(false)
        val btnDelete = view.findViewById<Button>(R.id.btnDelete)
        val tvCancel = view.findViewById<TextView>(R.id.tvCancel)
        btnDelete.setOnClickListener {

            /*selectedIndex = -1
            val address = addresses[position]
            addresses.removeAt(position)
            if ((context as HomeActivity).callingFragment == FragmentUpdateProfile().getFragmentName()!!) {
                (context as HomeActivity).updateUser!!.addresses = addresses
                if ((context as HomeActivity).updateUser!!.address == address) {
                    (context as HomeActivity).updateUser!!.address = null
                }
            }
            val user = (context as HomeActivity).getAppUser()
            user.addresses.clear()
            user.addresses = addresses
            if (user.address == address) {
                user.address = null
            }
            (context as HomeActivity).setAppUser(user)*/

            val addressToDelete = (context as HomeActivity).updateUser!!.addresses.find { it == address }
            if (addressToDelete != null) {
                addresses.removeAt(position)
                (context as HomeActivity).updateUser!!.addresses.remove(addressToDelete)
                PreferenceUtils.putModel(USER_MODEL, (context as HomeActivity).updateUser!!)
            }

            notifyDataSetChanged()

            dialog.dismiss()
        }
        tvCancel.setOnClickListener {
            dialog.dismiss()
        }


        dialog.window!!.setGravity(Gravity.BOTTOM)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }
}