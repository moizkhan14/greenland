package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.models.OrderModel
import com.one.greenland.utils.AppUtils.getFormatedDate
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.fragments.OrderDetailsFragment
import java.util.*


class BookingsRecyclerAdapter(val context: Context, private val orders: MutableList<OrderModel>) : RecyclerView.Adapter<BookingsRecyclerAdapter.BookingsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_booking_recycler, parent, false)


        return BookingsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return orders.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: BookingsViewHolder, position: Int) {
        setViewsColor(holder, orders[position])
        holder.tvDate.text = getFormatedDate(orders[position].order_date!!, "MMM dd")
        holder.tvTime.text = orders[position].order_time!!
        holder.tvOrderId.text = "Order ID: " + orders[position].id.toString()
        holder.tvOrderPrice.text = "Rs. " + orders[position].total_price?.toInt()
        var serviceName = String()
        for (service in orders[position].order_services!!)
            serviceName += service.service_title + if (service != orders[position].order_services?.get(orders[position].order_services!!.size - 1)) " + " else ""
        holder.tvServiceName.text = serviceName
        holder.tvOrderStatus.text = context.getString(R.string.confirmed_text)

        holder.cvShowDetails.setOnClickListener {
            (context as BaseActivity).changeFragment(OrderDetailsFragment(orders[position]), (context as BaseActivity).callingFragment!!, R.id.containerMainFragment, true)
        }
    }

    public class BookingsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvDate = view.findViewById<TextView>(R.id.tvDate)!!
        val tvOrderId = view.findViewById<TextView>(R.id.tvOrderId)!!
        val tvTime = view.findViewById<TextView>(R.id.tvTime)!!
        val tvOrderPrice = view.findViewById<TextView>(R.id.tvOrderPrice)!!
        val tvServiceName = view.findViewById<TextView>(R.id.tvServiceName)!!
        val tvOrderStatus = view.findViewById<TextView>(R.id.tvOrderStatus)!!
        val cvShowDetails = view.findViewById<CardView>(R.id.cvShowDetails)!!
        val rlShowDetails = view.findViewById<RelativeLayout>(R.id.rlShowDetails)!!
    }

    private fun setViewsColor(holder: BookingsViewHolder, orderModel: OrderModel) {
        when (orderModel.status?.toLowerCase(Locale.getDefault())) {
            "pending" -> {
                holder.cvShowDetails.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorOrangeAlphaLight))
                holder.tvOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.colorGreen))
            }
            "completed" -> {
                holder.cvShowDetails.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorLayoutBackgroundOrderCompleted))
                holder.tvOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.colorTextOrderCompleted))
            }
            "canceled", "cancelled" -> {
                holder.cvShowDetails.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorLayoutBackgroundOrderCancelled))
                holder.tvOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.colorTextOrderCanceled))
            }
            "confirmed" -> {
                holder.cvShowDetails.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorLayoutBackgroundOrderConfirmed))
                holder.tvOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.colorBlue))
            }
        }
    }
}