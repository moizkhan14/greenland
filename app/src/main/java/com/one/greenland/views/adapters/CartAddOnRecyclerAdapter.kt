package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.models.OrderService
import com.one.greenland.models.OrderServiceAddon
import com.one.greenland.views.activities.HomeActivity

class CartAddOnRecyclerAdapter(val context: Context, private val addOns: MutableList<OrderServiceAddon>, val servicePosition: Int, private val dataUpdateListener: DataUpdateListener) : RecyclerView.Adapter<CartAddOnRecyclerAdapter.AddOnViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddOnViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_add_ons, parent, false)

        return AddOnViewHolder(view)
    }

    override fun getItemCount(): Int {
        return addOns.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: AddOnViewHolder, position: Int) {
        var price = 0
        var tvPriceValue = ""

        if (!(context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].total_price.toString().isNullOrEmpty() && (context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].total_price.toString() != "0.0") {
            tvPriceValue = "Rs. ${(context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].total_price.toInt()}"
            price = (context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].total_price.toInt()
        } else {
            tvPriceValue = "Rs. ${((context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].unit_price.toInt())}"
            price = (context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].unit_price.toInt()
        }
        holder.tvAddOnPrice.text = tvPriceValue
        holder.tvAddonDetails.text = addOns[position].service_addon_title!!.capitalize()
        // holder.tvAddOnPrice.text = "${"Rs."} ${addOns[position].unit_price.toInt()}"
        //price = addOns[position].unit_price
        holder.tvAddOnItemCounter.text = (context as HomeActivity).orderServiceList[servicePosition].order_service_addons!![position].unit_count.toString()
        holder.ivAddOnItemAdd.setOnClickListener {
            if (context.orderServiceList[servicePosition].order_service_addons!![position].unit_count < 10) {
                holder.tvAddOnItemCounter.text = (++(context as HomeActivity).orderServiceList[servicePosition].order_service_addons!![position].unit_count).toString()
                holder.tvAddOnPrice.text = "${"Rs."} ${(price + addOns[position].unit_price.toInt())}"
                price += addOns[position].unit_price.toInt()
                (context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].total_price = price.toDouble()
                dataUpdateListener.onUpdateData()
                updateOrderCartRequest((context as HomeActivity).orderServiceList)
            }
        }
        holder.ivAddOnItemSub.setOnClickListener {
            if (holder.tvAddOnItemCounter.text.toString().toInt() > 1) {
                holder.tvAddOnPrice.text = "${"Rs."} ${(price - addOns[position].unit_price.toInt())}"
                price -= addOns[position].unit_price.toInt()
                holder.tvAddOnItemCounter.text = (--(context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].unit_count).toString()
                (context as HomeActivity).orderServiceList[servicePosition].order_service_addons[position].total_price = price.toDouble()
                dataUpdateListener.onUpdateData()
                updateOrderCartRequest((context as HomeActivity).orderServiceList)
            }
        }
    }

    private fun updateOrderCartRequest(orderServices: MutableList<OrderService>) {
        val orderRequest = (context as HomeActivity).getCartOrderRequest()
        orderRequest.order.order_services = orderServices
        (context as HomeActivity).setCartOrderRequest(orderRequest)
    }

    public class AddOnViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvAddonDetails = view.findViewById<TextView>(R.id.tvAddonDetails)
        val tvAddOnPrice = view.findViewById<TextView>(R.id.tvAddOnPrice)
        val ivAddOnItemAdd = view.findViewById<ImageView>(R.id.ivAddOnItemAdd)
        val ivAddOnItemSub = view.findViewById<ImageView>(R.id.ivAddOnItemSub)
        val tvAddOnItemCounter = view.findViewById<TextView>(R.id.tvAddOnItemCounter)
    }
}