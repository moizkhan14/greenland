package com.one.greenland.views.adapters

import android.content.Context
import android.graphics.Canvas
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.models.OrderService
import com.one.greenland.views.activities.HomeActivity
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator


/**
 * created by Moiz Khan
 */

class CartRecyclerAdapter(val context: Context, private val dataUpdateListener: DataUpdateListener) : RecyclerView.Adapter<CartRecyclerAdapter.CartViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_cart_recycler, parent, false)
        return CartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return (context as HomeActivity).orderServiceList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        var price = 0
        var tvPriceValue = ""
        holder.tvServiceName.text = (context as HomeActivity).orderServiceList[position].service_category_title!!.capitalize()
        holder.tvServiceDetails.text = context.orderServiceList[position].service_title!!.capitalize()
        if (!context.orderServiceList[position].total_price.toString().isNullOrEmpty() && context.orderServiceList[position].total_price.toString() != "0.0") {
            tvPriceValue = "Rs. ${context.orderServiceList[position].total_price.toInt()}"
            price = context.orderServiceList[position].total_price.toInt()
        } else {
            tvPriceValue = "Rs. ${(context.orderServiceList[position].unit_price.toInt())}"
            price = context.orderServiceList[position].unit_price.toInt()
        }
        holder.tvPrice.text = tvPriceValue

        holder.tvItemCounter.text = context.orderServiceList[position].unit_count.toString()

        if (context.orderServiceList[position].unit_count > 1) {
            holder.rgServiceTime.visibility = View.VISIBLE
            if (context.orderServiceList[position].is_same_time!!)
                holder.rbSameTime.isChecked = true
            else
                holder.rbAnotherTime.isChecked = true
        }
        holder.ivItemAdd.setOnClickListener {
            if (context.orderServiceList[position].unit_count < 10) {
                holder.tvItemCounter.text = (++context.orderServiceList[position].unit_count).toString()

                dataUpdateListener.onUpdateData()

                holder.tvPrice.text = "${"Rs."} ${(price + context.orderServiceList[position]
                        .unit_price.toInt())}"
                price += (context as HomeActivity).orderServiceList[position].unit_price.toInt()
                context.orderServiceList[position].total_price = price.toDouble()
                if (holder.tvItemCounter.text.toString().toInt() == 2) {
                    holder.rgServiceTime.visibility = View.VISIBLE
                    holder.separatorView.visibility = View.VISIBLE
                }

                updateOrderCartRequest((context as HomeActivity).orderServiceList)
            }
        }
        holder.ivItemSub.setOnClickListener {
            if (holder.tvItemCounter.text.toString().toInt() > 1) {
                holder.tvPrice.text = "${"Rs."} ${(price - (context.orderServiceList[position].unit_price.toInt()))}"
                price -= context.orderServiceList[position].unit_price.toInt()
                holder.tvItemCounter.text = (--context.orderServiceList[position].unit_count).toString()
                context.orderServiceList[position].total_price = price.toDouble()
                dataUpdateListener.onUpdateData()
                updateOrderCartRequest(context.orderServiceList)

            }
            if (holder.tvItemCounter.text.toString().toInt() == 1) {
                holder.rgServiceTime.visibility = View.GONE
                holder.separatorView.visibility = View.GONE
                holder.rbSameTime.isChecked = true
                context.orderServiceList[position].is_same_time = true
                updateOrderCartRequest(context.orderServiceList)
            }
        }

        if (context.orderServiceList[position].order_service_addons!!.size > 0) {
            holder.addOnsLayout.visibility = View.VISIBLE
            val layoutManager = LinearLayoutManager(context)
            holder.rvAddOnCart.layoutManager = layoutManager
           /* val dividerItemDecoration = DividerItemDecoration(holder.rvAddOnCart.context,
                    layoutManager.orientation)
            holder.rvAddOnCart.addItemDecoration(dividerItemDecoration)*/
            val adapter = CartAddOnRecyclerAdapter(context, (context as HomeActivity).orderServiceList[position].order_service_addons!!, position, dataUpdateListener)
            holder.rvAddOnCart.adapter = adapter
            removeAddOnFromCart(position, holder, adapter, dataUpdateListener)
        } else {
            holder.addOnsLayout.visibility = View.GONE
        }

        holder.rbAnotherTime.setOnClickListener {
            if (holder.rbAnotherTime.isChecked) {
                context.orderServiceList[position].is_same_time = false
                updateOrderCartRequest((context as HomeActivity).orderServiceList)
            }
        }

        holder.rbSameTime.setOnClickListener {
            if (holder.rbAnotherTime.isChecked) {
                context.orderServiceList[position].is_same_time = true
                updateOrderCartRequest((context as HomeActivity).orderServiceList)
            }
        }


    }

    private fun removeAddOnFromCart(servicePosition: Int, cartViewHolder: CartViewHolder, adapter: CartAddOnRecyclerAdapter, dataUpdateListener: DataUpdateListener) {
        val simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {

                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                //Remove swiped item from list and notify the RecyclerView
                val position = viewHolder.adapterPosition
                (context as HomeActivity).orderServiceList[servicePosition].order_service_addons.removeAt(position)
                //Toast.makeText(context, "Item Removed", Toast.LENGTH_SHORT).show()
                // adapter.notifyDataSetChanged()
                if (context.orderServiceList[servicePosition].order_service_addons.size == 0) {
                    cartViewHolder.addOnsLayout.visibility = View.GONE
                }
                dataUpdateListener.onUpdateData()
                updateOrderCartRequest((context as HomeActivity).orderServiceList)
                context.updateHomeCartBadge()
                context.setCartOrderRequest((context as HomeActivity).orderRequest)
                adapter.notifyItemRemoved(position)

            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addBackgroundColor(ContextCompat.getColor(context!!, R.color.colorRed))
                        .addActionIcon(R.drawable.ic_vector_delete)
                        .addSwipeRightLabel(context.getString(R.string.remove_text))
                        .setSwipeRightLabelColor(ContextCompat.getColor(context, R.color.colorWhite))
                        .create()
                        .decorate()
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }

        }

        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(cartViewHolder.rvAddOnCart)
    }

    private fun updateOrderCartRequest(orderServices: MutableList<OrderService>) {
        val orderRequest = (context as HomeActivity).getCartOrderRequest()
        orderRequest.order.order_services = orderServices
        context.setCartOrderRequest(orderRequest)
    }

    public class CartViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvServiceName = view.findViewById<TextView>(R.id.tvServiceName)
        val tvServiceDetails = view.findViewById<TextView>(R.id.tvServiceDetails)
        val tvPrice = view.findViewById<TextView>(R.id.tvPrice)
        val ivItemAdd = view.findViewById<ImageView>(R.id.ivItemAdd)
        val ivItemSub = view.findViewById<ImageView>(R.id.ivItemSub)
        val tvItemCounter = view.findViewById<TextView>(R.id.tvItemCounter)

        val addOnsLayout = view.findViewById<ConstraintLayout>(R.id.addOnsLayout)
        val rvAddOnCart = view.findViewById<RecyclerView>(R.id.rvAddOnCart)


        val separatorView = view.findViewById<View>(R.id.separatorView)

        val rgServiceTime = view.findViewById<RadioGroup>(R.id.rgServiceTime)
        val rbSameTime = view.findViewById<RadioButton>(R.id.rbSameTime)
        val rbAnotherTime = view.findViewById<RadioButton>(R.id.rbAnotherTime)
    }
}