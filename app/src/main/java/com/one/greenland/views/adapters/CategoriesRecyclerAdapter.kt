package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.one.greenland.R
import com.one.greenland.common.CLICK_ID_CATEGORIES
import com.one.greenland.interfaces.ItemClickListener
import com.one.greenland.models.ServiceCategoriesModel
import com.one.greenland.utils.AppUtils
import java.util.*

/**
 * created by Moiz Khan
 */

class CategoriesRecyclerAdapter(
        var context: Context,
        private var categoriesList: MutableList<ServiceCategoriesModel>,
        private var itemClickListener: ItemClickListener
) :
        RecyclerView.Adapter<CategoriesRecyclerAdapter.CategoriesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.item_categories_recycler_layout, parent, false)
        return CategoriesViewHolder(view)
    }

    fun updateItem(list: MutableList<ServiceCategoriesModel>) {
        //categoriesList.clear()
        categoriesList = list
        notifyDataSetChanged()
    }

    fun clearData() {
        categoriesList.clear()
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return categoriesList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    @ExperimentalStdlibApi
    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        try {
            Glide.with(context)
                    .load(categoriesList[position].category_svg_icon).into(holder.categoryImageView)
        } catch (e: Exception) {
            AppUtils.DebugToast(context, "Server uri is null.")
            e.printStackTrace()
        }

//        holder.categoryImageView.setColorFilter(ContextCompat.getColor(context, R.color.colorOrange), PorterDuff.Mode.SRC_IN)
        if (!categoriesList[position].service_category_title.isNullOrEmpty())
            holder.categoryNameTextView.text = categoriesList[position].service_category_title.capitalize(Locale.ROOT)

        holder.categoryConstraintLayout.setOnClickListener {
            itemClickListener.itemClicked(CLICK_ID_CATEGORIES, position)
        }
    }

    class CategoriesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val categoryImageView = view.findViewById<androidx.appcompat.widget.AppCompatImageView>(R.id.categoriesItemImageView)!!
        val categoryNameTextView = view.findViewById<TextView>(R.id.categoryNameTextView)!!
        val categoryConstraintLayout = view.findViewById<ConstraintLayout>(R.id.categoryConstraintLayout)!!
    }

}