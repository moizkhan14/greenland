package com.one.greenland.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.common.CURRENT_USER_MESSAGE
import com.one.greenland.common.OTHER_USER_MESSAGE
import com.one.greenland.models.ChatMessage
import java.util.*

class ChatMessagesAdapter(private val chatMessages: MutableList<ChatMessage>?) : RecyclerView.Adapter<ChatMessagesAdapter.ChatMessagesViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        return if (chatMessages!![position].id == 1) CURRENT_USER_MESSAGE else OTHER_USER_MESSAGE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatMessagesViewHolder {
        when (viewType) {
            CURRENT_USER_MESSAGE -> return ChatMessagesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_current_user_chat, parent, false))
            OTHER_USER_MESSAGE -> return ChatMessagesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_other_user_chat, parent, false))
        }
        return ChatMessagesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_current_user_chat, parent, false))
    }

    override fun onBindViewHolder(holder: ChatMessagesViewHolder, position: Int) {
        holder.textViewUserMessage.visibility = View.VISIBLE
        holder.imageViewFileIcon!!.visibility = View.GONE
        holder.textViewUserMessage.text = chatMessages!![position].message
    }

    override fun getItemCount(): Int {
        return chatMessages?.size ?: 0
    }

    class ChatMessagesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewUserMessage: TextView = itemView.findViewById(R.id.textViewUserMessage)
        val imageViewFileIcon: ImageView? = itemView.findViewById(R.id.imageViewFileIcon)

    }

}
