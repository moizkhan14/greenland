package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.one.greenland.R
import com.one.greenland.interfaces.CalendarItemClickListener
import com.one.greenland.models.DateModel
import com.one.greenland.utils.AppUtils.viewFadeInVisibleAnimation
import com.one.greenland.utils.AppUtils.viewFadeOutHiddenAnimation
import java.util.*

class DateRecyclerViewAdapter(private val context: Context, private val dates: MutableList<DateModel>, private val isCurrentMonth: Boolean,
                              private val currentDate: Int, private val selected_month: Int, private val current_month: Int, private val current_year: Int, val listener: CalendarItemClickListener)
    : RecyclerView.Adapter<DateRecyclerViewAdapter.DateViewHolder>() {
    private var view: View? = null
    private var selectedIndex = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DateViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        view = inflater.inflate(R.layout.item_recycler_date, parent, false)
        return DateViewHolder(view!!)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: DateViewHolder, position: Int) {
        holder.dateTextView.text = dates[position].date
        holder.dayTextView.text = dates[position].day
        view!!.background = null

        /*   (holder.dateLayout as ViewGroup).layoutTransition
                   .enableTransitionType(LayoutTransition.CHANGING)*/

        holder.dateLayout.setOnClickListener {
            if ((isCurrentMonth && (dates[position].date!!.toInt() < currentDate)) ||((selected_month < current_month))) {

            } else {
                if (selectedIndex > -1) {
                    dates[selectedIndex].isSelected = false
                    changeColorOfSelectedItem(
                            selectedIndex,
                            position,
                            holder,
                            dates[selectedIndex],
                            null, null
                    )
                }
                selectedIndex = position
                dates[selectedIndex].isSelected = true
                listener.dateClicked(dates[position])
                notifyDataSetChanged()
                // TODO: Send selected card view text to main fragment
            }
        }


        if (isCurrentMonth) {
            if (dates[position].date!!.toInt() == currentDate && dates[position].isSelected) {

                viewFadeInVisibleAnimation(holder.backgroundLayout, holder.parentView)
                changeColorOfSelectedItem(selectedIndex, position, holder, dates[position], ContextCompat.getColor(context, R.color.colorGreyLightAlpha),
                        ContextCompat.getColor(context, R.color.colorGreyLightAlpha))
                listener.dateClicked(dates[position])
                /*holder.dateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLightAlpha))
                holder.dayTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLightAlpha))*/
                //  view.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shape));
            } else {
                viewFadeOutHiddenAnimation(holder.backgroundLayout, holder.parentView)
                if (dates[position].date!!.toInt() < currentDate) {
                    changeColorOfSelectedItem(selectedIndex, position, holder, dates[position], ContextCompat.getColor(context, R.color.colorGreyLight), ContextCompat.getColor(context, R.color.colorGreyLight))
                    /*            holder.dateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLight))
                                holder.dayTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLight))*/
                } else {
                    changeColorOfSelectedItem(selectedIndex, position, holder, dates[position], ContextCompat.getColor(context, R.color.colorAqua), ContextCompat.getColor(context, R.color.colorGreyLight))
                    /* holder.dateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAqua))
                     holder.dayTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLight))*/
                }
            }
        } else {
            if ((selected_month < current_month) && (current_year == Calendar.getInstance().get(Calendar.YEAR))) {
                changeColorOfSelectedItem(selectedIndex, position, holder, dates[position], ContextCompat.getColor(context, R.color.colorGreyLight), ContextCompat.getColor(context, R.color.colorGreyLight))
                /*holder.dateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLight))
                holder.dayTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLight))*/
                viewFadeOutHiddenAnimation(holder.backgroundLayout, holder.parentView)
            } else if (selected_month > current_month) {
                changeColorOfSelectedItem(selectedIndex, position, holder, dates[position], ContextCompat.getColor(context, R.color.colorAqua), ContextCompat.getColor(context, R.color.colorGreyLight))
                /*    holder.dateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAqua))
                    holder.dayTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLight))*/
                viewFadeOutHiddenAnimation(holder.backgroundLayout, holder.parentView)
            }
        }

        // changeColorOfSelectedItem(selectedIndex, position, holder, dates[position])

        if (dates[position].isSelected!!) {
            selectedIndex = position
            viewFadeInVisibleAnimation(holder.backgroundLayout, holder.parentView)
            holder.dateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLightAlpha))
            holder.dayTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLightAlpha))
        }
    }

    override fun getItemCount(): Int {
        return dates.size
    }

    inner class DateViewHolder(itemView: View) : ViewHolder(itemView) {
        val dateTextView: TextView = itemView.findViewById(R.id.dateTextView)
        val dayTextView: TextView = itemView.findViewById(R.id.dayTextView)
        val backgroundLayout: RelativeLayout = itemView.findViewById(R.id.backgroundLayout)
        val dateLayout: ConstraintLayout = itemView.findViewById(R.id.dateLayout)
        val parentView = backgroundLayout.parent as ViewGroup

    }

    private fun changeColorOfSelectedItem(
            selectedIndex: Int,
            position: Int,
            holder: DateViewHolder,
            dateModel: DateModel,
            colorDate: Int?,
            colorDay: Int?
    ) {
        if (selectedIndex == position && dateModel.isSelected) {
            viewFadeInVisibleAnimation(holder.backgroundLayout, holder.parentView)
            holder.dateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLightAlpha))
            holder.dayTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLightAlpha))

        } else if (colorDate == null && colorDay == null) {
            holder.dateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAqua))
            holder.dayTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGreyLight))
        } else {
            viewFadeOutHiddenAnimation(holder.backgroundLayout, holder.parentView)
            holder.dateTextView.setTextColor(colorDate!!)
            holder.dayTextView.setTextColor(colorDay!!)
        }
    }

}