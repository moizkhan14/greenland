package com.one.greenland.views.adapters


import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.one.greenland.models.DealsModel
import com.one.greenland.views.fragments.DealsFragment


class DealsSliderAdapter(
        var context: Context,
        var dealsList: MutableList<DealsModel>,
        private val fragmentManager: FragmentManager
) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return DealsFragment.newInstance(dealsList[position], position)
    }

    fun updateItem(list: MutableList<DealsModel>){
        //dealsList.clear()

        //list.remove(list.find { it.deal_banner == null })
        dealsList = list
        notifyDataSetChanged()
    }

    fun clearData(){
        dealsList.clear()
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return dealsList.size
    }

}