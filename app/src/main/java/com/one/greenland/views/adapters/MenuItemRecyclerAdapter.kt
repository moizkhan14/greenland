package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.models.MenuModel
import com.one.greenland.utils.AppUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.fragments.*

import com.bumptech.glide.Glide

class MenuItemRecyclerAdapter(
        private val context: Context,
        private val menuList: MutableList<MenuModel>
) :
        RecyclerView.Adapter<MenuItemRecyclerAdapter.MenuItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuItemViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_menu_recycler, parent, false)
        return MenuItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return menuList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: MenuItemViewHolder, position: Int) {
        Glide.with(context)
                .load(menuList[position].imageSrc).into(holder.itemImageView)
        holder.itemNameTextView.text = menuList[position].menuItemName
        holder.clItemMenu.setOnClickListener(View.OnClickListener {
            when (menuList[position].menuItemName) {
                context.getString(R.string.logout_text) -> {
                    AppUtils.logOut(context, (context as HomeActivity).updateUser!!)
                }
                context.getString(R.string.my_cart) -> {
                    (context as HomeActivity).changeFragment(CartFragment(), (context as BaseActivity).callingFragment!!, R.id.containerMainFragment, false)
                }
                context.getString(R.string.orders_text) -> {
                    (context as HomeActivity).changeFragment(BookingFragment.bookingFragment(), (context as BaseActivity).callingFragment!!, R.id.containerMainFragment, false)
                }
                context.getString(R.string.appointments) -> {
                    (context as HomeActivity).changeFragment(BookingFragment.bookingFragment(), (context as BaseActivity).callingFragment!!, R.id.containerMainFragment, false)
                }
                context.getString(R.string.faq_text) -> {
                    (context as HomeActivity).changeFragment(ContactUsFAQsFragment.contactUsFaqFragment("https://gharpar.co/app-faq/"), (context as BaseActivity).callingFragment!!, R.id.containerMainFragment, true)
                }

            }
        })
    }


    public class MenuItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val itemImageView = view.findViewById<ImageView>(R.id.itemImageView)!!
        val itemNameTextView = view.findViewById<TextView>(R.id.itemNameTextView)!!
        val clItemMenu = view.findViewById<ConstraintLayout>(R.id.itemMenuLayout)!!
    }
}