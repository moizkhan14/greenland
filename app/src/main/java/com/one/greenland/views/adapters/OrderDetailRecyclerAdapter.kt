package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.models.OrderModel
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class OrderDetailRecyclerAdapter(val context: Context, private val orderModel: OrderModel) : RecyclerView.Adapter<OrderDetailRecyclerAdapter.OrderDetailsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDetailsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_service_order, parent, false)
        return OrderDetailsViewHolder(view)
    }

    override fun getItemCount(): Int {
        if (!orderModel.order_jobs.isNullOrEmpty()) {
            return orderModel.order_jobs!!.size
        }
        return orderModel.order_services!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: OrderDetailsViewHolder, position: Int) {

        if (!orderModel.order_jobs.isNullOrEmpty() && orderModel.status!!.toLowerCase() != "pending") {
            holder.clOrder.visibility = View.GONE
            holder.clTech.visibility = View.VISIBLE
            holder.tvJobIdTech.text = "${context.getString(R.string.job_id_text)}${orderModel.order_jobs!![position].job_code}"
            if (orderModel.order_jobs!![position].technician != null) {
                /*   if (!orderModel.order_services!![position].technician!!.user_name.isNullOrEmpty())
                       holder.tvTechName.text = orderModel.order_jobs!![position].technician!!.user_name
                   else*/
                holder.tvTechName.text = orderModel.order_jobs!![position].technician!!.first_name + " " + orderModel.order_jobs!![position].technician!!.last_name
            }
            holder.tvStartTimeValueTech.text = orderModel.order_jobs!![position].start_time
            holder.tvOrderStatusTech.text = orderModel.status

            if (orderModel.order_jobs!![position].job_duration != null) {
                holder.tvDuration.text = context.getString(R.string.duration_text)
                holder.tvDurationValueTech.text = orderModel.order_jobs!![position].job_duration.toString()
            } else {
                holder.tvDurationValueTech.text = orderModel.order_services!![position].end_time
            }

            if (orderModel.status == "Confirmed") {
                holder.tvOrderStatusTech.visibility = View.VISIBLE
            }

            Glide.with(context).load(orderModel.order_jobs!![position].technician!!.profile_photo_url).placeholder(R.drawable.ic_user).into(holder.civTech)

            var details = orderModel.order_jobs!![position].order_job_services!![0].service_title
            if (orderModel.order_jobs!![position].order_job_services!!.size > 1) {
                for (index in 1 until orderModel.order_jobs!![position].order_job_services.size)
                    details += " + " + orderModel.order_jobs!![position].order_job_services[index].service_title!!.capitalize()
            }
            /*var details = orderModel.order_services!![position].service_category_title!!.capitalize()
            for (addOns in orderModel.order_services!![position].order_service_addons!!)
                details += "+" + addOns.service_addon_title!!.capitalize()*/
            holder.tvServiceDetailsTech.text = details
        } else {
            holder.clOrder.visibility = View.VISIBLE
            holder.clTech.visibility = View.GONE

            //var details = orderModel.order_services!![position].service_category_title!!.capitalize()
            var details = orderModel.order_services!![position].service_title!!.capitalize()
            for (addOns in orderModel.order_services!![position].order_service_addons!!)
                details += "+" + addOns.service_addon_title!!.capitalize()
            //holder.tvServiceDetails.text = details
            holder.tvServiceDetails.text = details
            holder.tvServiceName.text = orderModel.order_services!![position].service_category_title!!.capitalize()
        }
    }

    public class OrderDetailsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        // Layout with Simple Order Details .... Currently this is VISIBLE
        val clOrder = view.findViewById<ConstraintLayout>(R.id.clOrder)
        val tvServiceName = view.findViewById<TextView>(R.id.tvServiceName)
        val tvServiceDetails = view.findViewById<TextView>(R.id.tvServiceDetails)

        // Layout with technician Details .... Currently this is GONE
        val clTech = view.findViewById<ConstraintLayout>(R.id.clTech)
        val civTech = view.findViewById<CircleImageView>(R.id.civTech)
        val tvTechName = view.findViewById<TextView>(R.id.tvTechName)
        val tvJobIdTech = view.findViewById<TextView>(R.id.tvJobIdTech)
        val tvServiceDetailsTech = view.findViewById<TextView>(R.id.tvServiceDetailsTech)
        val tvStartTimeValueTech = view.findViewById<TextView>(R.id.tvStartTimeValueTech)
        val tvDurationValueTech = view.findViewById<TextView>(R.id.tvDurationValueTech)
        val tvDuration = view.findViewById<TextView>(R.id.tvDuration)
        val tvOrderStatusTech = view.findViewById<TextView>(R.id.tvOrderStatusTech)
    }
}