package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.models.OrderService

class OrderSummaryRecyclerAdapter(val context: Context, private val orderServices: List<OrderService>) : RecyclerView.Adapter<OrderSummaryRecyclerAdapter.OrderSummaryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderSummaryViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_order_summary_recycler, parent, false)

        return OrderSummaryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return orderServices.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: OrderSummaryViewHolder, position: Int) {

        holder.tvServiceCounter.text = "${"x"}${orderServices[position].unit_count}"
        holder.tvServiceName.text = orderServices[position].service_title!!.capitalize()
        val price = orderServices[position].unit_price.toInt() * orderServices[position].unit_count
        holder.tvServicePrice.text = "${"Rs. "}${price}"

        if (orderServices[position].order_service_addons.size > 0)
            if (orderServices[position].order_service_addons!!.size > 0) {
                holder.clSubCategory.visibility = View.VISIBLE
                holder.rvAddonSummary.layoutManager = LinearLayoutManager(context)
                holder.rvAddonSummary.adapter = AddOnSummaryRecyclerAdapter(context, orderServices[position].order_service_addons!!)
            }
    }

    public class OrderSummaryViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvServiceCounter = view.findViewById<TextView>(R.id.tvServiceCounter)
        val tvServiceName = view.findViewById<TextView>(R.id.tvServiceName)
        val tvServicePrice = view.findViewById<TextView>(R.id.tvServicePrice)

        val clSubCategory = view.findViewById<ConstraintLayout>(R.id.clSubCategory)
        val rvAddonSummary = view.findViewById<RecyclerView>(R.id.rvAddonSummary)
    }

}