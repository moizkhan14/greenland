package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.common.CLICK_ID_POPULAR_SERVICES
import com.one.greenland.interfaces.ItemClickListener
import com.one.greenland.models.PopularServicesModel
import com.bumptech.glide.Glide

class PopularServicesRecyclerAdapter(
    var context: Context,
    var popularServicesList: MutableList<PopularServicesModel>,
    private var itemClickListener: ItemClickListener
) : RecyclerView.Adapter<PopularServicesRecyclerAdapter.PopularServiceViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularServiceViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_popular_services, parent, false)
        return PopularServiceViewHolder(view)
    }

    fun updateItem(list: MutableList<PopularServicesModel>){
        popularServicesList = list
        notifyDataSetChanged()
    }

    fun clearData(){
        popularServicesList.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return popularServicesList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: PopularServiceViewHolder, position: Int) {

        Glide.with(context)
            .load(popularServicesList[position].cover_image)
            .into(holder.serviceImageView)
        holder.serviceNameTextView.text = popularServicesList[position].service_title
        val price = (popularServicesList[position].service_price.toDouble()).toInt()
        holder.servicePriceTextView.text = "${context.getString(R.string.initial_price_text)} ${price}."

        holder.serviceConstraintLayout.setOnClickListener {
            itemClickListener.itemClicked(CLICK_ID_POPULAR_SERVICES,popularServicesList[position])
        }
    }

    public class PopularServiceViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var serviceImageView = view.findViewById<ImageView>(R.id.serviceImageView)
        var serviceNameTextView = view.findViewById<TextView>(R.id.serviceNameTextView)
        var servicePriceTextView = view.findViewById<TextView>(R.id.servicePriceTextView)
        var serviceConstraintLayout = view.findViewById<CardView>(R.id.serviceConstraintLayout)

    }
}