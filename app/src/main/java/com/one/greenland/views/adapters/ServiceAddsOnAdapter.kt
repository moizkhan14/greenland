package com.one.greenland.views.adapters

import android.content.Context
import android.graphics.drawable.AnimatedVectorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.one.greenland.R
import com.one.greenland.common.CROSS_TO_TICK
import com.one.greenland.common.TICK_TO_CROSS
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.interfaces.ServiceItemClickListener
import com.one.greenland.models.OrderServiceAddon
import com.one.greenland.models.ServiceAddon
import com.one.greenland.views.activities.HomeActivity
import java.lang.Exception

/**
 * created by Moiz Khan
 */

class ServiceAddsOnAdapter(
        private val context: Context,
        private val serviceAddon: List<ServiceAddon>,
        private val view: View?,
        private val serviceId: Int,
        private val dataUpdateListener: DataUpdateListener?,
        private val serviceItemClickListener: ServiceItemClickListener?,
        private var animationName: String = ""

) : RecyclerView.Adapter<ServiceAddsOnAdapter.ServiceDetailsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceDetailsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_service_details_recycler, parent, false)

        return ServiceDetailsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return serviceAddon.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ServiceDetailsViewHolder, position: Int) {
        holder.tvServiceName.text = serviceAddon[position].service_title
        holder.tvPrice.text = "Rs. " + serviceAddon[position].service_addon_price.toInt()
        val service = (context as HomeActivity).orderServiceList.find { it.service_id == serviceId }
        if (service != null) {
            for (orderServiceAddon in service.order_service_addons)
                if (orderServiceAddon.service_id == serviceAddon[position].id) {
                    holder.cbItemAdded.isChecked = true
                    if (animationName != CROSS_TO_TICK)
                        changeTickCross()
                    break
                }
        } else if ((context).popularServiceOrderServiceAddons.size > 0) {
            for (orderServiceAddon in context.popularServiceOrderServiceAddons)
                if (orderServiceAddon.service_id == serviceAddon[position].id) {
                    holder.cbItemAdded.isChecked = true
                    if (animationName != CROSS_TO_TICK)
                        changeTickCross()
                    break
                }
        }
        holder.clService.setOnClickListener {
            holder.cbItemAdded.isChecked = !holder.cbItemAdded.isChecked
            handleAddon(holder, position)

        }
        holder.cbItemAdded.setOnClickListener {
            handleAddon(holder, position)
        }
    }

    private fun handleAddon(holder: ServiceDetailsViewHolder, position: Int) {
        if (holder.cbItemAdded.isChecked) {

            serviceItemClickListener?.itemSelected(serviceId)
            val addOns = OrderServiceAddon()
            addOns.service_id = serviceAddon[position].id
            addOns.unit_count = 1
            addOns.unit_price = serviceAddon[position].service_addon_price
            addOns.service_addon_title = serviceAddon[position].service_title
            try {
                if ((context as HomeActivity).orderServiceList.find { it.service_id == serviceId } != null && serviceItemClickListener != null) {
                    (context as HomeActivity).orderServiceList.find { it.service_id == serviceId }!!.order_service_addons.add(addOns)
                } else {
                    (context as HomeActivity).popularServiceOrderServiceAddons.add(addOns)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (animationName != CROSS_TO_TICK)
                changeTickCross()

//                if(service == null){
//                    view?.ivCancel?.performClick()
//                }

        } else {
            // try {
            if ((context as HomeActivity).orderServiceList.size > 0) {
                try {

                    for (orderServiceAddon in (context as HomeActivity).orderServiceList.find { it.service_id == serviceId }!!.order_service_addons)
                        if (orderServiceAddon.service_id == serviceAddon[position].id) {
                            (context as HomeActivity).orderServiceList.find { it.service_id == serviceId }!!.order_service_addons.remove(orderServiceAddon)
                            break
                        }

                    if ((context as HomeActivity).orderServiceList.find { it.service_id == serviceId }!!.order_service_addons.size < 1) {
                        changeTickCross()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if ((context as HomeActivity).popularServiceOrderServiceAddons.size > 0) {
                for (orderServiceAddon in (context as HomeActivity).popularServiceOrderServiceAddons)
                    if (orderServiceAddon.service_id == serviceAddon[position].id) {
                        (context as HomeActivity).popularServiceOrderServiceAddons.remove(orderServiceAddon)
                        break
                    }

                if ((context as HomeActivity).popularServiceOrderServiceAddons.size < 1) {
                    changeTickCross()
                }
            }
            /*} catch (e: Exception) {
                e.printStackTrace()
            }*/
        }
        dataUpdateListener?.onUpdateData()
    }

    private fun changeTickCross() {
        view?.let {
            val v: ImageView = it.findViewById(R.id.ivCancel) as ImageView
            animationName = if (animationName != CROSS_TO_TICK) {
                v.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.cross_to_tick))
                CROSS_TO_TICK
            } else {
                v.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.tick_to_cross));
                TICK_TO_CROSS
            }
            val d = v.drawable
            if (d is AnimatedVectorDrawableCompat) {
                val avd: AnimatedVectorDrawableCompat = d
                avd.start()
            } else if (d is AnimatedVectorDrawable) {
                val avd: AnimatedVectorDrawable = d
                avd.start()
            }
        }
    }

    public class ServiceDetailsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cbItemAdded = view.findViewById<CheckBox>(R.id.cbItemAdded)!!
        val tvServiceName = view.findViewById<TextView>(R.id.tvServiceName)!!
        val tvPrice = view.findViewById<TextView>(R.id.tvPrice)!!
        val clService = view.findViewById<ConstraintLayout>(R.id.clService)!!
    }

}