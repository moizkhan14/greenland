package com.one.greenland.views.adapters

import android.animation.LayoutTransition
import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.one.greenland.R
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.interfaces.ServiceItemClickListener
import com.one.greenland.interfaces.WindowStateListener
import com.one.greenland.models.OrderService
import com.one.greenland.models.Service
import com.one.greenland.utils.AppUtils.showAddOnDialog
import com.one.greenland.views.activities.HomeActivity
import java.util.*

/**
 * created by Moiz Khan
 */

class ServiceDetailsAdapter(
        private val context: Context,
        private val services: List<Service>,
        private val categoryTitle: String,
        private val dataUpdateListener: DataUpdateListener)
    : RecyclerView.Adapter<ServiceDetailsAdapter.ServiceDetailsViewHolder>(), ServiceItemClickListener, WindowStateListener {

    var isAdOnDialog = false
    var name: String? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceDetailsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_service_details_recycler, parent, false)

        return ServiceDetailsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return services.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ServiceDetailsViewHolder, position: Int) {

        holder.tvServiceName.text = services[position].service_title
        holder.tvPrice.text = "Rs. " + services[position].service_price?.toInt()
        if (services[position].service_addons!!.isNotEmpty())
            holder.tvAddOns.visibility = View.VISIBLE
        for (orderService in (context as HomeActivity).orderServiceList)
            if (orderService.service_id == services[position].id) {
                holder.cbItemAdded.isChecked = true
                break
            }

        holder.tvAddOns.setOnClickListener {
//            if (!holder.cbItemAdded.isChecked)
//                holder.cbItemAdded.isChecked = true
//            else
            if (!isAdOnDialog)
                showAddOnDialog(context, services[position].service_addons!!, services[position].id, dataUpdateListener, this, this)
        }

//        holder.clService.setOnClickListener {
//            holder.cbItemAdded.isChecked = !holder.cbItemAdded.isChecked
//        }

        try {
            name = services[position].image.toLowerCase(Locale.US)
            var id = 0
            id = context!!.resources.getIdentifier(name, "drawable", context.packageName)
            if (id != 0) {
                val drawable = ContextCompat.getDrawable(context, id)!!

                Glide
                        .with(context)
                        .load(drawable)
                        .centerCrop()
                        .placeholder(R.drawable.rosepeach)
                        .into(holder.itemImageView)


            }

            holder.tvDescription.text = services[position].description
        } catch (e: Exception) {
            Log.d("Exception In Image: ", e.toString())
        }


        holder.tvShowText.setOnClickListener {
            if (holder.tvShowText.text.toString().contains("more")) {
                holder.tvShowText.setText(R.string.see_less)

                if (holder.tvDescription.visibility == View.VISIBLE) {
                    holder.tvDescription.visibility = View.GONE
                    holder.tvShowText.setText(R.string.see_more)
                } else {
                    holder.tvDescription.visibility = View.VISIBLE
                    holder.tvShowText.setText(R.string.see_less)
                }
            } else {
                holder.tvShowText.setText(R.string.see_more)
                holder.tvDescription.visibility = View.GONE
            }
        }

        holder.cbItemAdded.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            /*  if((context as HomeActivity).orderServiceList.size >= 10 && isChecked) {
                  holder.cbItemAdded.isChecked = false
                  Toast.makeText(context,"You cannot select more than 10 services",Toast.LENGTH_LONG).show()
              }
              else {*/
            if (isChecked) {
                addService(position)
                if (services[position].service_addons!!.isNotEmpty() /*&& (context as HomeActivity).orderServiceList.size < 10*/)
                    if (!isAdOnDialog)
                        showAddOnDialog(context, services[position].service_addons!!, services[position].id, dataUpdateListener, this, this)
            } else {
                for (orderService in (context as HomeActivity).orderServiceList)
                    if (orderService.service_id == services[position].id) {
                        (context as HomeActivity).orderServiceList.remove(orderService)
                        break
                    }
            }
            //}
            dataUpdateListener.onUpdateData()
        })
    }

    private fun addService(position: Int) {
        val orderService: OrderService = OrderService()
        if ((context as HomeActivity).orderServiceList.find { it.service_id == services[position].id } == null) {
            orderService.service_id = services[position].id
            orderService.unit_count = 1
            orderService.service_title = services[position].service_title
            orderService.service_category_title = categoryTitle
            orderService.unit_price = services[position].service_price
            (context as HomeActivity).orderServiceList.add(orderService)
        }
    }

    public class ServiceDetailsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val cbItemAdded = view.findViewById<CheckBox>(R.id.cbItemAdded)!!
        val tvServiceName = view.findViewById<TextView>(R.id.tvServiceName)!!
        val tvPrice = view.findViewById<TextView>(R.id.tvPrice)!!
        val tvAddOns = view.findViewById<TextView>(R.id.tvAddOns)!!
        val tvShowText = view.findViewById<TextView>(R.id.tvShowText)!!
        val tvDescription = view.findViewById<TextView>(R.id.tvDescription)!!
        val itemImageView = view.findViewById<ImageView>(R.id.itemImageView)!!
        val clService = view.findViewById<ConstraintLayout>(R.id.clService)!!
    }

    override fun itemSelected(service: Any) {
        val serviceId = service as Int
        val targetService = services.find { it.id == serviceId }
        val servicePosition = services.indexOf(targetService)
        addService(servicePosition)
        notifyItemChanged(servicePosition)
    }

    override fun itemDeselected(service: Any) {
        TODO("Not yet implemented")
    }

    override fun isWindowOpen(isOpen: Boolean) {
        isAdOnDialog = isOpen
    }

}