package com.one.greenland.views.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.models.ServiceCategoriesModel
import com.one.greenland.views.fragments.ServiceDetailsFragment

/**
 * created by Moiz Khan
 */

class ServiceDetailsPagerAdapter(fragmentManager: FragmentManager, private val serviceCategories: MutableList<ServiceCategoriesModel>,
                                 private val dataUpdateListener: DataUpdateListener) :
        FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        val serviceDetailsFragment = ServiceDetailsFragment.newInstance(serviceCategories[position])
        serviceDetailsFragment.setCartUpdateListener(dataUpdateListener)
        return serviceDetailsFragment
    }

    override fun getCount(): Int {
        return serviceCategories.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (serviceCategories[position].service_category_title.isNullOrEmpty()) "Unknown" else serviceCategories[position].service_category_title.capitalize()
    }
}