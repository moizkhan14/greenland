package com.one.greenland.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.models.Service
import com.one.greenland.models.SubCategoriesModel
import com.one.greenland.views.activities.HomeActivity

/**
 * created by Moiz Khan
 */

class SubCategoryAdapter(private val context: Context, private val subCategoriesModel: SubCategoriesModel, private val categoryTitle: String,
                         private val dataUpdateListener: DataUpdateListener) : RecyclerView.Adapter<SubCategoryAdapter.SubCategoriesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubCategoriesViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_sub_category, parent, false)

        return SubCategoriesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return subCategoriesModel.subcategories.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: SubCategoriesViewHolder, position: Int) {

        holder.tvSubCategoryName.text = subCategoriesModel.subcategories[position].service_category_title
        holder.clSubCategory.setOnClickListener {
            if (holder.rvServices.visibility == View.GONE) {
                notifyDataSetChanged()
                holder.ivDropDownArrow.setImageResource(R.drawable.ic_keyboard_up)
                holder.rvServices.visibility = View.VISIBLE
            } else {
                notifyDataSetChanged()
                holder.ivDropDownArrow.setImageResource(R.drawable.ic_keyboard_down)
                holder.rvServices.visibility = View.GONE
            }
        }
        setupRecyclerView(holder, subCategoriesModel.subcategories[position].services,subCategoriesModel.subcategories[position].service_category_title!!)
    }

    private fun setupRecyclerView(holder: SubCategoriesViewHolder, services: List<Service>, serviceCategoryTitle: String) {
        val layoutManager = LinearLayoutManager(context)

        var servicePosition = 0
        val service = services.find { it.id == (context as HomeActivity).serviceItemId }
        if (service != null) {
            servicePosition = services.indexOf(service)
            layoutManager.scrollToPositionWithOffset(servicePosition, 20)
        }
        holder.rvServices.layoutManager = layoutManager
       // val itemDecor = DividerItemDecoration(context, LinearLayout.VERTICAL)
        //holder.rvServices.addItemDecoration(itemDecor)
        val adapter = ServiceDetailsAdapter(context, services,
                "$categoryTitle -> $serviceCategoryTitle", dataUpdateListener)
        holder.rvServices.adapter = adapter
        //holder.rvServices.visibility = View.GONE
        //notifyDataSetChanged()
    }

    public class SubCategoriesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rvServices = view.findViewById<RecyclerView>(R.id.rvServices)!!
        val tvSubCategoryName = view.findViewById<TextView>(R.id.tvSubCategoryName)!!
        val ivDropDownArrow = view.findViewById<ImageView>(R.id.ivDropDownArrow)!!
        val clSubCategory = view.findViewById<ConstraintLayout>(R.id.clSubCategory)!!
    }

}