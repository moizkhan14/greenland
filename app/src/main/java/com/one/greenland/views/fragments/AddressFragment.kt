package com.one.greenland.views.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.one.greenland.R
import com.one.greenland.common.USER_MODEL
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.models.AddressModel
import com.one.greenland.models.AreaModel
import com.one.greenland.models.CityModel
import com.one.greenland.models.User
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import kotlinx.android.synthetic.main.fragment_address.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*

/**
 * A simple [Fragment] subclass.
 */
class AddressFragment : BaseFragment(), View.OnClickListener {

    private var city = -1
    private var area = -1
    private var cityName = ""
    private var areaName = ""
    private lateinit var mContext: Context
    private lateinit var mView: View
    private var cities: MutableList<CityModel> = mutableListOf()
    private var areasLahore: MutableList<AreaModel> = mutableListOf()
    private var areasIslamabad: MutableList<AreaModel> = mutableListOf()

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_address
    }

    override fun getFragmentName(): String? {
        return "Address Fragment"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = requireContext()
        registerClicks()
        mView = view
        val lahore = CityModel("Lahore", 0, true, false)
        val islamabad = CityModel("Islamabad", 1, true, false)
        getIslamabadAreas()
        getLahoreAreas()
        cities.add(lahore)
        cities.add(islamabad)
        setupCitySpinner(cities)
        imeOption()
    }

    private fun getLahoreAreas() {
        val area = AreaModel("Allama Iqbal Town", 0, 0)
        val area1 = AreaModel("Samnabad", 0, 1)
        val area2 = AreaModel("Model Town", 0, 2)
        val area3 = AreaModel("Wapda Town", 0, 3)
        val area4 = AreaModel("Sadar Cantt", 0, 4)
        areasLahore.add(area)
        areasLahore.add(area1)
        areasLahore.add(area2)
        areasLahore.add(area3)
        areasLahore.add(area4)
    }

    private fun getIslamabadAreas() {
        val area = AreaModel("Bahria Town", 1, 0)
        val area1 = AreaModel("I8", 1, 1)
        val area2 = AreaModel("G10", 1, 2)
        val area3 = AreaModel("PWD Colony", 1, 3)
        val area4 = AreaModel("Blue Area", 1, 4)
        areasIslamabad.add(area)
        areasIslamabad.add(area1)
        areasIslamabad.add(area2)
        areasIslamabad.add(area3)
        areasIslamabad.add(area4)
    }

    private fun imeOption() {
        etAddress.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                if (!etAddress.text.isNullOrEmpty()) {
                    createAddress()
                } else {
                    Toast.makeText(mContext, "Please enter address first.", Toast.LENGTH_SHORT).show()
                }
            } else if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_NEXT) {
                spArea.requestFocus()
            }
            false
        }
    }

    private fun registerClicks() {
        submitImageButton.setOnClickListener(this)
        backImageView.setOnClickListener(this)
    }

    private fun getAreas(city: String) {
        when (city.toLowerCase()) {
            "lahore" -> {
                setupAreasSpinner(areasLahore)
            }
            "islamabad" -> {
                setupAreasSpinner(areasIslamabad)
            }
        }
    }

    private fun createAddress() {
        if (AppUtils.isValidAddress(etAddress)) {
            submitImageButton.isEnabled = false
            val addressModel = AddressModel()
            if ((mContext as BaseActivity).callingFragment == ScheduleServiceFragment.scheduleServiceFragment().getFragmentName()) {
                if (!(mContext as HomeActivity).getAppUser().cityId.isNullOrEmpty())
                    addressModel.city_id = cityName
                else {
                    addressModel.city_id = cityName

                }
            } else {
                addressModel.city_id = cityName
            }

            addressModel.area_id = areaName
            addressModel.address_1 = etAddress.text.toString()
            addressModel.is_default = true

            val user = Gson().fromJson(PreferenceUtils.getModelForKey(USER_MODEL), User::class.java)
            user.addresses.add(addressModel)
            (context as HomeActivity).updateUser = user
            PreferenceUtils.putModel(USER_MODEL, user)
            AppUtils.hideKeyboardFromFragment(mContext, mView)
            onBackPressed()
        }
    }

    private fun setupCitySpinner(cities: MutableList<CityModel>) {
        spArea.visibility = View.GONE
        val cityStringArray: MutableList<String> = mutableListOf()
        if ((mContext as BaseActivity).callingFragment == ScheduleServiceFragment.scheduleServiceFragment().getFragmentName()
                && !(mContext as HomeActivity).getAppUser().cityId.isNullOrEmpty())
            cityStringArray.add(cities.find { it.id == (mContext as HomeActivity).getAppUser().cityId!!.toInt() }!!.city_name)
        for (index in 0 until cities.size) {
            cityStringArray.add(cities[index].city_name)
        }
        val citySpinnerArrayAdapter: ArrayAdapter<*> = ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, cityStringArray)
        spCity.adapter = citySpinnerArrayAdapter
        spCity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.d("Noting", "Nothing Selected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                city = cities[position].id
                cityName = spCity.selectedItem.toString()
                getAreas(spCity.selectedItem.toString())
            }
        }
        if ((mContext as HomeActivity).callingFragment == ScheduleServiceFragment.scheduleServiceFragment().getFragmentName()
                && !(mContext as HomeActivity).getAppUser().cityId.isNullOrEmpty()) {
            spCity.isEnabled = false
            spCity.isClickable = false
            ivAddressDropDown.visibility = View.INVISIBLE
        }
    }

    override fun onBackPressed() {
        for (index in 0..1)
            (activity as BaseActivity).supportFragmentManager.popBackStack()
        if ((mContext as BaseActivity).callingFragment == ScheduleServiceFragment.scheduleServiceFragment().getFragmentName()) {
            (mContext as BaseActivity).changeFragment(ScheduleServiceFragment.scheduleServiceFragment(), getFragmentName()!!, R.id.containerMainFragment, true)
        } else if ((mContext as BaseActivity).callingFragment == FragmentUpdateProfile().getFragmentName()) {
            (mContext as BaseActivity).changeFragment(FragmentUpdateProfile.fragmentUpdateProfile(false),
                    getFragmentName()!!, R.id.containerMainFragment, true)
            //(mContext as  BaseActivity).onSuperBackPressed()
        }
    }

    private fun setupAreasSpinner(areas: MutableList<AreaModel>) {
        spArea.visibility = View.VISIBLE
        val areasStringArray: MutableList<String> = mutableListOf()
        for (index in 0 until areas.size) {
            areasStringArray.add(areas[index].area)
        }
        val areaSpinnerArrayAdapter: ArrayAdapter<*> = ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, areasStringArray)
        spArea.adapter = areaSpinnerArrayAdapter

        spArea.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                spArea.selectedItem.toString()
                if (areas.size > 0) {
                    area = areas[position].id
                    areaName = spArea.selectedItem.toString()
                }
            }
        }
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.submitImageButton -> {
                    if (AppUtils.isValidEditText(etAddress, getString(R.string.address_error_message))) {
                        createAddress()
                    } else {
                        Toast.makeText(mContext, "Please enter address first.", Toast.LENGTH_SHORT).show()
                    }
                }
                R.id.backImageView -> {
                    onBackPressed()
                }
            }
        }
    }

}
