package com.one.greenland.views.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.interfaces.BaseFragmentListener

abstract class BaseFragment : Fragment() {
    override fun onResume() {
        super.onResume()
    }

    /**
     * This method makes sure that whether the fragment should appear when back button is pressed or not
     *
     * @return true if you want to add fragment to backstack otherwise false
     */
    abstract fun addToBackStack(): Boolean

    abstract fun getRootLayoutId(): Int

    abstract fun getFragmentName(): String?
    open fun onBackPressed() {
        (activity as BaseActivity?)!!.onSuperBackPressed()
    }

    /**
     * set the base fragment listener after getting in onAttach
     * please replace your basefragmentlistener with the one provided
     * in the arguments
     *
     * @param baseFragmentListener
     */
    abstract fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (activity is BaseFragmentListener) setBaseFragmentListener(activity as BaseFragmentListener?) else if (parentFragment is BaseFragmentListener) setBaseFragmentListener(parentFragment as BaseFragmentListener?)
    }

    /**
     * if you are receiving extras from an activity through intent, Please get those extras
     * in this method. This method is called in the base Activity onCreate. Get all your arguments
     * by the provided arguments variable passed as an argument
     *
     * @param arguments get your data from this variable e.g. arguments.getSringExtra("your code");
     */
    abstract fun receiveExtras(arguments: Bundle?)

    fun loadFragment(baseFragment: BaseFragment?, fragmentToLoadLayoutId: Int) {
        childFragmentManager.beginTransaction().replace(fragmentToLoadLayoutId, baseFragment!!).commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            receiveExtras(arguments)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if (getRootLayoutId() == 0) {
            throw NullPointerException("No layout id returned in getRootLayoutId to inflate")
        } else {
            val view = inflater.inflate(getRootLayoutId(), container, false)
            view
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    //    @Override
//    public void onMessageReceived(Notification notification) {
//        int userId = (SharedData.getInstance().getProfile().getUserProfile() != null) ?
//                SharedData.getInstance().getProfile().getUserProfile().getUserId() :
//                SharedData.getInstance().getProfile().getAgencyProfile().getUserId();
//
//
//        if (notification.getNotificationTo().contains(userId))
//            new NotificationUtils().showSmallNotification(getActivity(), R.drawable.ic_app_logo,
//                    "New Message Received", notification.getNotificationMessage());
//    }
    fun handleSearchQuery(searchQuery: String?) {}

    fun onDataSetChanged() {}
}