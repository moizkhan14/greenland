package com.one.greenland.views.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.one.greenland.R
import com.one.greenland.common.BOOKING_FRAGMENT_TAG
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.models.OrderModel
import com.one.greenland.utils.AppUtils.showBottomNavigationView
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.BookingsRecyclerAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_booking.*


/**
 * A simple [Fragment] subclass.
 */
class BookingFragment : BaseFragment(), View.OnClickListener {


    private lateinit var mContext: Context
    private lateinit var adapterBookingRecycler: BookingsRecyclerAdapter
    private lateinit var orders: MutableList<OrderModel>

    companion object {
        fun bookingFragment(): BookingFragment {
            return BookingFragment()
        }
    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_booking
    }

    override fun getFragmentName(): String? {
        return BOOKING_FRAGMENT_TAG
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = requireContext()
        if ((mContext as HomeActivity).bottomNavigationView.visibility != View.VISIBLE)
            showBottomNavigationView(requireActivity(), (mContext as HomeActivity).bottomNavigationView)
        (mContext as HomeActivity).selectedBottomNavigation(R.id.bottomNavigationItemBooking)
        clFilledBooking.visibility = View.VISIBLE
        clNoBooking.visibility = View.GONE

        btnBooking.text = getString(R.string.book_now_text)
        registerClicks()


    }

    override fun onResume() {
        super.onResume()
        if (this.isVisible) {
            if ((context as HomeActivity).updateUser!!.orders.size > 0) {
                (context as HomeActivity).updateUser!!.orders.sortByDescending { it.id }
                orders = (context as HomeActivity).updateUser!!.orders
                setupRecyclerView()
            } else {
                clNoBooking.visibility = View.VISIBLE
                tvEmptyCart.text = getString(R.string.no_booking)
                tvEmptyCartDetails.text = getString(R.string.no_booking_details)
                btnBooking.text = getString(R.string.book_now_text)
                btnBooking.visibility = View.VISIBLE
                rvBookings.visibility = View.GONE

            }

        }
    }

    private fun registerClicks() {
        btnBooking.setOnClickListener(this)
    }

    private fun setupRecyclerView() {

        registerClicks()
        if (orders.size > 0) {

            rvBookings.visibility = View.VISIBLE
            rvBookings.layoutManager = LinearLayoutManager(context)
            (mContext as BaseActivity).callingFragment = getFragmentName()!!

            //adapterBookingRecycler = BookingsRecyclerAdapter(mContext, orders.sortedByDescending { it.id } as MutableList<OrderModel>)
            adapterBookingRecycler = BookingsRecyclerAdapter(mContext, orders)
            rvBookings.adapter = adapterBookingRecycler

            clFilledBooking.visibility = View.VISIBLE
            clNoBooking.visibility = View.GONE

            clNoBooking.visibility = View.GONE

        } else {

            clNoBooking.visibility = View.VISIBLE
            tvEmptyCart.text = getString(R.string.no_booking)
            tvEmptyCartDetails.text = getString(R.string.no_booking_details)
            btnBooking.text = getString(R.string.book_now_text)
            btnBooking.visibility = View.VISIBLE
            rvBookings.visibility = View.GONE


        }
    }

    override fun onBackPressed() {
        if ((mContext as BaseActivity).latestFragmentFromBackStack != null) {
            var selectedFragment = 0
            selectedFragment = when {
                (mContext as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == CartFragment().getFragmentName() -> R.id.bottomNavigationItemCart
                (mContext as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == MenuFragment().getFragmentName() -> R.id.bottomNavigationItemMore
                else -> R.id.bottomNavigationItemHome
            }

            (mContext as HomeActivity).selectedBottomNavigation(selectedFragment)
        }
        (mContext as BaseActivity).onSuperBackPressed()

    }

    override fun onClick(v: View?) {
        v?.let {
            when (v.id) {
                R.id.btnBooking -> {
                    onBackPressed()
                }
            }
        }
    }

}
