package com.one.greenland.views.fragments

import android.content.Context
import android.graphics.Canvas
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.one.greenland.R
import com.one.greenland.common.CART_FRAGMENT_TAG
import com.one.greenland.common.FLOWERING_PLANTS
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.models.Service
import com.one.greenland.models.ServiceCategoriesModel
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.showBottomNavigationView
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.CartRecyclerAdapter
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_cart.*


/**
 * created by Moiz Khan
 */
class CartFragment : BaseFragment(), View.OnClickListener, DataUpdateListener {

    private var serviceCategories: MutableList<ServiceCategoriesModel> = mutableListOf()
    private var selectedCategory: Int? = null
    private var totalBill = 0
    private lateinit var mContext: Context
    private var categoriesItemList: MutableList<ServiceCategoriesModel> = mutableListOf()

    //Constructor
    companion object {
        fun cartFragment(serviceCategories: MutableList<ServiceCategoriesModel>, selectedCategory: Int): CartFragment {
            val fragmentObject = CartFragment()

            val bundle = Bundle().apply {
                putParcelableArrayList("service_categories", ArrayList<Parcelable>(serviceCategories))
                putInt("selectedCategory", selectedCategory)
            }
            fragmentObject.arguments = bundle
            return fragmentObject
        }
    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_cart
    }

    override fun getFragmentName(): String? {
        return CART_FRAGMENT_TAG
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {
        if (arguments != null) {
            selectedCategory = arguments.getInt("selectedCategory", 0)
            this.serviceCategories = arguments.getParcelableArrayList<Parcelable>("service_categories")!! as MutableList<ServiceCategoriesModel>
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = requireContext()
        if ((mContext as BaseActivity).callingFragment == DetailsFragment().getFragmentName())
            showBottomNavigationView(mContext, (mContext as HomeActivity).bottomNavigationView)
        /*(container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)*/
        (mContext as HomeActivity).selectedBottomNavigation(R.id.bottomNavigationItemCart)
        registerClicks()
        setupViews()

    }

    private fun setupViews() {
        if ((mContext as HomeActivity).orderServiceList.size == 0) {
            clEmptyCart.visibility = View.VISIBLE
            clFilledCart.visibility = View.GONE
        } else {
            clEmptyCart.visibility = View.GONE
            clFilledCart.visibility = View.VISIBLE
            updateTotalAmount()
            setupCartRecycler()
        }
    }

    private fun updateTotalAmount() {
        var totalAmount: Double = 0.0
        for (orderService in (mContext as HomeActivity).orderServiceList) {
            totalAmount += orderService.unit_price * orderService.unit_count
            if (orderService.order_service_addons.size > 0)
                for (addOn in orderService.order_service_addons) {
                    totalAmount += addOn.unit_price * addOn.unit_count
                }
        }
        val amount = "Rs. ${totalAmount.toInt()}"
        tvTotalBillAmount.text = amount
        totalBill = totalAmount.toInt()
    }


    override fun onBackPressed() {

        if ((mContext as BaseActivity).latestFragmentFromBackStack != null) {
            var selectedFragment = 0
            selectedFragment = when {
                (mContext as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == BookingFragment().getFragmentName() -> R.id.bottomNavigationItemBooking
                (mContext as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == MenuFragment().getFragmentName() -> R.id.bottomNavigationItemMore
                else -> R.id.bottomNavigationItemHome
            }

            if ((mContext as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == DetailsFragment().getFragmentName())
                AppUtils.hideBottomNavigationView(mContext, (mContext as HomeActivity).bottomNavigationView)
            (mContext as HomeActivity).selectedBottomNavigation(selectedFragment)

        }

        (mContext as BaseActivity).onSuperBackPressed()
    }

    private fun setupCartRecycler() {
        rvCart.layoutManager = LinearLayoutManager(context)
        val adapter = CartRecyclerAdapter(mContext, this)
        rvCart.adapter = adapter
        val simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {

                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                //Remove swiped item from list and notify the RecyclerView
                val position = viewHolder.adapterPosition
                (mContext as HomeActivity).orderServiceList.removeAt(position)
                //Toast.makeText(context, "Item Removed", Toast.LENGTH_SHORT).show()

                updateTotalAmount()
                (mContext as HomeActivity).updateHomeCartBadge()
                (mContext as HomeActivity).setCartOrderRequest((mContext as HomeActivity).orderRequest)
                if ((mContext as HomeActivity).orderServiceList.size == 0) {
                    clEmptyCart.visibility = View.VISIBLE
                    clFilledCart.visibility = View.GONE
                }
                adapter.notifyItemRemoved(position)
            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addBackgroundColor(ContextCompat.getColor(mContext, R.color.colorRed))
                        .addActionIcon(R.drawable.ic_vector_delete)
                        .addSwipeLeftLabel(getString(R.string.remove_text))
                        .setSwipeLeftLabelColor(ContextCompat.getColor(mContext, R.color.colorWhite))
                        .create()
                        .decorate()
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }

        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(rvCart)
    }

    private fun registerClicks() {
        tvScheduleOder.setOnClickListener(this)
        tvAddMoreService.setOnClickListener(this)
        btnAddServiceToCart.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.tvScheduleOder -> {

                    billAmountValidation()

                }
                R.id.btnAddServiceToCart -> {
                    getServiceCategories()
                }
                R.id.tvAddMoreService -> {
                    getServiceCategories()
                }
            }
        }
    }

    private fun billAmountValidation() {
        if (totalBill < 1000) {
            Toast.makeText(context, "The minimum amount of order at least Rs 1000.", Toast.LENGTH_SHORT).show()
        } else {
            (requireActivity() as BaseActivity).changeFragment(ScheduleServiceFragment(), getFragmentName()!!, R.id.containerMainFragment, true)
        }
    }


    private fun getServiceCategories() {
        categoreis()
        val listType =
                object : TypeToken<ArrayList<Service>>() {

                }.type

        val serviceList = Gson().fromJson<MutableList<Service>>(
                FLOWERING_PLANTS,
                listType
        )


        if (serviceList.size > 0) {

            (mContext as BaseActivity).changeFragment(DetailsFragment.detailsFragment(
                    categoriesItemList,
                    0),
                    getFragmentName()!!, R.id.containerMainFragment,
                    true)
        } else {
            AppUtils.showErrorDialog((mContext as HomeActivity), getString(R.string.no_service))
        }
    }

    private fun categoreis() {
        val caterogy1 = ServiceCategoriesModel(1, R.drawable.flowering, "Flowering Plants")
        val caterogy2 = ServiceCategoriesModel(2, R.drawable.vegetable, "Vegetable Plants")
        val caterogy3 = ServiceCategoriesModel(3, R.drawable.fruit, "Fruit Plants")
        val caterogy4 = ServiceCategoriesModel(4, R.drawable.fertilizers, "Herbs and Fertilizers")

        categoriesItemList.add(caterogy1)
        categoriesItemList.add(caterogy2)
        categoriesItemList.add(caterogy3)
        categoriesItemList.add(caterogy4)
    }


    override fun onUpdateData() {
        updateTotalAmount()
    }
}
