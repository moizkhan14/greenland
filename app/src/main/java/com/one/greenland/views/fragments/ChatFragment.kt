package com.one.greenland.views.fragments

import android.animation.LayoutTransition
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.one.greenland.R
import com.one.greenland.common.USER_MODEL
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.models.ChatMessage
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.ChatMessagesAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*

class ChatFragment : BaseFragment(), View.OnClickListener {

    private lateinit var chatMessages: MutableList<ChatMessage>
    private lateinit var chatAdapter: ChatMessagesAdapter

    companion object {
        fun newInstance(): ChatFragment {
            return ChatFragment()
        }
    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_chat
    }

    override fun getFragmentName(): String? {
        return "Chat Fragment"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (view.findViewById(R.id.constraintLayoutChatMainHolder) as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)

        AppUtils.hideBottomNavigationView(context, (context as HomeActivity).bottomNavigationView)
        if ((context as HomeActivity).updateUser!!.chatMessages.isNullOrEmpty()) {
            chatMessages = mutableListOf()
            addChat()
        } else {
            chatMessages = (context as HomeActivity).updateUser!!.chatMessages!!
        }
        setToolBar()
        setUpRecyclerView()
        registerClicks()

    }

    private fun registerClicks() {
        imageViewComposeChatMessage.setOnClickListener(this)
        backImageView.setOnClickListener(this)
    }

    private fun setToolBar() {
        tvFragmentName.text = getString(R.string.chat_text)
        tvFragmentName.visibility = View.VISIBLE
    }

    private fun setUpRecyclerView() {
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        chatAdapter = ChatMessagesAdapter(chatMessages)
        recyclerViewAllChatMessages.layoutManager = mLayoutManager
        recyclerViewAllChatMessages.itemAnimator = DefaultItemAnimator()
        recyclerViewAllChatMessages.adapter = chatAdapter
        recyclerViewAllChatMessages.visibility = View.VISIBLE
        if (chatMessages.size > 0) recyclerViewAllChatMessages.scrollToPosition(chatMessages.size - 1)
    }

    private fun addChat() {
        val chat1 = ChatMessage(1, "Hi")
        val chat2 = ChatMessage(2, "Hello.\n How may I assist you?")
        val chat3 = ChatMessage(1, "I want gardner services at my home. How I can get them?")
        val chat4 = ChatMessage(2, "You can get these services from our Popular Services.")
        val chat5 = ChatMessage(1, "Thanks.")
        val chat6 = ChatMessage(2, "Its our pleasure to assist you.")

        chatMessages.add(chat1)
        chatMessages.add(chat2)
        chatMessages.add(chat3)
        chatMessages.add(chat4)
        chatMessages.add(chat5)
        chatMessages.add(chat6)
    }

    private fun populateUserMessage() {
        val chat = ChatMessage(1, editTextComposeChatMessage.text.toString())
        chatMessages.add(chat)
        chatAdapter.notifyDataSetChanged()
        editTextComposeChatMessage.text.clear()
    }

    private fun populateAdminMessage() {
        val chat = ChatMessage(2, getString(R.string.offline_chat_message))
        chatMessages.add(chat)
        chatAdapter.notifyDataSetChanged()
        (context as HomeActivity).updateUser!!.chatMessages = chatMessages
        PreferenceUtils.putModel(USER_MODEL, (context as HomeActivity).updateUser)
    }

    override fun onBackPressed() {
        AppUtils.showBottomNavigationView(requireContext(), (context as HomeActivity).bottomNavigationView)
        (context as HomeActivity).selectedBottomNavigation(R.id.bottomNavigationItemHome)
        super.onBackPressed()
    }

    override fun onClick(v: View?) {
        v?.let {
            when (v.id) {
                R.id.imageViewComposeChatMessage -> {
                    if (editTextComposeChatMessage.text.isNotEmpty()) {
                        populateUserMessage()
                        Handler(Looper.getMainLooper()).postDelayed({
                            populateAdminMessage()
                        }, 1000)
                    }
                }
                R.id.backImageView -> {
                    onBackPressed()
                }
            }
        }
    }
}