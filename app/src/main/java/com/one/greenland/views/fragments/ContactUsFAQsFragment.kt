package com.one.greenland.views.fragments

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.one.greenland.R
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.utils.AppUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_faqs.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ContactUsFAQsFragment : BaseFragment() {
    private var url = ""
    private lateinit var mContext: Context

    companion object {
        fun contactUsFaqFragment(url: String): ContactUsFAQsFragment {

            val fragmentObject = ContactUsFAQsFragment()

            val bundle = Bundle().apply {
                putString("url", url)
            }
            fragmentObject.arguments = bundle
            return fragmentObject
        }
    }

    override fun onBackPressed() {
        try {
            webView.stopLoading()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        (mContext as BaseActivity).onSuperBackPressed()
        activity?.let { AppUtils.showBottomNavigationView(it, (mContext as HomeActivity).bottomNavigationView) }
    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_faqs
    }

    override fun getFragmentName(): String? {
        return "FAQ Fragment"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {
        if (arguments != null) {
            url = arguments.getString("url", "")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = requireContext()
        activity?.let { AppUtils.hideBottomNavigationView(it, (mContext as HomeActivity).bottomNavigationView) }
        backImageView.setOnClickListener {
            onBackPressed()
        }
        if (url.isNotEmpty()) {
            when {
                url.contains("faq") -> {
                    tvFragmentName.text = getString(R.string.faq_text)
                    tvFragmentName.visibility = View.VISIBLE
                }
                url.toLowerCase(Locale.getDefault()).contains("covid") -> {
                    tvFragmentName.text = getString(R.string.covid)
                    tvFragmentName.visibility = View.VISIBLE
                }
                else -> {
                    tvFragmentName.text = getString(R.string.contact_us)
                    tvFragmentName.visibility = View.VISIBLE
                }
            }
            openUrl()
        }

    }

    override fun onResume() {
        super.onResume()

    }

    private fun openUrl() {
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                url?.let {
                    view?.loadUrl(it)
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                mContext?.let {

                }
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                mContext?.let {

                }
            }
        }
        webView.clearCache(true)
        webView.clearHistory()
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.builtInZoomControls = false
        webView.settings.domStorageEnabled = true;
        //webView.loadData(url, "text/html", "utf-8")
        webView.loadUrl(url)
    }

}
