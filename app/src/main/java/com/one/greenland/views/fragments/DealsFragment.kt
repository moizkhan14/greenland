package com.one.greenland.views.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.one.greenland.R
import com.one.greenland.models.DealsModel
import com.bumptech.glide.Glide

/**
 * A simple [Fragment] subclass.
 */
class DealsFragment : Fragment() {

    private var sliderImageView: ImageView? = null
    var introModel: DealsModel? = null
    var position: Int? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.item_deal_slider, container, false)
        viewInitializer(view)

        return view
    }

    private fun viewInitializer(view: View) {
        sliderImageView = view.findViewById(R.id.sliderImageView)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val circularProgressDrawable = CircularProgressDrawable(context!!)
//        circularProgressDrawable.strokeWidth = 5f
//        circularProgressDrawable.centerRadius = 30f
//        circularProgressDrawable.start()
        if(position==0){
            sliderImageView!!.setOnClickListener {
              //  (context as HomeActivity).changeFragment(ContactUsFAQsFragment.contactUsFaqFragment("https://gharpar.co/covid-19-pandemic-services/"), (context as BaseActivity).callingFragment!!, R.id.containerMainFragment, true)
            }
        }
        if (introModel != null && !introModel!!.deal_banner.isNullOrEmpty())
            Glide.with(this)
                    .load(introModel!!.deal_banner)
                    .into(sliderImageView!!)
    }

    private fun openUrlInBrowser() {
        val uri: Uri = Uri.parse("https://gharpar.co/covid-19-pandemic-services/") // missing 'http://' will cause crashed
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }

    companion object {
        fun newInstance(introModel: DealsModel, position: Int): DealsFragment {
            val viewFragment = DealsFragment()
            viewFragment.position = position
            viewFragment.introModel = introModel
            return viewFragment
        }
    }

}
