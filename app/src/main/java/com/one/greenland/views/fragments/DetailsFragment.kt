package com.one.greenland.views.fragments

import android.animation.LayoutTransition
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.one.greenland.R
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.models.ServiceCategoriesModel
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.ServiceDetailsPagerAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*

/**
 * created by Moiz Khan
 */
class DetailsFragment : BaseFragment(), DataUpdateListener {

    private var serviceCategories: MutableList<ServiceCategoriesModel> = mutableListOf()
    private var selectedCategoryPosition: Int? = null

    //Constructor
    companion object {
        fun detailsFragment(serviceCategories: MutableList<ServiceCategoriesModel>, selectedCategoryPosition: Int): DetailsFragment {
            val fragmentObject = DetailsFragment()

            val bundle = Bundle().apply {
                putParcelableArrayList("service_categories", ArrayList<Parcelable>(serviceCategories))
                putInt("selectedCategory", selectedCategoryPosition)
            }
            fragmentObject.arguments = bundle
            return fragmentObject
        }
    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_details
    }

    override fun getFragmentName(): String? {
        return "Fragment Details"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {
        if (arguments != null) {
            selectedCategoryPosition = arguments.getInt("selectedCategory", 0)
            this.serviceCategories = arguments.getParcelableArrayList<Parcelable>("service_categories")!! as MutableList<ServiceCategoriesModel>
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AppUtils.hideBottomNavigationView(requireContext(), (context as HomeActivity).bottomNavigationView)
        viewInitializer()
        backImageView.setOnClickListener {
            onBackPressed()
        }
        initializeAndLoadData()
    }

    private fun initializeAndLoadData() {
        vpServiceDetails.offsetLeftAndRight(2)
        vpServiceDetails.adapter = ServiceDetailsPagerAdapter(childFragmentManager, serviceCategories, this)
        if (selectedCategoryPosition != null) {
            vpServiceDetails.setCurrentItem(selectedCategoryPosition!!, true)
        }
        cartLayout.setOnClickListener {
            (requireActivity() as BaseActivity).changeFragment(CartFragment.cartFragment(serviceCategories, selectedCategoryPosition!!),
                    getFragmentName()!!, R.id.containerMainFragment, false)
        }
        updateCartBadge(false)
    }

    private fun updateCartBadge(shouldAnimate: Boolean) {
        if (shouldAnimate)
            cartLayout.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse_animation))

        if ((context as HomeActivity).orderServiceList.size > 0) {
            cartBadgeLayout.visibility = View.VISIBLE
            tvCartBadge.text = (context as HomeActivity).orderServiceList.size.toString()
        } else
            cartBadgeLayout.visibility = View.GONE

        (context as HomeActivity).orderModel.order_services = (context as HomeActivity).orderServiceList
        val orderRequest = (context as HomeActivity).getCartOrderRequest()
        orderRequest.order = (context as HomeActivity).orderModel
        (context as HomeActivity).setCartOrderRequest(orderRequest)

        (context as HomeActivity).updateHomeCartBadge()
    }

    private fun viewInitializer() {
        (toolbarLayout as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGE_APPEARING)
        (context as HomeActivity).selectedBottomNavigation(R.id.bottomNavigationItemHome)
        cartLayout.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        (context as BaseActivity).onSuperBackPressed()
        if ((context as BaseActivity).latestFragmentFromBackStack != null)
            if ((context as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == HomeFragment().getFragmentName()
                    || (context as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == CartFragment().getFragmentName()) {
                AppUtils.showBottomNavigationView(requireContext(), (context as HomeActivity).bottomNavigationView)
                (context as HomeActivity).registerClicks()
                (context as HomeActivity).focusListener()
            }

    }

    override fun onUpdateData() {
        updateCartBadge(true)
    }

}
