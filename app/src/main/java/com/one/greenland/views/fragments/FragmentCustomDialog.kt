package com.one.greenland.views.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.one.greenland.R
import com.one.greenland.common.*
import com.one.greenland.interfaces.DialogFragmentClicks
import com.one.greenland.models.DialogFragmentButtonProperties
import com.one.greenland.models.PopularServicesModel
import com.one.greenland.utils.AppUtils
import com.one.greenland.views.adapters.ServiceAddsOnAdapter
import kotlinx.android.synthetic.main.fragment_custom_dialog.*

class FragmentCustomDialog(
        private var dialogFragmentClicks: DialogFragmentClicks,
        private val isCancelableValue: Boolean,
        private val dialogType: String,
        private val model: Any?
) : DialogFragment() {

    private lateinit var rootView: View
    private var mContext: Context? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.let {
            it.window?.let { window ->
                // Hide the background. So we can have curved background.
                window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                window.requestFeature(Window.FEATURE_NO_TITLE)

                // Place the dialog at the bottom of the screen.
                window.setGravity(Gravity.BOTTOM)

                window.attributes.windowAnimations = R.style.DialogAnimation
            }
        }

        rootView = inflater.inflate(R.layout.fragment_custom_dialog, container)
        isCancelable = isCancelableValue
        mContext = context
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (dialogType) {
            MAIN_POPULAR_SERVICE_DIALOG_TYPE -> {
                showPopularServiceDialog(model as PopularServicesModel)
            }
            MAIN_POPULAR_SERVICE_ADD_ON_DIALOG_TYPE -> {
                showPopularServiceAddonsDialog(model as PopularServicesModel)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.let {
            it.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    private fun showPopularServiceDialog(model: PopularServicesModel) {

        popularServicesMainConstraintLayout.visibility = View.VISIBLE
        fragmentCustomDialogMainConstraintLayout.setPadding(50, 50, 50, 50)
        val price = model.service_price.split(".")[0]
        tvPrice.text = "Rs.${price}"
        tvServiceName.text = model.description
        tvCategoryType.text = model.service_category_title
        Glide.with(requireContext()).load(model.cover_image).into(imageService)

        if (model.service_addons.size > 0) {
            tvAddOn.visibility = View.VISIBLE
            tvAddOn.setOnClickListener {
                view?.let {
                    dialogFragmentClicks.onDialogFragmentViewClicked(DialogFragmentButtonProperties(
                            it,
                            POPULAR_SERVICE_ADD_ON_DIALOG
                    ))
                }
            }
        }

        addToCartButton.setOnClickListener {
            AppUtils.isPopularServiceDialogOpen = false
            view?.let {
                dialogFragmentClicks.onDialogFragmentViewClicked(DialogFragmentButtonProperties(
                        it,
                        POPULAR_SERVICE_DIALOG
                ))
            }

        }

        notNowButton.setOnClickListener {
            AppUtils.isPopularServiceDialogOpen = false
            this.dismiss()
        }

        cancelLayout.setOnClickListener {
            AppUtils.isPopularServiceDialogOpen = false
            this.dismiss()
        }
    }

    private fun showPopularServiceAddonsDialog(model: PopularServicesModel) {
        popularServicesMainConstraintLayout.visibility = View.GONE
        addOnsMainConstraintLayout.visibility = View.VISIBLE

        fragmentCustomDialogMainConstraintLayout.setPadding(50, 50, 50, 50)
        ivCancel.setOnClickListener {
            AppUtils.isPopularServiceDialogOpen = false
            view?.let {
                dialogFragmentClicks.onDialogFragmentViewClicked(DialogFragmentButtonProperties(
                        it,
                        POPULAR_SERVICE_ADD_ON_DIALOG_CANCEL
                ))
            }
            this.dismiss()
        }
        rvAddOn.layoutManager = LinearLayoutManager(context)
        rvAddOn.adapter = ServiceAddsOnAdapter(requireContext(), model.service_addons, view, model.id, null, null)

    }
}