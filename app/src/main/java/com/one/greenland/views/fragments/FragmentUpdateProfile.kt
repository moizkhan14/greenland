package com.one.greenland.views.fragments

import android.animation.LayoutTransition
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.AnimatedVectorDrawable
import android.os.Bundle
import android.util.Base64
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.one.greenland.R
import com.one.greenland.common.PROFILE_PICTURE
import com.one.greenland.common.USER_MODEL
import com.one.greenland.interfaces.BackPressedListener
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.interfaces.ItemClickListener
import com.one.greenland.models.AddressModel
import com.one.greenland.models.User
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.isValidCNIC
import com.one.greenland.utils.AppUtils.isValidFullName
import com.one.greenland.utils.AppUtils.isValidPhone
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.utils.PreferenceUtils.Companion.setValueForKey
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.AddressRecyclerAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_update_profile.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*
import java.io.ByteArrayOutputStream
import java.io.File

class FragmentUpdateProfile() : BaseFragment(), View.OnClickListener, ItemClickListener {

    private var animationName: String = ""
    private var isReadOnly = true
    private val ageGroupArray = arrayOf("18 - 24", "25 - 34", "35 - 44", "45 - 60", "60+")
    private val genderArray = arrayOf("Male", "Female", "Other")
    private var listener: BackPressedListener? = null

    var localPhotoPath: String? = null
    var serverPhotoPath: String? = null

    //Constructor

    constructor(listener: BackPressedListener) : this() {
        this.listener = listener
    }

    companion object {
        fun fragmentUpdateProfile(readOnly: Boolean): FragmentUpdateProfile {
            val fragmentObject = FragmentUpdateProfile()

            val bundle = Bundle().apply {
                putBoolean("isReadOnly", readOnly)
            }
            fragmentObject.arguments = bundle
            return fragmentObject
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializer()
        getAddresses()
        setImage()
        if (isReadOnly)
            replaceReadOnlyUi()
        else {
            view.let {
                val v: ImageView = it.findViewById(R.id.cartImageView) as ImageView
                v.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.pencil_to_tick))
                animationName = "pencil_to_tick"
                val d = v.drawable
                if (d is AnimatedVectorDrawableCompat) {
                    val avd: AnimatedVectorDrawableCompat = d
                    avd.start()
                } else if (d is AnimatedVectorDrawable) {
                    val avd: AnimatedVectorDrawable = d
                    avd.start()
                }
                replaceEditUi()
            }
        }
        registerClicks()
    }

    private fun initializer() {
        activity?.let { AppUtils.hideBottomNavigationView(it, (context as HomeActivity).bottomNavigationView) }

        (updateProfileMainConstraintLayout as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)

        (toolbarLayout as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)

        (mainInnerConstraintLayout as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)

        (clHasAddress as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGE_APPEARING)

        cartLayout.visibility = View.VISIBLE
        cartLayout.setBackgroundResource(0)
        cartImageView.setBackgroundResource(0)
        cartImageView.setImageResource(R.drawable.pencil_to_tick)
        cartImageView.visibility = View.VISIBLE

        cartLayout.layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT
        cartLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT

        cartImageView.layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT
        cartImageView.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT

        tvFragmentName.visibility = View.VISIBLE
        tvFragmentName.text = requireActivity().resources.getString(R.string.profile_text)
    }

    private fun loadViewData() {

        userNameTextView.text = (context as HomeActivity).updateUser!!.user_name
        if ((context as HomeActivity).updateUser?.user_details != null && (context as HomeActivity).updateUser?.user_details?.membership_code != null) {
            userIdTextView.text = "${requireContext().getString(R.string.membership_id)} ${(context as HomeActivity).updateUser!!.user_details?.membership_code}"
        } else {
            userIdTextView.visibility = View.GONE
        }
        val phone = (context as HomeActivity).updateUser!!.country_code + " " +
                (context as HomeActivity).updateUser!!.phone!!.substring(0, 3) + " " +
                (context as HomeActivity).updateUser!!.phone!!.substring(3, 6) + " " +
                (context as HomeActivity).updateUser!!.phone!!.substring(6, 10)

        userPhoneValueTextView.setText(phone)
        userFullNameValueTextView.setText((context as HomeActivity).updateUser!!.user_name)
        setImage()
        showLocalAddresses()
    }

    private fun registerClicks() {
        cartLayout.setOnClickListener(this)
        backImageView.setOnClickListener(this)
        uploadPictureButton.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        backImageView.isEnabled = true
    }

    private fun setupAddressRecycler(addresses: MutableList<AddressModel>, canDeleteAddress: Boolean) {
        if (rvAddress != null) {
            rvAddress.visibility = View.VISIBLE
            userAddressTitleTextView.visibility = View.VISIBLE
            (context as BaseActivity).callingFragment = getFragmentName()!!
            rvAddress.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            if (addresses.size > 1) {
                val address = addresses.find { it.is_default }
                if (address != null) {
                    addresses.remove(address)
                    addresses.add(0, address)
                }
            }
            rvAddress.adapter = AddressRecyclerAdapter(requireContext(), addresses, this, !isReadOnly, canDeleteAddress)
        }

    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_update_profile
    }

    override fun getFragmentName(): String? {
        return "UpdateProfile"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {
    }

    override fun receiveExtras(arguments: Bundle?) {
        if (arguments != null) {
            isReadOnly = arguments.getBoolean("isReadOnly", true)
        }
    }

    override fun onClick(view: View?) {
        view?.let {
            when (view.id) {
                R.id.cartLayout -> {
                    if (animationName != "pencil_to_tick") {
                        val v: ImageView = it.findViewById(R.id.cartImageView) as ImageView
                        v.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.pencil_to_tick))
                        animationName = "pencil_to_tick"
                        val d = v.drawable
                        if (d is AnimatedVectorDrawableCompat) {
                            val avd: AnimatedVectorDrawableCompat = d
                            avd.start()
                        } else if (d is AnimatedVectorDrawable) {
                            val avd: AnimatedVectorDrawable = d
                            avd.start()
                        }
                        replaceEditUi()
                    } else {
                        localPhotoPath?.let { localPath ->
                            activity?.let { fragmentActivity ->
                                context?.let { currentContext ->

                                    val encodedImage: String = encodeImage(localPath)!!
                                    println("Encoded Image --> $encodedImage end here.")
                                    setValueForKey(PROFILE_PICTURE, encodedImage)
                                    val user = Gson().fromJson(PreferenceUtils.getModelForKey(USER_MODEL), User::class.java)
                                    user.profile_photo_url = encodedImage
                                    (context as HomeActivity).updateUser!!.profile_photo_url = encodedImage
                                    PreferenceUtils.putModel(USER_MODEL, user)
                                }
                            }
                        } ?: updateUser()
                    }
                }

                R.id.backImageView -> {
                    onBackPressed()
                }

                R.id.uploadPictureButton -> {
                    (context as HomeActivity).openImagePicker(this)
                }
            }
        }
    }


    private fun encodeImage(localPath: String): String? {
        val baos = ByteArrayOutputStream()
        val image = File(localPath)
        val bmOptions = BitmapFactory.Options()
        val bitmap = BitmapFactory.decodeFile(image.absolutePath, bmOptions)
        //bitmap = Bitmap.createScaledBitmap(bitmap, parent.getWidth(), parent.getHeight(), true)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b: ByteArray = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    private fun decodeBase64(encodeString: String): Bitmap {
        val decodedString: ByteArray = Base64.decode(encodeString, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }

    override fun onBackPressed() {
        (context as HomeActivity).selectedAgeRange = ""
        (context as HomeActivity).selectedGender = ""
        if (animationName == "pencil_to_tick") {
            view?.let {
                val v: ImageView = it.findViewById(R.id.cartImageView) as ImageView
                v.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.tick_to_pencil))
                animationName = "tick_to_pencil"
                val d = v.drawable
                if (d is AnimatedVectorDrawableCompat) {
                    val avd: AnimatedVectorDrawableCompat = d
                    avd.start()
                } else if (d is AnimatedVectorDrawable) {
                    val avd: AnimatedVectorDrawable = d
                    avd.start()
                }
                replaceReadOnlyUi()
            }
        } else {
            //(context as BaseActivity).changeFragment(MenuFragment.menuFragment(), getFragmentName()!!, R.id.containerMainFragment)
            backImageView.isEnabled = false
            (context as BaseActivity).onSuperBackPressed()
            listener?.let {
                listener!!.onFragmentBackPressed()
            }

            activity?.let { AppUtils.showBottomNavigationView(it, (context as HomeActivity).bottomNavigationView) }
        }
    }

    private fun replaceEditUi() {
        tvFragmentName.text = requireActivity().resources.getString(R.string.edit_profile_text)
        setImage()

        changeViewVisibility()

        userFullNameTitleTextView.visibility = View.VISIBLE
        userFullNameValueTextView.visibility = View.VISIBLE

        viewSeparatorSeven.visibility = View.VISIBLE
        viewSeparatorSeven.visibility = View.GONE
        viewSeparatorSix.visibility = View.GONE
        viewSeparatorFive.visibility = View.GONE

        userNameTextView.visibility = View.GONE
        userIdTextView.visibility = View.GONE

        uploadPictureButton.visibility = View.VISIBLE
        removePictureButton.visibility = View.GONE

        //userGenderValueSpinner.visibility = View.VISIBLE
        userAgeValueSpinner.visibility = View.GONE

        userAgeValueTextView.visibility = View.GONE
        userAgeTitleTextView.visibility = View.GONE
        //userGenderValueTextView.visibility = View.GONE


        userCnicTitleTextView.visibility = View.GONE
        userCnicValueTextView.visibility = View.GONE


        textViewFocusListener()
        //textChangeListener()

        isReadOnly = false
        setSpinners()
        loadViewData()


    }

    private fun replaceReadOnlyUi() {
        tvFragmentName.text = requireActivity().resources.getString(R.string.profile_text)
        setImage()
        changeViewVisibility()

        userFullNameTitleTextView.visibility = View.GONE
        userFullNameValueTextView.visibility = View.GONE

        viewSeparatorSeven.visibility = View.GONE
        viewSeparatorSix.visibility = View.GONE
        viewSeparatorFive.visibility = View.GONE
        viewSeparatorSeven.visibility = View.GONE

        userPhoneValueTextView.visibility = View.VISIBLE
        userCnicValueTextView.visibility = View.GONE
        userCnicTitleTextView.visibility = View.GONE


        userNameTextView.visibility = View.VISIBLE
        userIdTextView.visibility = View.VISIBLE

        uploadPictureButton.visibility = View.GONE
        removePictureButton.visibility = View.GONE

        userGenderValueSpinner.visibility = View.GONE
        userAgeValueSpinner.visibility = View.GONE

        userAgeValueTextView.visibility = View.GONE
        userGenderValueTextView.visibility = View.GONE
        userGenderTitleTextView.visibility = View.GONE

        userAgeValueSpinner.visibility = View.GONE

        userAgeValueTextView.visibility = View.GONE
        userAgeTitleTextView.visibility = View.GONE

        isReadOnly = true
        loadViewData()
    }

    private fun setImage() {
        val user = Gson().fromJson(PreferenceUtils.getValueForKey(USER_MODEL, null), User::class.java)
        var bitmap: Bitmap? = null
        var imageString: String? = null
        if (user.profile_photo_url != null)
            imageString = user.profile_photo_url!!
        if (imageString != null)
            bitmap = decodeBase64(imageString)
        context?.let {
            Glide.with(it)
                    .load(bitmap)
                    .placeholder(R.drawable.ic_user)
                    .into(userImageView)
        }

    }

    private fun updateUser() {

        (context as HomeActivity).updateUser?.let { user ->
            serverPhotoPath?.let { serverPath ->
                user.profile_photo_url = serverPath
            }
        }

        (context as HomeActivity).updateUser!!.user_name = userFullNameValueTextView.text.toString()
        val fullName = userFullNameValueTextView.text.toString().split(" ")
        (context as HomeActivity).updateUser!!.first_name = fullName[0]
        if (fullName.size > 1) {
            (context as HomeActivity).updateUser!!.last_name = fullName[1]
            if (fullName.size > 2) {
                (context as HomeActivity).updateUser!!.last_name += (" " + fullName[2])
                if (fullName.size > 3) {
                    (context as HomeActivity).updateUser!!.last_name += (" " + fullName[3])
                }
            }
        }

        if ((context as HomeActivity).updateUser != (context as HomeActivity).getAppUser()) {
            PreferenceUtils.putModel(USER_MODEL, (context as HomeActivity).updateUser)
            onBackPressed()
        } else {
            PreferenceUtils.putModel(USER_MODEL, (context as HomeActivity).updateUser)
            onBackPressed()
        }
        onBackPressed()

    }

    private fun textViewFocusListener() {

        userFullNameValueTextView.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus)
                isValidFullName(requireContext(), userFullNameValueTextView)
        }

        userPhoneValueTextView.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus)
                isValidPhone(requireContext(), userPhoneValueTextView)
        }

        userCnicValueTextView.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus)
                isValidCNIC(requireContext(), userCnicValueTextView)
        }

    }

    private fun isValidUpdate(animate: Boolean): Boolean {
        val animShake = AnimationUtils.loadAnimation(activity, R.anim.shake);

        if (!isValidFullName(requireContext(), userFullNameValueTextView)) {
            if (animate)
                userFullNameValueTextView.startAnimation(animShake)
            return false
        }

        if (!userCnicValueTextView.text.isNullOrEmpty() && !isValidCNIC(requireContext(), userCnicValueTextView)) {
            if (animate)
                userCnicValueTextView.startAnimation(animShake)
            return false
        }

        return true
    }

    private fun setSpinners() {

        val genderSpinnerArrayAdapter: ArrayAdapter<*> = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, genderArray)
        userGenderValueSpinner.adapter = genderSpinnerArrayAdapter


        val userAgeValueArrayAdapter: ArrayAdapter<*> = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, ageGroupArray)
        userAgeValueSpinner.adapter = userAgeValueArrayAdapter

        userAgeValueSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

        }
    }

    private fun changeViewVisibility() {
        userCnicTitleTextView.visibility = View.VISIBLE
        userCnicValueTextView.visibility = View.VISIBLE
        viewSeparatorFive.visibility = View.VISIBLE

        userAgeValueTextView.visibility = View.VISIBLE
        userAgeTitleTextView.visibility = View.VISIBLE
        viewSeparatorSix.visibility = View.VISIBLE
    }

    override fun itemClicked(model: Any) {
        val address = model as AddressModel
        address.is_default = true
        (context as HomeActivity).updateUser!!.address = address
    }

    override fun itemClicked(clickId: Int, model: Any) {

    }

    private fun getAddresses() {
        showLocalAddresses()
    }

    private fun showLocalAddresses() {
        if ((context as HomeActivity).updateUser!!.addresses.size > 0 || !isReadOnly) {
            /*val user = Gson().fromJson(PreferenceUtils.getModelForKey(USER_MODEL), User::class.java)
            if (!user.addresses.isNullOrEmpty()) {*/
            setupAddressRecycler((context as HomeActivity).updateUser!!.addresses, !isReadOnly)
            // }
        } else {
            rvAddress.visibility = View.GONE
            userAddressTitleTextView.visibility = View.GONE
        }
    }

    fun setLocalPathPhotoInImageView(localPath: String) {
        Glide.with(requireContext())
                .load(localPath)
                .placeholder(R.drawable.ic_user)
                .into(userImageView)
    }

}