package com.one.greenland.views.fragments

import android.animation.LayoutTransition
import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.one.greenland.R
import com.one.greenland.common.*
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.interfaces.DialogFragmentClicks
import com.one.greenland.interfaces.ItemClickListener
import com.one.greenland.models.*
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.isPopularServiceDialogOpen
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.CategoriesRecyclerAdapter
import com.one.greenland.views.adapters.DealsSliderAdapter
import com.one.greenland.views.adapters.PopularServicesRecyclerAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.container
import pl.pzienowicz.autoscrollviewpager.AutoScrollViewPager

/**
 * A simple [Fragment] subclass.
 */
@Suppress("UNCHECKED_CAST")
class HomeFragment : BaseFragment(), View.OnClickListener,
        ItemClickListener, DialogFragmentClicks, SwipeRefreshLayout.OnRefreshListener {

    //Recycler Views
    private var handlerViewPager: Handler? = null
    private var isAddOnDialogOpen = false
    private var dialogPopularService: FragmentCustomDialog? = null
    private var dialogPopularServiceAddon: FragmentCustomDialog? = null

    //Adapters
    private var categoriesAdapter: CategoriesRecyclerAdapter? = null
    private var popularServicesAdapter: PopularServicesRecyclerAdapter? = null
    private var dealsSliderAdapter: DealsSliderAdapter? = null

    //Lists
    private var categoriesItemList: MutableList<ServiceCategoriesModel> = mutableListOf()
    private var popularServicesItemList: MutableList<PopularServicesModel> = mutableListOf()
    private var dealsList: MutableList<DealsModel> = mutableListOf()

    //Conditional Variables
    private var isDealsAvailable: Boolean? = null
    private var isCategoriesAvailable: Boolean? = null
    private var isPopularServiceAvailable: Boolean? = null

    private var service: PopularServicesModel? = null

    // User objects
    var user: User? = null

    //Constructor
    companion object {
        fun homeFragment(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
        getCategoreis()
        getPopularServices()
        viewInitializer()
        initializeSwipeLayout()

        imageViewCreateEvent.setOnClickListener {
            (context as BaseActivity).changeFragment(ChatFragment.newInstance(), getFragmentName()!!, R.id.containerMainFragment, true)
        }
    }

    private fun initializeSwipeLayout() {
        wsLayout!!.setOnRefreshListener(this)
        wsLayout.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.colorGreen))
    }

    private fun getCategoreis() {
        val caterogy1 = ServiceCategoriesModel(1, R.drawable.flowering, "Flowering Plants")
        val caterogy2 = ServiceCategoriesModel(2, R.drawable.vegetable, "Vegetable Plants")
        val caterogy3 = ServiceCategoriesModel(3, R.drawable.fruit, "Fruit Plants")
        val caterogy4 = ServiceCategoriesModel(4, R.drawable.fertilizers, "Herbs and Fertilizers")

        categoriesItemList.add(caterogy1)
        categoriesItemList.add(caterogy2)
        categoriesItemList.add(caterogy3)
        categoriesItemList.add(caterogy4)
    }

    private fun getPopularServices() {

        val listType =
                object : TypeToken<MutableList<PopularServicesModel>>() {

                }.type

        popularServicesItemList = Gson().fromJson<MutableList<PopularServicesModel>>(
                POPULAR_SERVICES,
                listType
        )

    }

    private fun initializeAndLoadData() {
        user = (requireActivity() as HomeActivity).getAppUser()

        if ((context as HomeActivity).callingFragment.isNullOrEmpty()) {
            loadData()
        }

    }

    override fun onBackPressed() {
        activity?.moveTaskToBack(true)
    }

    private fun viewInitializer() {
        if ((context as HomeActivity).bottomNavigationView.visibility != View.VISIBLE)
            AppUtils.showBottomNavigationView(requireActivity(), (context as HomeActivity).bottomNavigationView)
        (context as HomeActivity).selectedBottomNavigation(R.id.bottomNavigationItemHome)

        setUpSliderAdapter()
        setUpCategoriesRecyclerAdapter()
        setUpPopularServicesRecyclerAdapter()
    }

    private fun setUpCategoriesRecyclerAdapter() {
        categoriesRecyclerView!!.layoutManager = GridLayoutManager(requireContext(), 3)
        categoriesAdapter = CategoriesRecyclerAdapter(requireContext(), categoriesItemList, this)
        categoriesRecyclerView!!.adapter = categoriesAdapter

        if (categoriesItemList.size == 0) {
            categoriesRecyclerView.visibility = View.GONE
            tvCategoriesTitle.visibility = View.GONE
        } else {
            tvCategoriesTitle.visibility = View.VISIBLE
        }
    }

    private fun setUpPopularServicesRecyclerAdapter() {
        popularServicesRecyclerView!!.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        popularServicesAdapter = PopularServicesRecyclerAdapter(requireContext(), popularServicesItemList, this)
        popularServicesRecyclerView!!.adapter = popularServicesAdapter

        if (popularServicesItemList.size == 0) {
            popularServicesRecyclerView.visibility = View.GONE
            tvPopularServicesTitle.visibility = View.GONE
        } else {
            tvPopularServicesTitle.visibility = View.VISIBLE
        }
    }

    private fun setUpSliderAdapter() {
        dealsList.add(0, DealsModel("https://firebasestorage.googleapis.com/v0/b/instam-afd92.appspot.com/o/plant%20banner%201.png?alt=media&token=b66e8c95-fc92-4ca3-b048-97f97de1a205"))
        dealsList.add(1, DealsModel("https://firebasestorage.googleapis.com/v0/b/instam-afd92.appspot.com/o/plants%20banner%202.png?alt=media&token=dd9c42ed-604c-428b-a247-c8446e928b9b"))
        dealsList.add(2, DealsModel("https://firebasestorage.googleapis.com/v0/b/instam-afd92.appspot.com/o/plants%20banner%203.png?alt=media&token=5de99e2b-76d4-458a-8a45-038607e6521f"))
        handlerViewPager = Handler()
        dealsSliderAdapter = DealsSliderAdapter(requireContext(), dealsList, childFragmentManager)
        dealSlider.clipToPadding = false
        dealSlider.setPadding(50, 0, 50, 0)
        dealSlider.pageMargin = 10
        dealSlider.adapter = dealsSliderAdapter
        dealSlider.offscreenPageLimit = dealsList.size

        dealSlider.startAutoScroll(8000)
        dealSlider.setDirection(AutoScrollViewPager.Direction.RIGHT)
        dealSlider.setCycle(true)
        dealSlider.setInterval(5000)
        dealSlider.setScrollDurationFactor(3.0)
    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun getFragmentName(): String? {
        return HOME_FRAGMENT_TAG
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {

    }

    private fun loadData() {
        isDealsAvailable = null
        isCategoriesAvailable = null
        isPopularServiceAvailable = null
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()

    }

    override fun onClick(v: View?) {
    }

    override fun itemClicked(id: Any) {

    }

    override fun itemClicked(clickId: Int, model: Any) {
        if (clickId == CLICK_ID_CATEGORIES) {
            (context as HomeActivity).serviceItemId = 0
            (requireActivity() as BaseActivity).changeFragment(DetailsFragment.detailsFragment(categoriesItemList, model as Int), getFragmentName()!!, R.id.containerMainFragment, true)

        } else if (clickId == CLICK_ID_POPULAR_SERVICES) {

            service = model as PopularServicesModel
            if (!isPopularServiceDialogOpen) {
                isPopularServiceDialogOpen = true
                dialogPopularService = FragmentCustomDialog(
                        this,
                        false,
                        MAIN_POPULAR_SERVICE_DIALOG_TYPE,
                        model
                )
                //Todo: if service already added then get that instance and pass to it
                if (model.service_addons.size > 0) {
                    (context as HomeActivity).popularServiceOrderServiceAddons = mutableListOf()
                }

                dialogPopularService!!.show(childFragmentManager, "")
            }
        }
    }

    override fun onDialogFragmentViewClicked(dialogFragmentButtonProperties: DialogFragmentButtonProperties) {
        try {
            println("Item Clicked")
            when (dialogFragmentButtonProperties.firstButtonTag) {

                POPULAR_SERVICE_DIALOG -> {

                    if (service!!.service_addons.size > 0 && !isAddOnDialogOpen) {
                        isAddOnDialogOpen = true
                        dialogPopularServiceAddon = FragmentCustomDialog(
                                this,
                                false,
                                MAIN_POPULAR_SERVICE_ADD_ON_DIALOG_TYPE, service
                        )
                        dialogPopularServiceAddon?.show(childFragmentManager, "")
                    } else {
                        isAddOnDialogOpen = false
                        dialogPopularService!!.dismiss()
                        addService(service!!)
                    }

                }

                POPULAR_SERVICE_ADD_ON_DIALOG -> {
                    isAddOnDialogOpen = true
                    dialogPopularServiceAddon = FragmentCustomDialog(
                            this,
                            false,
                            MAIN_POPULAR_SERVICE_ADD_ON_DIALOG_TYPE, service
                    )
                    dialogPopularServiceAddon?.show(childFragmentManager, "")
                }

                POPULAR_SERVICE_ADD_ON_DIALOG_CANCEL -> {
                    if ((context as HomeActivity).popularServiceOrderServiceAddons.size > 0) {
                        if ((context as HomeActivity).orderServiceList.find { it.service_id == service?.id } != null) {
                            for (index in 0 until (context as HomeActivity).orderServiceList.size) {
                                if ((context as HomeActivity).orderServiceList[index].service_id == service?.id) {
                                    (context as HomeActivity).orderServiceList[index].order_service_addons.addAll((context as HomeActivity).popularServiceOrderServiceAddons)
                                    break
                                }
                            }
                            (context as HomeActivity).setCartOrderRequest((context as HomeActivity).orderRequest)
                            isAddOnDialogOpen = false
                            dialogPopularServiceAddon?.dismiss()
                            dialogPopularService!!.dismiss()
                        } else {
                            isAddOnDialogOpen = false
                            dialogPopularServiceAddon?.dismiss()
                            dialogPopularService!!.dismiss()
                            addService(service!!)
                        }
                    } else {
                        dialogPopularServiceAddon?.dismiss()
                    }

                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun addService(service: PopularServicesModel) {

        val serviceId = service.id
        if ((context as HomeActivity).orderServiceList.find { it.service_id == serviceId } == null) {
            val orderService = OrderService()
            orderService.service_id = service.id
            orderService.unit_count = 1
            orderService.service_title = service.service_title
            orderService.service_category_title = service.service_category_title
            orderService.unit_price = service.service_price.toDouble()
            if ((context as HomeActivity).popularServiceOrderServiceAddons.size > 0) {
                orderService.order_service_addons = (context as HomeActivity).popularServiceOrderServiceAddons
            }
            (context as HomeActivity).orderServiceList.add(orderService)
            (context as HomeActivity).orderRequest.order.order_services = (context as HomeActivity).orderServiceList
            (context as HomeActivity).setCartOrderRequest((context as HomeActivity).orderRequest)
            (context as HomeActivity).updateHomeCartBadge()
        } else {
            Toast.makeText(context, "Service already added in cart.", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onRefresh() {
        if (wsLayout != null) {
            wsLayout!!.isRefreshing = false
        }
    }
}

