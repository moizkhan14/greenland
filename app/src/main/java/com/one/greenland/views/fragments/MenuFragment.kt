package com.one.greenland.views.fragments

import android.animation.LayoutTransition
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.one.greenland.R
import com.one.greenland.common.MENU_FRAGMENT_TAG
import com.one.greenland.common.PROFILE_PICTURE
import com.one.greenland.interfaces.BackPressedListener
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.models.MenuModel
import com.one.greenland.models.User
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.MenuItemRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_menu.*

/**
 * A simple [Fragment] subclass.
 */
class MenuFragment : BaseFragment(), View.OnClickListener, BackPressedListener {

    //    private var updateImageView: ImageView? = null
    private var adapter: MenuItemRecyclerAdapter? = null
    private var menuList: MutableList<MenuModel> = mutableListOf()

    companion object {
        fun menuFragment(): MenuFragment {
            return MenuFragment()
        }
    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_menu
    }

    override fun getFragmentName(): String? {
        return MENU_FRAGMENT_TAG
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
        viewInitializer()
        updateUserInfo((context as HomeActivity).getAppUser())

        registerClicks()
    }

    override fun onResume() {
        setMenuRecyclerAdapter()
        super.onResume()
    }

    private fun getUserData() {
        if ((context as HomeActivity).updateUser?.profile_photo_url != null) {
            val bitmap = decodeBase64((context as HomeActivity).updateUser?.profile_photo_url!!)
            Glide.with(requireContext()).load(bitmap).into(userImageView)
        }

        userNameTextView.text = (context as HomeActivity).updateUser!!.user_name
    }

    private fun registerClicks() {
        updateImageView?.setOnClickListener(this)
        editableView?.setOnClickListener(this)
    }

    private fun viewInitializer() {
        (context as HomeActivity).selectedBottomNavigation(R.id.bottomNavigationItemMore)
        tvSignInSignUp.visibility = View.GONE
    }

    private fun decodeBase64(encodeString: String): Bitmap {
        val decodedString: ByteArray = Base64.decode(encodeString, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }

    @SuppressLint("SetTextI18n")
    private fun updateUserInfo(user: User) {

        userNameTextView!!.text = if (user.user_name.isNullOrEmpty()) user.first_name + " " + user.last_name else user.user_name
        userIdTextView.visibility = View.VISIBLE
        if (user.user_details != null && user.user_details?.membership_code != null) {
            userIdTextView.text = "${getString(R.string.membership_id)} ${user.user_details?.membership_code}"
        } else {
            userIdTextView.visibility = View.GONE
        }
        val imageString = PreferenceUtils.getValueForKey(PROFILE_PICTURE, null)
        if (imageString != null) {
            val bitmap = decodeBase64(imageString!!)
            context?.let {
                Glide.with(it)
                        .load(bitmap)
                        .placeholder(R.drawable.ic_user)
                        .into(userImageView)
            }
        } else {
            context?.let {
                Glide.with(it)
                        .load(R.drawable.ic_user)
                        .placeholder(R.drawable.ic_user)
                        .into(userImageView)
            }
        }

    }

    private fun setMenuRecyclerAdapter() {
        getMenuList()
        (context as BaseActivity).callingFragment = getFragmentName()
        if (menuRecyclerView != null) {
            menuRecyclerView!!.layoutManager = LinearLayoutManager(context)
            adapter = MenuItemRecyclerAdapter(requireContext(), menuList)
            menuRecyclerView!!.adapter = adapter
        }
    }

    override fun onBackPressed() {
        //(context as BaseActivity).changeFragment(HomeFragment.homeFragment(), getFragmentName()!!, R.id.containerMainFragment)

        if ((context as BaseActivity).latestFragmentFromBackStack != null) {
            var selectedFragment = 0
            selectedFragment = when {
                (context as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == CartFragment().getFragmentName() -> R.id.bottomNavigationItemCart
                (context as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == BookingFragment().getFragmentName() -> R.id.bottomNavigationItemBooking
                else -> R.id.bottomNavigationItemHome
            }

            (context as HomeActivity).selectedBottomNavigation(selectedFragment)
        }

        (context as BaseActivity).onSuperBackPressed()
    }

    private fun getMenuList() {
        menuList.clear()
        val history = MenuModel(ContextCompat.getDrawable(requireContext(), R.drawable.ic_orders), requireContext().getString(R.string.orders_text))
        menuList.add(history)
        val appointment = MenuModel(ContextCompat.getDrawable(requireContext(), R.drawable.ic_appointment), requireContext().getString(R.string.appointments))
        menuList.add(appointment)
        val cart = MenuModel(ContextCompat.getDrawable(requireContext(), R.drawable.ic_my_cart), requireContext().getString(R.string.my_cart))
        menuList.add(cart)

        /*val faq = MenuModel(ContextCompat.getDrawable(requireContext(), R.drawable.ic_faq), requireContext().getString(R.string.faq_text))
        menuList.add(faq)*/

        val logout = MenuModel(ContextCompat.getDrawable(requireContext(), R.drawable.ic_logout), requireContext().getString(R.string.logout_text))
        menuList.add(logout)


    }

    override fun onClick(v: View?) {
        v?.let {
            when (it.id) {
                R.id.editableView -> {
                    // Launch Update Profile Fragment
                    (context as HomeActivity).updateUser = (context as HomeActivity).getAppUser()
                    (context as BaseActivity).changeFragment(FragmentUpdateProfile(this), getFragmentName()!!, R.id.containerMainFragment, true)

                }
            }
        }
    }

    override fun onFragmentBackPressed() {
        getUserData()
    }

}
