package com.one.greenland.views.fragments

import android.animation.LayoutTransition
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.one.greenland.R
import com.one.greenland.common.PHONE_NUMBER
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.models.User
import com.one.greenland.receivers.ConnectivityReceiver
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.AppUtils.showErrorDialog
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.activities.UserSessionActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_otp_verification.*
import kotlinx.android.synthetic.main.activity_otp_verification.container
import kotlinx.android.synthetic.main.activity_otp_verification.forwardConstraintLayout
import kotlinx.android.synthetic.main.activity_otp_verification.forwardImageView
import kotlinx.android.synthetic.main.activity_otp_verification.forwardProgressBar
import kotlinx.android.synthetic.main.activity_user_session.*

/**
 * A simple [Fragment] subclass.
 */
class OTPVerificationFragment : BaseFragment(), View.OnClickListener {
    private var phoneNumber: String? = null
    private var user: User? = null

    //Constructor
    companion object {
        fun otpVerificationFragment(phoneNumber: String): OTPVerificationFragment {
            val fragmentObject = OTPVerificationFragment()
            val bundle = Bundle().apply {
                putString(PHONE_NUMBER, phoneNumber)
            }
            fragmentObject.arguments = bundle
            return fragmentObject
        }

        fun otpVerificationFragment(user: User): OTPVerificationFragment {
            val fragmentObject = OTPVerificationFragment()
            val bundle = Bundle().apply {
                putParcelable("user", user)
            }
            fragmentObject.arguments = bundle
            return fragmentObject
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializer()
        listenersInitializer()
        AppUtils.startTimer(requireContext(), tvResendTime, ivResendCode, tvResendCode)
    }

    private fun initializer() {
        (container as ViewGroup).layoutTransition
                .enableTransitionType(LayoutTransition.CHANGING)
        tvPhoneNumber.text = AppUtils.getCountryCode(requireContext()) + phoneNumber
    }

    private fun listenersInitializer() {
        forwardConstraintLayout.setOnClickListener(this)
        textPinEntry.setOnPinEnteredListener(PinEntryEditText.OnPinEnteredListener {
            verifyPin()
        })
        ivResendCode.setOnClickListener(this)
    }

    private fun pinVerification(pin: String) {
        showProgressBar()
        //  if (AppUtils.isGuestUser)
        // user!!.phone?.let { pinVerificationAPIRequest.pinVerification(requireContext(), this, it, pin, RI_GUEST_PIN_VERIFICATION) }
        //else
        /// pinVerificationAPIRequest.pinVerification(requireContext(), this, phoneNumber!!, pin, RI_PIN_VERIFICATION)
    }

    private fun resendCode() {
        //if (!phoneNumber.isNullOrEmpty())
        // resendCodeAPIRequest.resendCode(requireContext(), this, phoneNumber!!)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.forwardConstraintLayout -> {
                    showProgressBar()
                    verifyPin()
                }
                R.id.ivResendCode -> {
                    showProgressBar()
                    resendCode()
                }
            }
        }
    }

    override fun onBackPressed() {
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_otp_verification
    }

    override fun receiveExtras(arguments: Bundle?) {
        if (!requireArguments().getString(PHONE_NUMBER).isNullOrEmpty()) {
            phoneNumber = arguments?.getString(PHONE_NUMBER)
            (context as UserSessionActivity).topBar.visibility = View.GONE
        } else {
            user = arguments?.getParcelable("user")
            if (user != null)
                phoneNumber = user!!.phone
            AppUtils.hideBottomNavigationView(requireActivity(), (context as HomeActivity).bottomNavigationView)
        }
    }

    private fun verifyPin() {
        AppUtils.hideKeyboard(requireActivity())
        if (textPinEntry.text.toString().isEmpty() || textPinEntry.text.toString().length < 4) {
            hideProgressBar()
            Toast.makeText(
                    requireContext(),
                    "Please enter 4 digit verification code!!!",
                    Toast.LENGTH_SHORT
            ).show()
        } else {
            pinVerification(textPinEntry.text.toString())
        }
    }


    override fun addToBackStack(): Boolean {
        return false
    }

    override fun getFragmentName(): String? {
        return "OTP Verification"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    private fun showProgressBar() {
        context?.let {
            forwardConstraintLayout.background = ContextCompat.getDrawable(it, R.drawable.background_circle)
            forwardProgressBar.visibility = View.VISIBLE
            forwardImageView.visibility = View.GONE
        }
    }

    private fun hideProgressBar() {
        context?.let {
            forwardConstraintLayout.background = ContextCompat.getDrawable(it, R.drawable.round_edge_rectangle)
            forwardProgressBar.visibility = View.GONE
            forwardImageView.visibility = View.VISIBLE
        }
    }
}
