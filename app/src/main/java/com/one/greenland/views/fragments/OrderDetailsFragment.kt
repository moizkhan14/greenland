package com.one.greenland.views.fragments

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.one.greenland.R
import com.one.greenland.interfaces.BackPressedListener
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.models.OrderModel
import com.one.greenland.utils.AppUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.adapters.OrderDetailRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_order_details.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class OrderDetailsFragment(val orderModel: OrderModel) : BaseFragment(), View.OnClickListener, BackPressedListener {

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_order_details
    }

    override fun getFragmentName(): String? {
        return "Order Details"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerClicks()
        if (orderModel != null) {
            setupViews(orderModel)
        }
    }


    private fun registerClicks() {
        backImageView.setOnClickListener(this)
    }

    private fun setupViews(orderModel: OrderModel) {

        updateStatusColor()
        cvTotalBill.visibility = View.VISIBLE
        tvOrderIdHeader.text = getString(R.string.order_id_text) + orderModel.id
        tvOrderDateTimeHeader.text = "${AppUtils.getFormatedDate(orderModel.order_date!!, "yyyy MMMM, dd")} - ${orderModel!!.order_time}"
        tvPayableAmount.text = getString(R.string.payable_amount_text) + "Rs. " + orderModel.total_price?.toInt()
        tvOrderStatusBanner.text = getString(R.string.order_status_text) + getString(R.string.confirmed_text).capitalize()
        rvOrderDetails.layoutManager = LinearLayoutManager(context)
        rvOrderDetails.adapter = OrderDetailRecyclerAdapter(requireContext(), orderModel)

        clOrder.visibility = View.VISIBLE

    }

    private fun updateStatusColor() {
        when (orderModel!!.status?.toLowerCase(Locale.getDefault())) {
            "pending" -> {
                clOrder.background = ResourcesCompat.getDrawable(resources, R.drawable.round_edge_fourteen_dp, null)
                tvOrderStatusBanner.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorGreen))
            }
        }
        tvOrderStatusBanner.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        if ((context as BaseActivity).latestFragmentFromBackStack == null) {
            (context as BaseActivity).changeFragment(HomeFragment.homeFragment(), getFragmentName()!!, R.id.containerMainFragment, true)
        } else {
            (context as BaseActivity).onSuperBackPressed()
        }
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.backImageView -> {
                    onBackPressed()
                }
            }
        }
    }

    override fun onFragmentBackPressed() {


    }
}
