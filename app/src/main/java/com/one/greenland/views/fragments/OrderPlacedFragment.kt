package com.one.greenland.views.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.google.gson.Gson

import com.one.greenland.R
import com.one.greenland.common.ORDER_LISTING_MODEL
import com.one.greenland.common.USER_MODEL
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.utils.AppUtils
import com.one.greenland.utils.PreferenceUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_order_placed.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*

/**
 * A simple [Fragment] subclass.
 */
class OrderPlacedFragment : BaseFragment(), View.OnClickListener {

    var orderId: String = ""

    companion object {
        fun orderPlacedFragment(): OrderPlacedFragment {
            val fragmentObject = OrderPlacedFragment()


            return fragmentObject
        }
    }

    override fun addToBackStack(): Boolean {
        return false
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_order_placed
    }

    override fun getFragmentName(): String? {
        return "Order Discount Fragment"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {

    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        backImageView.visibility = View.GONE
        tvFragmentName.visibility = View.VISIBLE
        AppUtils.hideBottomNavigationView(requireActivity(), (context as HomeActivity).bottomNavigationView)
        tvFragmentName.text = getString(R.string.order_placed_text)
        registerClicks()
        if ((context as HomeActivity).orderModel.id.toString().isNotEmpty() && (context as HomeActivity).orderModel.id.toString() != "null") {
            tvOrderId.text = getString(R.string.your_order_id_text) + (context as HomeActivity).orderModel.id.toString()
        } else {
            tvOrderId.visibility = View.INVISIBLE
        }
        (context as HomeActivity).updateUser!!.orders.add((context as HomeActivity).orderModel)
        PreferenceUtils.putModel(USER_MODEL, (context as HomeActivity).updateUser)
        (context as HomeActivity).clearCart()
    }

    private fun registerClicks() {
        btnGoAppointments.setOnClickListener(this)
    }

    override fun onBackPressed() {

    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.btnGoAppointments -> {
                    PreferenceUtils.putModel(USER_MODEL, (context as HomeActivity).updateUser)
                    (requireActivity() as BaseActivity).changeFragment(BookingFragment(), getFragmentName()!!, R.id.containerMainFragment, false)
                }
            }
        }
    }


}
