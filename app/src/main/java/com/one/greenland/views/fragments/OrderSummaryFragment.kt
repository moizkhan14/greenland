package com.one.greenland.views.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.one.greenland.R
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.models.OrderResponseModel
import com.one.greenland.models.OrderService
import com.one.greenland.utils.AppUtils
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.OrderSummaryRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_order_summary.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*

/**
 * A simple [Fragment] subclass.
 */
class OrderSummaryFragment : BaseFragment(), View.OnClickListener {

    private var orderResponseModel: OrderResponseModel? = null


    //Constructor
    companion object {
        fun orderSummaryFragment(orderResponseModel: OrderResponseModel): OrderSummaryFragment {
            val fragmentObject = OrderSummaryFragment()
            val bundle = Bundle().apply {
                putParcelable("order_response", orderResponseModel)
            }
            fragmentObject.arguments = bundle
            return fragmentObject
        }
    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_order_summary
    }

    override fun getFragmentName(): String? {
        return "Order Summary"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {
        if (arguments != null) {
            this.orderResponseModel = arguments.getParcelable("order_response")!!
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerClicks()
        tvFragmentName.visibility = View.VISIBLE
        tvFragmentName.text = getString(R.string.order_summary_text)
        initializeData()
    }

    private fun registerClicks() {
        tvSubmitOrder.setOnClickListener(this)
        backImageView.setOnClickListener(this)
    }

    private fun setupRecyclerView(orderServices: List<OrderService>) {
        rvOrderSummary.layoutManager = LinearLayoutManager(context)

        rvOrderSummary.adapter = OrderSummaryRecyclerAdapter(requireContext(), orderServices)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.tvSubmitOrder -> {
                    (requireActivity() as BaseActivity).changeFragment(OrderPlacedFragment(), getFragmentName()!!, R.id.containerMainFragment, true)
                }

                R.id.backImageView -> {
                    onBackPressed()
                }
            }
        }
    }

    override fun onBackPressed() {
        val orderRequest = (context as HomeActivity).getCartOrderRequest()
        orderRequest.order.id = null
        orderRequest.order_id = null
        (context as HomeActivity).setCartOrderRequest(orderRequest)
        (requireActivity() as BaseActivity).onSuperBackPressed()
    }

    @SuppressLint("SetTextI18n")
    private fun initializeData() {
        cartLayout.visibility = View.GONE
        tvAddressValue.text = "${orderResponseModel!!.order!!.address!!.address_1}, ${orderResponseModel!!.order!!.address!!.area_id}, ${orderResponseModel!!.order!!.address!!.city_id}."
        tvDate.text = AppUtils.getFormatedDate(orderResponseModel!!.order!!.order_date!!, "MMM dd, yyyy")
        tvOrderTime.text = orderResponseModel!!.order!!.order_time
        if (orderResponseModel!!.order_summary != null) {
            clOrderSummery.visibility = View.VISIBLE
            tvNetTotalValue.text = "${"Rs."} ${orderResponseModel!!.order_summary!!.total_price.toInt()}"
            tvTravelChargesValue.text = "${"Rs."} ${orderResponseModel!!.order_summary!!.travel_charges.toInt()}"
            tvDiscountValue.text = "${"Rs."} ${orderResponseModel!!.order_summary!!.discount.toInt()}"
            tvTotalAmountValue.text = "${"Rs."} ${orderResponseModel!!.order_summary!!.total_price.toInt()}"
            tvOutStandingValue.text = "${"Rs."} ${orderResponseModel!!.order_summary!!.outstanding_balance.toInt()}"
        } else {
            clOrderSummery.visibility = View.GONE
        }
        if (!orderResponseModel!!.order!!.special_notes.isNullOrEmpty()) {
            cvSpecialInstruction.visibility = View.VISIBLE
            tvSpecialInstructionsValue.text = orderResponseModel!!.order!!.special_notes
        } else {
            cvSpecialInstruction.visibility = View.GONE
        }

        setupRecyclerView((context as HomeActivity).orderServiceList)
    }

}
