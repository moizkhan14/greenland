package com.one.greenland.views.fragments

import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.one.greenland.R
import com.one.greenland.common.ORDER_ID
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.interfaces.CalendarItemClickListener
import com.one.greenland.interfaces.ItemClickListener
import com.one.greenland.models.*
import com.one.greenland.utils.AppUtils.hideBottomNavigationView
import com.one.greenland.utils.AppUtils.showBottomNavigationView
import com.one.greenland.utils.CalendarUtils.convertMonthToEnglish
import com.one.greenland.utils.PreferenceUtils.Companion.getIntValueForKey
import com.one.greenland.utils.PreferenceUtils.Companion.setValueForKey
import com.one.greenland.views.activities.BaseActivity
import com.one.greenland.views.activities.HomeActivity
import com.one.greenland.views.adapters.AddressRecyclerAdapter
import com.one.greenland.views.adapters.DateRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_schedule_service.*
import kotlinx.android.synthetic.main.layout_custom_toolbar.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * created by Moiz Khan
 */
class ScheduleServiceFragment : BaseFragment(), View.OnClickListener, CalendarItemClickListener, ItemClickListener {


    private var dates: MutableList<DateModel> = mutableListOf()
    private var calendarInstance: Calendar? = null
    private var currentDate = 0
    private var currentDateString = ""
    private var lastSelectedDateString = ""
    private var currentYear = 0
    private var currentMonth = 0
    private var currentMonthPosition = 0
    private var month: String? = null
    private var monthSelected: String? = null
    private var monthSelectedInt = 0
    private var isCurrentMonth = false
    private var dateString = ""
    private var address: AddressModel? = null
    private val mcurrentTime = Calendar.getInstance()
    private var totalBill = 0.0

    companion object {
        fun scheduleServiceFragment(): ScheduleServiceFragment {
            return ScheduleServiceFragment()
        }


    }

    override fun addToBackStack(): Boolean {
        return true
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_schedule_service
    }

    override fun getFragmentName(): String? {
        return "Schedule Service"
    }


    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {

    }

    override fun receiveExtras(arguments: Bundle?) {

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        calendarInstance = Calendar.getInstance()
        (context as HomeActivity).orderModel = (context as HomeActivity).getCartOrderRequest().order
        registerClicks()
        setupCalendar()
        tvFragmentName.visibility = View.VISIBLE
        tvFragmentName.text = getString(R.string.schedule_order)
        setAddressLayout()
        setUserContactDetails()
        hitAddressesRequest()
        initializeView()
    }

    private fun initializeView() {
        hideBottomNavigationView(requireContext(), (context as HomeActivity).bottomNavigationView)

        if (!(context as HomeActivity).orderModel.order_time.isNullOrEmpty()) {
            val orderTime = (context as HomeActivity).orderModel.order_time
            tvHours.text = orderTime!!.split(":")[0]
            tvMinutes.text = orderTime.split(":")[1].split(" ")[0]
            tvMeridian.text = orderTime.split(" ")[1]

            tvHours.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAqua))
            tvMeridian.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAqua))
            tvMinutes.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAqua))

            tvHoursMinuteSeparator.visibility = View.VISIBLE
            tvMinuteMeridianSeparator.visibility = View.VISIBLE

            (context as HomeActivity).orderModel.order_time = orderTime
        }

        if (!(context as HomeActivity).orderModel.order_date.isNullOrEmpty()) {
            val orderDate = (context as HomeActivity).orderModel.order_date
            // updateCalender(Integer.parseInt(orderDate!!.split("-")[0]), Integer.parseInt(orderDate.split("-")[1]) - 1)
        }

        if (!(context as HomeActivity).orderModel.special_notes.isNullOrEmpty())
            etSpecialInstruction.setText((context as HomeActivity).orderModel.special_notes)

    }

    private fun hitAddressesRequest() {
        showLocalAddresses()
    }

    override fun onBackPressed() {
        if ((context as BaseActivity).latestFragmentFromBackStack!!.getFragmentName() == CartFragment().getFragmentName())
            showBottomNavigationView(requireContext(), (context as HomeActivity).bottomNavigationView)
        (context as BaseActivity).onSuperBackPressed()
        //(activity!! as BaseActivity).changeFragment(CartFragment(), getFragmentName()!!, R.id.containerMainFragment)
    }

    private fun registerClicks() {
        ivNextMonth.setOnClickListener(this)
        ivPreviousMonth.setOnClickListener(this)
        backImageView.setOnClickListener(this)
        cvSelectTime.setOnClickListener(this)
        tvPlaceOder.setOnClickListener(this)
    }

    private fun updateCalender(year: Int, month: Int) {
        if (isCurrentMonth) {
            ivPreviousMonth.visibility = View.GONE
        } else {
            ivPreviousMonth.visibility = View.VISIBLE
        }
        dates.clear()
        //SimpleDateFormat dateFormat = new SimpleDateFormat("d");
        val dayFormat = SimpleDateFormat("E")
        val dateFormat = SimpleDateFormat("d")
        val fullDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val cal = Calendar.getInstance()
        cal.clear()
        cal[year, month - 1] = 1
        currentDate = calendarInstance!!.get(Calendar.DATE)

        val daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        currentDateString = fullDateFormat.format(Calendar.getInstance().time)
        for (i in 0 until daysInMonth) {

            val dateModel = DateModel()
            if ((context as HomeActivity).orderModel.order_date.isNullOrEmpty() && isCurrentMonth && currentDate == (dateFormat.format(cal.time)).toInt())
                dateModel.isSelected = true
            else if (!(context as HomeActivity).orderModel.order_date.isNullOrEmpty()) {
                if ((((context as HomeActivity).orderModel.order_date!!.split("-")[2].toInt()) == (dateFormat.format(cal.time).toInt())) &&
                        ((context as HomeActivity).orderModel.order_date!!.split("-")[1].toInt()) == month)
                    dateModel.isSelected = true
            }
            dateModel.date = dateFormat.format(cal.time)
            dateModel.day = dayFormat.format(cal.time)
            dateModel.fullDate = fullDateFormat.format(cal.time)
            if (isCurrentMonth) {
                if (i >= currentDate - 3)
                    dates.add(dateModel)
            } else dates.add(dateModel)
            cal.add(Calendar.DAY_OF_MONTH, 1)
        }
        populateDates()
    }

    private fun populateDates() {
        val adapter = DateRecyclerViewAdapter(requireContext(), dates, isCurrentMonth, currentDate,
                currentMonth + 1, currentMonthPosition, currentYear, this)

        datesRecyclerView.adapter = adapter
        /*if (isCurrentMonth) {
            Handler().postDelayed({
                when {
                    currentDate <= 27 -> datesRecyclerView.smoothScrollToPosition(currentDate + 3)
                    currentDate == 28 -> datesRecyclerView.smoothScrollToPosition(currentDate + 2)
                    currentDate == 29 -> datesRecyclerView.smoothScrollToPosition(currentDate + 1)
                    else -> datesRecyclerView.smoothScrollToPosition(currentDate)
                }
            }, 200)

            adapter.notifyDataSetChanged()
        }*/
    }

    private fun isMonthStatus() {
        if (currentYear == calendarInstance!!.get(Calendar.YEAR))
            isCurrentMonth = this.month == monthSelected
    }

    private fun getMonthName(month: String): String {
        return "${month}, $currentYear"
    }

    private fun setupCalendar() {
        //val orderDate = (context as HomeActivity).orderModel.order_date
        calendarInstance = Calendar.getInstance()
//        if(!orderDate.isNullOrEmpty()){
//            currentYear = orderDate.split("-")[0].toInt()
//            currentMonth = orderDate.split("-")[1].toInt() - 1
//            isCurrentMonth = currentMonth == calendarInstance!!.get(Calendar.MONTH)
//        }
//        else {
        currentYear = calendarInstance!!.get(Calendar.YEAR)
        currentMonth = calendarInstance!!.get(Calendar.MONTH)
        isCurrentMonth = true
//        }
        monthSelectedInt = currentMonth
        tvMonth.text = getMonthName(convertMonthToEnglish(monthSelectedInt)!!)
        currentMonthPosition = calendarInstance!!.get(Calendar.MONTH) + 1
        currentMonthPosition = currentMonth + 1
        month = convertMonthToEnglish(currentMonth)

        updateCalender(currentYear, currentMonth + 1)
        datesRecyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
    }

    override fun onResume() {
        super.onResume()

    }


    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                R.id.ivNextMonth -> {
                    if (monthSelectedInt < 11) {
                        monthSelectedInt++
                        monthSelected = convertMonthToEnglish(monthSelectedInt)
                        currentMonth = monthSelectedInt
                        monthSelected = convertMonthToEnglish(monthSelectedInt)
                        isMonthStatus()
                        tvMonth.text = getMonthName(monthSelected!!)
                        updateCalender(currentYear, monthSelectedInt + 1)
                    }
                }
                R.id.ivPreviousMonth -> {
                    if (monthSelectedInt > 0) {
                        monthSelectedInt--
                        monthSelected = convertMonthToEnglish(monthSelectedInt)
                        currentMonth = monthSelectedInt
                        isMonthStatus()
                        tvMonth.text = getMonthName(monthSelected!!)
                        updateCalender(currentYear, monthSelectedInt + 1)
                    }
                }
                R.id.backImageView -> {
                    onBackPressed()
                }

                R.id.cvSelectTime -> {
                    openTimePickerDialog()
                }

                R.id.tvPlaceOder -> {
                    if (isTimeSelected()) {

                        if (address != null) {
                            createOrder()
                        } else {
                            Toast.makeText(context, "Please select address first.", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

    private fun createOrder() {
        updateTotalAmount()
        if ((context as HomeActivity).orderModel.order_date.isNullOrEmpty())
            (context as HomeActivity).orderModel.order_date = dateString
        (context as HomeActivity).orderModel.phone = (context as HomeActivity).getAppUser().phone
        (context as HomeActivity).orderModel.special_notes = etSpecialInstruction.text.toString()
        (context as HomeActivity).orderModel.order_services = (context as HomeActivity).orderServiceList

        if (etSpecialInstruction.text.isNotEmpty())
            (context as HomeActivity).orderModel.special_notes = etSpecialInstruction.text.toString()
        (context as HomeActivity).orderModel.id = null
        updateOrderCartRequest((context as HomeActivity).orderModel)
        val orderId = getIntValueForKey(ORDER_ID)
        if (orderId == 0) {
            (context as HomeActivity).orderModel.id = 1
            setValueForKey(ORDER_ID, orderId + 1)
        } else {
            (context as HomeActivity).orderModel.id = orderId
            setValueForKey(ORDER_ID, orderId + 1)
        }
        val orderResponse = OrderResponseModel()
        (context as HomeActivity).orderModel.total_price = totalBill
        (context as HomeActivity).orderModel.status = "Pending"
        orderResponse.order = (context as HomeActivity).orderModel
        val orderSummary = OrderSummaryModel(0.0, orderId.toDouble(), 2, 0.0, totalBill, 0.0)
        orderResponse.order_summary = orderSummary
        (context as HomeActivity).changeFragment(OrderSummaryFragment.orderSummaryFragment(orderResponse), getFragmentName()!!, R.id.containerMainFragment, true)

    }

    private fun updateTotalAmount() {
        var totalAmount = 0.0
        for (orderService in (requireContext() as HomeActivity).orderServiceList) {
            totalAmount += orderService.unit_price * orderService.unit_count
            if (orderService.order_service_addons.size > 0)
                for (addOn in orderService.order_service_addons) {
                    totalAmount += addOn.unit_price * addOn.unit_count
                }
        }
        totalBill = totalAmount
    }

    private fun isTimeSelected(): Boolean {
        if (!(context as HomeActivity).orderModel.order_time.isNullOrEmpty()) {
            val dateObj = SimpleDateFormat("hh:mm a").parse((context as HomeActivity).orderModel.order_time)
            val selectedHour = SimpleDateFormat("HH").format(dateObj)
            //val selectedMinute = SimpleDateFormat("mm").format((context as HomeActivity).orderModel.order_time)
            val selectedMinute = (context as HomeActivity).orderModel.order_time!!.split(":")[1].split(" ")[0]
            if (timeValidations(selectedHour.toInt(), selectedMinute.toInt())) {
                return true
            }
        } else
            Toast.makeText(context, "Please select time first.", Toast.LENGTH_SHORT).show()
        return false
    }

    private fun openTimePickerDialog() {
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val mTimePicker: TimePickerDialog

        mTimePicker = TimePickerDialog(context, OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
            run {
                updateOrderTimeView(selectedHour, selectedMinute)
            }
        }, hour, minute, false)

        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }

    private fun timeValidations(selectedHour: Int, selectedMinute: Int): Boolean {
        val calendar = Calendar.getInstance()
        mcurrentTime[Calendar.HOUR_OF_DAY] = selectedHour
        mcurrentTime[Calendar.MINUTE] = selectedMinute

        if (mcurrentTime.timeInMillis <= calendar.timeInMillis && currentDateString == dateString) {
            Toast.makeText(context, "You are trying to set past time.", Toast.LENGTH_LONG).show()
            return false
        } else if (mcurrentTime.timeInMillis <= calendar.timeInMillis + 7200000 && currentDateString == dateString) {
            Toast.makeText(context, "Order time must be two hours ahead", Toast.LENGTH_LONG).show()
            return false
        } else if ((selectedHour < 9) || ((selectedHour == 20 && selectedMinute > 0) || (selectedHour > 20))) {
            Toast.makeText(context, "To avail services please select time between 9AM to 8PM.", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }


    private fun updateOrderTimeView(selectedHour: Int, selectedMinute: Int) {
        if (timeValidations(selectedHour, selectedMinute)) {
            var selectedTime = "$selectedHour:$selectedMinute"
            try {
                val sdf = SimpleDateFormat("H:mm")
                val dateObj = sdf.parse(selectedTime)
                selectedTime = SimpleDateFormat("hh:mm a").format(dateObj)
                tvHours.text = selectedTime.split(":")[0]
                tvMinutes.text = selectedTime.split(":")[1].split(" ")[0]
                tvMeridian.text = selectedTime.split(" ")[1]
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            tvHours.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAqua))
            tvMeridian.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAqua))
            tvMinutes.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAqua))

            tvHoursMinuteSeparator.visibility = View.VISIBLE
            tvMinuteMeridianSeparator.visibility = View.VISIBLE

            (context as HomeActivity).orderModel.order_time = selectedTime
            updateOrderCartRequest((context as HomeActivity).orderModel)
        }
    }

    private fun setAddressLayout() {
        clHasAddress.visibility = View.VISIBLE
        clNoAddress.visibility = View.GONE

    }

    private fun setupAddressRecycler(addresses: MutableList<AddressModel>) {
        (context as HomeActivity).callingFragment = getFragmentName()!!
        rvAddress.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        if (addresses.size > 1) {
            val address = addresses.find { it.is_default }
            if (address != null) {
                addresses.remove(address)
                addresses.add(0, address)
            }
        }
        rvAddress.adapter = AddressRecyclerAdapter(requireContext(), addresses, this, true, true)
    }

    private fun setUserContactDetails() {
        etPhoneNumber.isEnabled = false

        if (!(context as HomeActivity).getAppUser().phone.isNullOrEmpty()) {
            etPhoneNumber.setText((context as HomeActivity).getAppUser().phone)
        }
    }

    override fun dateClicked(date: DateModel) {
        if (lastSelectedDateString != date.fullDate) {

        }
        dateString = date.fullDate!!
        lastSelectedDateString = dateString
        (context as HomeActivity).orderModel.order_date = dateString
    }

    private fun showLocalAddresses() {
        setupAddressRecycler((context as HomeActivity).getAppUser().addresses)
    }

    override fun itemClicked(model: Any) {
        address = model as AddressModel
        address?.let {
            (context as HomeActivity).orderModel.address = it
            updateOrderCartRequest((context as HomeActivity).orderModel)
        }

    }

    private fun updateOrderCartRequest(order: OrderModel) {
        val orderRequest = (context as HomeActivity).getCartOrderRequest()
        orderRequest.order = order
        (context as HomeActivity).setCartOrderRequest(orderRequest)
    }

    override fun itemClicked(clickId: Int, model: Any) {

    }

}
