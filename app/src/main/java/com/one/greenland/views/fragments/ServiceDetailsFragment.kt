package com.one.greenland.views.fragments

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout.VERTICAL
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.one.greenland.R
import com.one.greenland.common.*
import com.one.greenland.interfaces.BaseFragmentListener
import com.one.greenland.interfaces.DataUpdateListener
import com.one.greenland.models.Service
import com.one.greenland.models.ServiceCategoriesModel
import com.one.greenland.views.adapters.ServiceDetailsAdapter
import kotlinx.android.synthetic.main.fragment_service_details.*


/**
 * A simple [Fragment] subclass.
 */
class ServiceDetailsFragment : BaseFragment() {

    private var serviceCategoriesModel: ServiceCategoriesModel? = null
    private var dataUpdateListener: DataUpdateListener? = null

    //Constructor
    companion object {
        fun newInstance(serviceCategoriesModel: ServiceCategoriesModel): ServiceDetailsFragment {
            val fragmentObject = ServiceDetailsFragment()
            val bundle = Bundle().apply {
                putParcelable("serviceCategory", serviceCategoriesModel)
            }
            fragmentObject.arguments = bundle
            return fragmentObject
        }
    }

    public fun setCartUpdateListener(dataUpdateListener: DataUpdateListener) {
        this.dataUpdateListener = dataUpdateListener
    }

    override fun addToBackStack(): Boolean {
        return false
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_service_details
    }

    override fun getFragmentName(): String? {
        return "Service Detail Fragment"
    }

    override fun setBaseFragmentListener(baseFragmentListener: BaseFragmentListener?) {
    }

    override fun receiveExtras(arguments: Bundle?) {
        if (arguments != null)
            this.serviceCategoriesModel = arguments.getParcelable("serviceCategory")!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this)
                .load(serviceCategoriesModel!!.category_svg_icon)
                .into(ivHeader)


    }

    override fun onResume() {
        super.onResume()
        when (serviceCategoriesModel!!.id) {
            1 -> {
                setupRecyclerView(FLOWERING_PLANTS)
            }
            2 -> {
                setupRecyclerView(PLANTS_VEGETABLE)
            }
            3 -> {
                setupRecyclerView(PLANTS_FRUIT)
            }
            4 -> {
                setupRecyclerView(HERBS_FERTILIZERS)
            }
        }
    }

    private fun setupRecyclerView(listToLoad: String) {
        val layoutManager = LinearLayoutManager(requireContext())
        rvServiceDetails.layoutManager = layoutManager
        val listType =
                object : TypeToken<ArrayList<Service>>() {

                }.type

        val serviceList = Gson().fromJson<MutableList<Service>>(
                listToLoad,
                listType
        )
        val itemDecor = DividerItemDecoration(requireContext(), VERTICAL)
        rvServiceDetails.addItemDecoration(itemDecor)
        if (serviceList.isNotEmpty()) {
            rvServiceDetails.layoutManager = layoutManager
            rvServiceDetails.adapter = ServiceDetailsAdapter(requireContext(), serviceList, serviceCategoriesModel!!.service_category_title, dataUpdateListener!!)
        } else {
            tvNoService.visibility = View.VISIBLE
        }
    }
}
